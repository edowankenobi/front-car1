export const environment = {
  production: true,
  log: {
    active: 'true',
    level: 'TRACE',
  },
  gtagManager: {
    id: 'GTM-WMD28CX',
  },
  flujoCompra: {
    baseUrl: "https://flujocompras.car1.cl",
    comprar: "/comprar/",
    login: "/login"
  },
  hubspot: {
    agenda: "https://agenda.car1.cl/meetings/ventas-car1/agenda-sell-out"
  },
  financiamiento: {
    pie: {
      auto: 40,
      minimo: 20,
      maximo: 90
    },
    seguros: {
      desgravamen: {
        checked: true,
        disabled: true
      },
      cesantia: {
        checked: true,
        disabled: false
      }
    },
    cuotas: {
      secuencia: [6, 12, 18, 24, 30, 36, 42, 48],
      defecto: 48
    }
  },
  documentos: 
  {
    informes: 
    {
      baseURL: 'https://informes.car1.cl',
      tipos:
      [
        {
          nombre: 'Certificados de anotaciones vigentes',
          id: 1,
          path: '/',
          prefijo: '',
          extension: '.pdf',
          orden: 1
        },
        {
          nombre: 'Informe mecánico',
          id: 2,
          path: '/',
          prefijo: 'InformeMecanico_',
          extension: '.pdf',
          orden: 2
        },
      ]
    }
  },
  fotos: {
    baseURL: 'https://photos-car.car1.cl/',
    nombre: 'cu-{number}.jpg'
  },
  telefonos: {
    compra: 
    [
      {
        visible: '+56 9 8555 5744',
        interno: '56985555744',
        esficha: true
      }
    ],
    vende: 
    [
      {
      visible: '+56 9 5862 0719',
      interno: '56958620719'
      },
      {
        visible: '+56 9 7127 5092',
        interno: '56971275092'
      },
      {
        visible: '+56 9 7134 5472',
        interno: '56971345472'
      }
    ]
  },
  restServices: {
    strapi: {
      baseURL: 'https://cms.car1.cl:1337',
      tipoVehiculo: '/tipo-de-vehiculos',
      publicidadHome: '/publicidad-homes',
      bannerHome: '/banner-home',
      catalogoAutos: '/catalogo-autos',
      filtros: '/filtros',
      marcas: '/marcas',
      tiposTransmision: '/tipo-de-transmisions',
      tiposCombustible: '/tipo-de-combustibles',
      kilometrajeTasacion: '/kilometraje-tasacions',
      pdfTerminosCondiciones: '/upload/files/185',
    },
    tasador: {
      baseURL: 'https://api.car1.cl/api2/v1/tasador',
      marca: '/marca',
      tasador: '/tasacion',
      patente: '/placa',
      modelo: '/modelo',
      anio: '/agno',
      version: '/version',
    },
    tasadorInterno: {
      baseURL: 'https://api-tasacion.car1.cl/api/tasador',
      anio: '/anos',
      marca: '/marcas/',
      modelo: '/modelos/',
      version: '/version/',
      kilometraje: '/kilometraje',
      tasador: '/cotizacion',
      patente: '/buscappu'
    },
    agenda: {
      baseURL: 'https://api.car1.cl/api2/v1/venta/agenda',
      hora: '/hora',
      cita: '/cita',
      feriados: '/feriados',
    },
    pasoFlujoVenta: {
      baseURL: 'https://api.car1.cl/api2/v1/venta/flujo',
      paso: '/paso',
      detalle: '/detalle',
    },
    btnComprasFichaVehiculo: {
      baseUrl: 'https://api.car1.cl/api2/v1/compra',
      estado: '/estado',
      empresa: '/empresa',
    },
    financiamiento: {
      baseURL: 'https://api-finan-web.car1.cl/api/financia/web',
      simulacion: '/simulacion',
      aceptacion: '/aceptacion',
      configuracion: '/configuracion'
    },
    autoPartePago: {
      baseURL: 'https://api-app.car1.cl/api/app',
      encuesta: {
        preguntas: '/encuesta/preguntas',
        verifica: '/encuesta/verifica'
      },
      guardar: '/guardar',
      resumen: '/resumen',
      aceptar: '/aceptar',
      origen: 101
    },
    formularioContacto: 'https://api.car1.cl/api2/v1/contacto/email',
    region: 'https://apis.digital.gob.cl/dpa/regiones',
    comuna: 'https://apis.digital.gob.cl/dpa/comunas',
    hubSpot:{
      contactoSellIn: 'https://api-hubspot.car1.cl/api/contacto/registrasellin',
      negocioSellIn: 'https://api-hubspot.car1.cl/api/negocio/registrasellin',
      agendaSellIn: 'https://api-hubspot.car1.cl/api/negocio/confirmaagendasellin',
      contactoSellOut: 'https://api-hubspot.car1.cl/api/contacto/registrasellout',
      negocioSellOut: 'https://api-hubspot.car1.cl/api/negocio/registrasellout',
    }
  },
  siteKeyCaptcha: '6Lf9RoMaAAAAAHO_PW5pIQI4_SYYHQneFVecpR7V',
  idServicio: '311092',
  idUbicacion: '13635',
  diasHabiles: 3,
  rollbarToken: 'b7fee2ac53c84c5b8ece67f26aefd663',
};

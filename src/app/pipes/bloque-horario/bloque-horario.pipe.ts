import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bloqueHorario'
})
export class BloqueHorarioPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    const inicioBloque = value.split(':')[0];
    const finBloqueNum = Number(inicioBloque) + 1;
    return `${value} - ${finBloqueNum.toString()}:00`;
  }

}

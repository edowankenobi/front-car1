import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactoGeneralComponent } from '@app/pages/contacto-general/contacto-general.component';
import { FichaVehiculoIframeComponent } from '@app/pages/ficha-vehiculo-iframe/ficha-vehiculo-iframe.component';
import { SobreNosotrosComponent } from '@app/pages/sobre-nosotros/sobre-nosotros.component';
import { StockDisponibleComponent } from '@app/pages/stock-disponible/stock-disponible.component';
import { TasadorComponent } from '@app/pages/tasador/tasador.component';
import { VentaVehiculoComponent } from '@app/pages/venta-vehiculo/venta-vehiculo.component';
import { HomeComponent } from './pages/home/home.component';
import { LandingSellInComponent } from './pages/landing-sell-in/landing-sell-in.component';
import { LandingSellOutComponent } from './pages/landing-sell-out/landing-sell-out.component';
import { DatosPersonaComponent } from './pages/nuevo-tasador/datos-persona/datos-persona.component';
import { NuevoTasadorComponent } from './pages/nuevo-tasador/nuevo-tasador.component';
import { AgendamientoComponent } from './pages/tasador/agendamiento/agendamiento.component';
import { DatosPersonalesComponent } from './pages/tasador/datos-personales/datos-personales.component';
import { FormularioTasacionComponent } from './pages/tasador/formulario-tasacion/formulario-tasacion.component';
import { ExitoUiComponent } from './ui/exito-ui/exito-ui.component';
import { PruebasComponent } from './pages/pruebas/pruebas.component';
import { FullViewImageMobileComponent } from '@app/pages/full-view-image-mobile/full-view-image-mobile.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'ficha-vehiculo-iframe/:patente',
    component: FichaVehiculoIframeComponent,
  },
  { path: 'stock-disponible', component: StockDisponibleComponent },
  { path: 'contacto', component: ContactoGeneralComponent },
  { path: 'sobre-nosotros', component: SobreNosotrosComponent },
  { path: 'tasador', component: VentaVehiculoComponent },
  


  { path: 'auto-quiero-vender', component: TasadorComponent},
  { path: 'auto-recibe-oferta', component: DatosPersonalesComponent },
  { path: 'auto-agenda-inspeccion', component: AgendamientoComponent },
  { path: 'auto-confirmacion-inspeccion', component: ExitoUiComponent },
  { path: 'nuevo-tasador', component: NuevoTasadorComponent },
  { path: 'compra-autos-usados', component: LandingSellInComponent },
  { path: 'autos-usados', component: LandingSellOutComponent },
  { path: 'photo/:patente/:validacion', component: FullViewImageMobileComponent },
  { path: 'pruebas', component: PruebasComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

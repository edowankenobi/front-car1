import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocationResponse } from '@app/models/locations-api/region-response.model';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LocationApiService {
  constructor(private httpClient: HttpClient) {}

  /**
   * Api que retorna las regiones
   * @param formulario datos para agendamiento.
   */
  public getRegion(): Observable<LocationResponse[]> {
    const baseURL = environment.restServices.region;
    return this.httpClient.get<LocationResponse[]>(baseURL);
  }

  /**
   * Api que retorna las comunas
   * @param formulario datos para agendamiento.
   */
  public getComuna(): Observable<any> {
    const baseURL = environment.restServices.comuna;
    return this.httpClient.get<any>(baseURL);
  }
}

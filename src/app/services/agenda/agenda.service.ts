import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '@env/environment';
import { FechasDisponiblesResponseModel } from '@app/models/agenda/fechas-disponibles-response.model';
import { PeticionEnvioCita } from '@app/models/agenda/peticion-envio-cita.model';
import { RespuestaEnvioCita } from '@app/models/agenda/respuesta-envio-cita.model';
import { DiasFeriadosResponseModel } from '@app/models/agenda/dias-feriados-response.model';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  /**
   * Subject que escuchar los días feriados rescatados desde el servicio
   */
  private diasFeriados = new BehaviorSubject<Date[]>([]);
  diasFeriados$ = this.diasFeriados.asObservable();

  /**
   * Constructor
   * @param httpClient cliente http.
   */
  constructor(private httpClient: HttpClient) { }

  /**
   * Obtiene las horas disponibles para el día indicado
   * @param dia fecha de consulta
   */
  public obtenerHorasDisponibles(
    dia: string
  ): Observable<FechasDisponiblesResponseModel> {
    const { baseURL, hora } = environment.restServices.agenda;

    const endpoint = `${baseURL}${hora}`;

    let params: HttpParams = new HttpParams();

    params = params.append('idServicio', environment.idServicio);
    params = params.append('idUbicacion', environment.idUbicacion);
    params = params.append('fecha', dia);

    const httpOptions = { params };

    return this.httpClient.get<FechasDisponiblesResponseModel>(
      endpoint,
      httpOptions
    );
  }

  /**
   * Envia los datos para agendar cita y enviar correo de confirmación
   * @param formulario datos para agendamiento.
   */
  public enviarFormularioCita(
    formulario: PeticionEnvioCita
  ): Observable<RespuestaEnvioCita> {
    const { baseURL, cita } = environment.restServices.agenda;
    const endpoint = `${baseURL}${cita}`;
    return this.httpClient.post<RespuestaEnvioCita>(endpoint, formulario);
  }

  /**
   * Obtiene las horas disponibles para el día indicado
   */
  public obtenerDiasFeriados(): Observable<DiasFeriadosResponseModel> {
    const { baseURL, feriados } = environment.restServices.agenda;

    const endpoint = `${baseURL}${feriados}`;

    return this.httpClient.get<DiasFeriadosResponseModel>(
      endpoint
    );
  }

  /**
   * Almacena arreglo de días feriados.
   */
  almacenaFeriados(diasFeriados: Date[]): void {
    this.diasFeriados.next(diasFeriados);
  }
}

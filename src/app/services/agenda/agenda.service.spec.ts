import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { DiasFeriadosResponseModel } from '@app/models/agenda/dias-feriados-response.model';
import { FechasDisponiblesResponseModel } from '@app/models/agenda/fechas-disponibles-response.model';
import { PeticionEnvioCita } from '@app/models/agenda/peticion-envio-cita.model';
import { RespuestaEnvioCita } from '@app/models/agenda/respuesta-envio-cita.model';
import { environment } from '@env/environment';

import { AgendaService } from './agenda.service';

describe('AgendaService', () => {
  let service: AgendaService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(AgendaService);
    injector = getTestBed();
    httpMock = injector.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('obtenerHorasDisponibles', (done) => {

    let mockResponse = {} as FechasDisponiblesResponseModel;
    service.obtenerHorasDisponibles('').subscribe((response) => {
      expect(response).toBe(mockResponse);
      done();
    });
    const { baseURL, hora } = environment.restServices.agenda;
    const endpoint = `${baseURL}${hora}?idServicio=311259&idUbicacion=13635&fecha=`;
    const req = httpMock.expectOne(endpoint);
    expect(req.request.method).toBe('GET');
    req.flush(mockResponse);
  });

  it('enviarFormularioCita', (done) => {

    let mockResponse = {} as RespuestaEnvioCita;
    service.enviarFormularioCita({} as PeticionEnvioCita).subscribe((response) => {
      expect(response).toBe(mockResponse);
      done();
    });
    const { baseURL, cita } = environment.restServices.agenda;
    const endpoint = `${baseURL}${cita}`;
    const req = httpMock.expectOne(endpoint);
    expect(req.request.method).toBe('POST');
    req.flush(mockResponse);
  });

  it('obtenerDiasFeriados', (done) => {

    let mockResponse = {} as DiasFeriadosResponseModel;
    service.obtenerDiasFeriados().subscribe((response) => {
      expect(response).toBe(mockResponse);
      done();
    });
    const { baseURL, feriados } = environment.restServices.agenda;
    const endpoint = `${baseURL}${feriados}`;
    const req = httpMock.expectOne(endpoint);
    expect(req.request.method).toBe('GET');
    req.flush(mockResponse);
  });

  it('almacenaFeriados', (done) => {

    service.almacenaFeriados([new Date()]);
    done();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoggerService } from '@app/shared/services/logger.service';
import { TipoVehiculo } from '@app/models/tipo-vehiculo/tipo-vehiculo.model';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class TipoVehiculoService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) { }

  /**
   * Obtiene los tipos de vehículos existentes en BD
   */
  public tipoVehiculos(): Observable<TipoVehiculo[]> {

    const { baseURL } = environment.restServices.strapi;
    const { tipoVehiculo } = environment.restServices.strapi;

    const endpoint = `${baseURL}${tipoVehiculo}`;

    return this.httpClient.get<TipoVehiculo[]>(endpoint);
  }
}

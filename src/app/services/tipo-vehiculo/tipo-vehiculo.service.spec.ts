import { TestBed } from '@angular/core/testing';

import { TipoVehiculoService } from './tipo-vehiculo.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TipoVehiculoService', () => {
  let service: TipoVehiculoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(TipoVehiculoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

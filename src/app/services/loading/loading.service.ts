import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  /**
   * Subject con información de elementos cargando
   */
  private loadingSubject = new BehaviorSubject<number>(0);

  /**
   * Subject con información de si es spinner es para fomulario
   */
  private loadingFormulario = new BehaviorSubject<boolean>(false);

  /**
   * Estado de loading
   */
  loadingState = this.loadingSubject.asObservable();

  /**
   * Estado que controla si el spinner es para formularios.
   */
  formularioState = this.loadingFormulario.asObservable();

  constructor() { }

  /**
   * Agrega un elemento del contador de carga
   */
  show(esFormulario: boolean = false): void {
    this.loadingSubject.next(this.loadingSubject.getValue() + 1);
    this.loadingFormulario.next(esFormulario);
  }

  /**
   * Elimina un elemento del contador de carga
   */
  hide(): void {
    const currentValue = this.loadingSubject.getValue();
    if (currentValue > 0) {
      this.loadingSubject.next(this.loadingSubject.getValue() - 1);
    }
  }

  /**
   * Resetea el contador de loading
   */
  reset(): void {
    this.loadingSubject.next(0);
  }
}

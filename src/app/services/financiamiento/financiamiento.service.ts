import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AceptaFinanModel } from '@app/models/financiamiento/financiamiento.acepta.model';
import { FinanConfiguracionModel } from '@app/models/financiamiento/financiamiento.configuracion.model';
import { FinanciamientoModel } from '@app/models/financiamiento/financiamiento.model';
import { FinanciamientoResponseModel } from '@app/models/financiamiento/financiamiento.response.model';
import { LoggerService } from '@app/shared/services/logger.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FinanciamientoService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) {}

  /**
   * Obtiene la ficha del vehículo en base a la patente
   */
  public simulacion(data: FinanciamientoModel): Observable<FinanciamientoResponseModel> {
    const { baseURL, simulacion } = environment.restServices.financiamiento;

    const endpoint = `${baseURL}${simulacion}`;

    return this.httpClient.post<FinanciamientoResponseModel>(endpoint, data);
  }
  public aceptacion(data: AceptaFinanModel): Observable<number> {
    const { baseURL, aceptacion } = environment.restServices.financiamiento;

    const endpoint = `${baseURL}${aceptacion}`;

    return this.httpClient.post<number>(endpoint, data);
  }
  public configuracion(): Observable<FinanConfiguracionModel> {
    const { baseURL, configuracion } = environment.restServices.financiamiento;

    const endpoint = `${baseURL}${configuracion}`;

    return this.httpClient.get<FinanConfiguracionModel>(endpoint);
  }
}

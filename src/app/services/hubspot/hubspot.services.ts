import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PeticionEnvioCorreo } from '@app/models/contacto/peticion-envio-correo.model';
import { LoggerService } from '@app/shared/services/logger.service';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { RespuestaEnvioCorreo } from '@app/models/contacto/respuesta-envio-correo.model';
import { PeticionContacto } from '@app/models/hubspot/peticion-contacto.model';
import { PeticionNegocio } from '@app/models/hubspot/peticion-negocio.model';
import { PeticionAgendaModel } from '@app/models/hubspot/peticion-agenda.model';

@Injectable({
  providedIn: 'root'
})
export class HubspotService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) { }

  /**
   * Envia los datos del formulario de contacto o cotización para envío de correo
   * @param formulario datos formulario.
   */
  public enviarContactoHubspotSellOut(
    request: PeticionContacto
  ): Observable<string> {

    const endpoint = environment.restServices.hubSpot.contactoSellOut;
    return this.httpClient.post<string>(endpoint, request);
  }

  public enviarNegocioHubspotSellOut(
    request: PeticionNegocio,
    idContacto: string
  ): Observable<PeticionNegocio> {
    const endpoint = environment.restServices.hubSpot.negocioSellOut + '/' + idContacto;
    return this.httpClient.post<any>(endpoint, request);
  }

  public enviarContactoHubspotSellIn(
    request: PeticionContacto
  ): Observable<any> {

    const endpoint = environment.restServices.hubSpot.contactoSellIn;
    return this.httpClient.post<any>(endpoint, request);
  }

  public enviarNegocioHubspotSellIn(
    request: PeticionNegocio,
    idContacto: string
  ): Observable<PeticionNegocio> {
    const endpoint = environment.restServices.hubSpot.negocioSellIn + '/' + idContacto;

    return this.httpClient.post<any>(endpoint, request);
  }

  public agendaNegocioHubspotSellIn(
    request: PeticionAgendaModel
  ): Observable<PeticionAgendaModel> {
    const endpoint = environment.restServices.hubSpot.agendaSellIn;

    console.log(endpoint);
    console.log(request);
    
    return this.httpClient.post<any>(endpoint, request);
  }
}

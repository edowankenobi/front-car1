import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { TerminosCondicionesResponseModel } from '@app/models/pdf/terminos-condiciones-response.model';

@Injectable({
  providedIn: 'root'
})
export class PdfService {

  /**
   * Constructor
   * @param httpClient cliente http.
   */
  constructor(private httpClient: HttpClient) { }

  /**
   * Obtiene la url de pdf de Términos y Condiciones
   */
  public obtenerPdfTerminosCondiciones(): Observable<TerminosCondicionesResponseModel> {
    const { baseURL, pdfTerminosCondiciones } = environment.restServices.strapi;
    const endpoint = `${baseURL}${pdfTerminosCondiciones}`;

    return this.httpClient.get<TerminosCondicionesResponseModel>(endpoint);
  }
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { TasadorService } from './tasador.service';

describe('TasadorService', () => {
  let service: TasadorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(TasadorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

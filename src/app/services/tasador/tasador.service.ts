import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AnioTasadorResponseModel } from '@app/models/tasador/anio-tasador-response.model';
import { KilometrajeTasadorModel } from '@app/models/tasador/kilometraje-tasador.model';
import { MarcaTasadorResponseModel } from '@app/models/tasador/marca-tasador-response.model';
import { PatenteTasadorResponseModel } from '@app/models/tasador/patente/patente-tasador-response.model';
import { ValorTasacionResponseModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion-response.model';
import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';
import { VersionTasadorResponseModel } from '@app/models/tasador/version-tasador-response.model';
import { filtrosStrapiUtil } from '@app/util/filtros-strapi.util';
import { environment } from '@env/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { ModeloTasadorResponseModel } from '../../models/tasador/modelo-tasador-response.model';

@Injectable({
  providedIn: 'root',
})
export class TasadorService {
  /**
   * Subject que escuchar la oferta aceptada
   */
  private ofertaAceptada = new BehaviorSubject<ValorTasacionModel | null>(null);
  ofertaAceptada$ = this.ofertaAceptada.asObservable();

  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private httpClient: HttpClient) {}

  /**
   * Obtiene los datos del vehículo por patente
   * @param patente patente del vehículo
   */
  public datosPorPatente(
    placa: string
  ): Observable<PatenteTasadorResponseModel> {
    const { baseURL } = environment.restServices.tasador;
    const { patente } = environment.restServices.tasador;

    const endpoint = `${baseURL}${patente}`;

    let params: HttpParams = new HttpParams();

    params = params.append('patente', placa);

    const httpOptions = { params };

    return this.httpClient.get<PatenteTasadorResponseModel>(
      endpoint,
      httpOptions
    );
  }

  /**
   * Obtiene valor de tasación en base a datos del vehículo
   * @param idMarca id de la Marca
   * @param idModelo id del Modelo
   * @param anio año de fabricación
   * @param idVersion id de la Versión
   * @param idRequested requested_id
   * @param kilometraje kilometraje del vehiculo
   * @param captcha captcha de seguridad.
   */
  public obtenerValorTasacion(
    idMarca: number,
    idModelo: number,
    anio: number,
    idVersion: number,
    idRequested: number,
    kilometraje: string,
    captcha: string
  ): Observable<ValorTasacionResponseModel> {
    const { baseURL } = environment.restServices.tasador;
    const { tasador } = environment.restServices.tasador;

    const endpoint = `${baseURL}${tasador}`;

    let params: HttpParams = new HttpParams();

    params = params.append('brand_id', idMarca.toString());
    params = params.append('model_id', idModelo.toString());
    params = params.append('year', anio.toString());
    params = params.append('version_id', idVersion.toString());
    params = params.append('requested_id', idRequested.toString());
    params = params.append('km', kilometraje);
    params = params.append('captcha', captcha);

    const httpOptions = { params };

    return this.httpClient.get<ValorTasacionResponseModel>(
      endpoint,
      httpOptions
    );
  }

  /**
   * Obtiene los valores para poblar el select de kilometraje en el formulario de tasación
   */
  public kilometrajeSeleccionable(): Observable<KilometrajeTasadorModel[]> {
    const { baseURL } = environment.restServices.strapi;
    const { kilometrajeTasacion } = environment.restServices.strapi;

    const endpoint = `${baseURL}${kilometrajeTasacion}`;

    let params: HttpParams = new HttpParams();

    params = params.append(
      filtrosStrapiUtil.orden.parametroOrdenamiento,
      filtrosStrapiUtil.orden.kilometrajeTasacion.ascendiente
    );

    const httpOptions = { params };

    return this.httpClient.get<KilometrajeTasadorModel[]>(
      endpoint,
      httpOptions
    );
  }

  /**
   * Obtiene el listado de marcas
   */
  public obtenerMarcas(ano: string): Observable<MarcaTasadorResponseModel> {
    const { baseURL, marca } = environment.restServices.tasador;
    const endpoint = `${baseURL}${marca}`;
    let params: HttpParams = new HttpParams();

    params = params.append('ano', ano.toString());

    const httpOptions = { params };

    return this.httpClient.get<MarcaTasadorResponseModel>(
      endpoint,
      httpOptions
    );
  }

  /**
   * Obtiene listado de modelos según marca
   * @param idMarca id de la Marca
   */
  public obtenerModelos(
    idMarca: number,
    anio: string
  ): Observable<ModeloTasadorResponseModel> {
    const { baseURL, modelo } = environment.restServices.tasador;

    const endpoint = `${baseURL}${modelo}`;

    let params: HttpParams = new HttpParams();

    params = params.append('idMarca', idMarca.toString());
    params = params.append('anio', anio.toString());

    const httpOptions = { params };

    return this.httpClient.get<ModeloTasadorResponseModel>(
      endpoint,
      httpOptions
    );
  }

  /**
   * Obtiene listado de años según marca/modelo
   * @param idModelo id del modelo
   */
  public obtenerAnios(idModelo: number): Observable<AnioTasadorResponseModel> {
    const { baseURL, anio } = environment.restServices.tasador;

    const endpoint = `${baseURL}${anio}`;

    let params: HttpParams = new HttpParams();

    params = params.append('idModelo', idModelo.toString());

    const httpOptions = { params };

    return this.httpClient.get<AnioTasadorResponseModel>(endpoint, httpOptions);
  }

  /**
   * Obtiene listado de versiones según modelo/año
   * @param idModelo id del modelo
   * @param idAnio id del modelo
   */
  public obtenerVersiones(
    idModelo: number,
    idAnio: number
  ): Observable<VersionTasadorResponseModel> {
    const { baseURL, version } = environment.restServices.tasador;

    const endpoint = `${baseURL}${version}`;

    let params: HttpParams = new HttpParams();

    params = params.append('idModelo', idModelo.toString());
    params = params.append('anio', idAnio.toString());

    const httpOptions = { params };

    return this.httpClient.get<VersionTasadorResponseModel>(
      endpoint,
      httpOptions
    );
  }

  /**
   * Almacena la oferta seleccionada.
   */
  enviarOferta(oferta: ValorTasacionModel): void {
    this.ofertaAceptada.next(oferta);
  }

  /**
   * Limpia la oferta seleccionada al salir del componente
   */
  limpiarOferta(): void {
    const valorTasacionModel = new ValorTasacionModel();
    this.ofertaAceptada.next(valorTasacionModel);
  }
}

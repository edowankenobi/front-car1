import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PreguntaAppModel } from '@app/models/auto-parte-pago/pregunta.model';
import { LoggerService } from '@app/shared/services/logger.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { IdValueModel } from '@app/models/idvalue.model';
import { AppFormModel } from '@app/models/auto-parte-pago/app.form.model';
import { AppResumenModel } from '@app/models/auto-parte-pago/app.resumen.model';
import { ValueModel } from '@app/models/value.model';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) {}

  /**
   * Obtiene la ficha del vehículo en base a la patente
   */
  public preguntas(): Observable<PreguntaAppModel[]> {
    const { baseURL } = environment.restServices.autoPartePago;
    const { preguntas } = environment.restServices.autoPartePago.encuesta;

    const endpoint = `${baseURL}${preguntas}`;

    return this.httpClient.get<PreguntaAppModel[]>(endpoint);
  }
  public verifica(respuestas: IdValueModel<number, number>[]): Observable<IdValueModel<number,number>> {
    const { baseURL } = environment.restServices.autoPartePago;
    const { verifica } = environment.restServices.autoPartePago.encuesta;

    const endpoint = `${baseURL}${verifica}`;

    return this.httpClient.post<IdValueModel<number,number>>(endpoint, respuestas);
  }
  public guardar(appFormData: AppFormModel): Observable<number> {
    const { baseURL, guardar } = environment.restServices.autoPartePago;

    const endpoint = `${baseURL}${guardar}`;

    return this.httpClient.post<number>(endpoint, appFormData);
  }
  public resumen(tasacionId: number): Observable<AppResumenModel> {
    const { baseURL, resumen } = environment.restServices.autoPartePago;

    const endpoint = `${baseURL}${resumen}`;

    const body: ValueModel<number> = {
      value: tasacionId
    };

    return this.httpClient.post<AppResumenModel>(endpoint, body);
  }
  public aceptar(tasacionId: number): Observable<boolean> {
    const { baseURL, aceptar } = environment.restServices.autoPartePago;

    const endpoint = `${baseURL}${aceptar}`;

    const body: ValueModel<number> = {
      value: tasacionId
    };

    return this.httpClient.post<boolean>(endpoint, body);
  }
}

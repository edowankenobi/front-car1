import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoggerService } from '@app/shared/services/logger.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '@env/environment';
import { FiltroModel } from '@app/models/filtros/filtro.model';

@Injectable({
  providedIn: 'root'
})
export class FiltrosService {
  /**
   * Subject que escuchar el filtro seleccionado
   */
  private filtroSeleccionado = new BehaviorSubject<string>('');
  filtroSeleccionado$ = this.filtroSeleccionado.asObservable();

  /**
   * Subject que escuchar los filtros seleccionados
   */
  private filtrosSeleccionados = new BehaviorSubject<any[]>([]);
  filtrosSeleccionados$ = this.filtrosSeleccionados.asObservable();

  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) {}

  /**
   * Obtiene los filtros a mostrar en la vista de stock disponible
   */
  public filtros(): Observable<FiltroModel[]> {
    const { baseURL } = environment.restServices.strapi;
    const { filtros } = environment.restServices.strapi;

    const endpoint = `${baseURL}${filtros}`;

    return this.httpClient.get<FiltroModel[]>(endpoint);
  }

  /**
   * Almacena filtro seleccionado, listo para enviarlo.
   */
  enviarFiltro(filtro: any): void {
    this.filtroSeleccionado.next(filtro);
  }

  /**
   * Almacena los filtros seleccionados.
   */
  enviarArrayFiltros(filtro: any): void {
    this.filtrosSeleccionados.next(filtro);
  }

  /**
   * Limpia el valor almacenado en el observable.
   */
  limpiarFiltro(): void {
    this.filtroSeleccionado.next('');
  }

  /**
   * Limpia el valor almacenado en el observable.
   */
  limpiarArrayFiltros(): void {
    this.filtrosSeleccionados.next([]);
  }
}

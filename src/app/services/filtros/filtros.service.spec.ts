import { TestBed } from '@angular/core/testing';
import {FiltrosService} from './filtros.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FiltrosService', () => {
  let service: FiltrosService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(FiltrosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('filtros', () => {
    service.filtros();
    expect(service).toBeTruthy();
  });

  it('enviarFiltro', () => {
    service.enviarFiltro({});
    expect(service).toBeTruthy();
  });

  it('enviarArrayFiltros', () => {
    service.enviarArrayFiltros([{}]);
    expect(service).toBeTruthy();
  });

  it('limpiarFiltro', () => {
    service.limpiarFiltro();
    expect(service).toBeTruthy();
  });

  it('limpiarArrayFiltros', () => {
    service.limpiarArrayFiltros();
    expect(service).toBeTruthy();
  });
});

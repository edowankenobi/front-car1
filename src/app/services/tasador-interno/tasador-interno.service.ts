import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { BehaviorSubject, Observable } from 'rxjs';

import { TasadorResponseModel } from '@app/models/tasador-interno/tasador.response.model'
import { TasadorPPUModel } from '@app/models/tasador-interno/tasador.ppu.model'
import { TasadorCotizacionModel, TasadorCotizacionMethods } from '@app/models/tasador-interno/tasador.cotizacion.model';
import { TasadorIdValorModel } from '@app/models/tasador-interno/tasador.idvalor.model';
import { TasadorIdNombreModel } from '@app/models/tasador-interno/tasador.idnombre.model';

@Injectable({
  providedIn: 'root',
})
export class TasadorService {
  /**
   * Subject que escuchar la oferta aceptada
   */
  private ofertaAceptada = new BehaviorSubject<TasadorCotizacionModel | null>(null);
  ofertaAceptada$ = this.ofertaAceptada.asObservable();

  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private httpClient: HttpClient) {}

  /**
   * Obtiene los datos del vehículo por patente
   * @param placa patente del vehículo
   */
  public datosPorPatente(
    placa: string
  ): Observable<TasadorResponseModel<TasadorPPUModel>> {
    const { baseURL } = environment.restServices.tasadorInterno;
    const { patente } = environment.restServices.tasadorInterno;

    const endpoint = `${baseURL}${patente}`;

    let params: HttpParams = new HttpParams();

    params = params.append('ppu', placa);

    const httpOptions = { params };

    return this.httpClient.get<TasadorResponseModel<TasadorPPUModel>>(
      endpoint,
      httpOptions
    );
  }

  /**
   * Obtiene valor de tasación en base a datos del vehículo
   * @param idVersion id de la Versión
   * @param kilometraje kilometraje del vehiculo
   * @param captcha captcha de seguridad.
   */
  public obtenerValorTasacion(
    idVersion: number,
    idkilometraje: number,
    captcha: string,
    telefono?: string,
    correo?: string,
  ): Observable<TasadorResponseModel<TasadorCotizacionModel>> {
    const { baseURL } = environment.restServices.tasadorInterno;
    const { tasador } = environment.restServices.tasadorInterno;

    const endpoint = `${baseURL}${tasador}`;

    const postData: any = {
      kilometraje: idkilometraje,
      versionid: idVersion,
      captcha: captcha,
      telefono: telefono,
      correo: correo
    };

    return this.httpClient.post<TasadorResponseModel<TasadorCotizacionModel>>(
      endpoint,
      postData
    );
  }

  /**
   * Obtiene los valores para poblar el select de kilometraje en el formulario de tasación
   */
  public kilometrajeSeleccionable(): Observable<TasadorIdValorModel<number, string>[]> {
    const { baseURL } = environment.restServices.tasadorInterno;
    const { kilometraje } = environment.restServices.tasadorInterno;

    const endpoint = `${baseURL}${kilometraje}`;

    return this.httpClient.get<TasadorIdValorModel<number, string>[]>(endpoint );
  }

  /**
   * Obtiene el listado de marcas
   */
  public obtenerMarcas(anoid: number): Observable<TasadorResponseModel<TasadorIdNombreModel[]>> {
    const { baseURL, marca } = environment.restServices.tasadorInterno;
    const endpoint = `${baseURL}${marca}${anoid}`;

    return this.httpClient.get<TasadorResponseModel<TasadorIdNombreModel[]>>(endpoint);
  }

  /**
   * Obtiene listado de modelos según marca
   * @param idMarca id de la Marca
   */
  public obtenerModelos(
    idMarca: number
  ): Observable<TasadorResponseModel<TasadorIdNombreModel[]>> {
    const { baseURL, modelo } = environment.restServices.tasadorInterno;

    const endpoint = `${baseURL}${modelo}${idMarca}`;

    return this.httpClient.get<TasadorResponseModel<TasadorIdNombreModel[]>>(endpoint);
  }

  /**
   * Obtiene listado de años
   */
  public obtenerAnios(): Observable<TasadorResponseModel<TasadorIdNombreModel[]>> {
    const { baseURL, anio } = environment.restServices.tasadorInterno;

    const endpoint = `${baseURL}${anio}`;

    return this.httpClient.get<TasadorResponseModel<TasadorIdNombreModel[]>>(endpoint);
  }

  /**
   * Obtiene listado de versiones según modelo
   * @param idModelo id del modelo
   */
  public obtenerVersiones(
    idModelo: number
  ): Observable<TasadorResponseModel<TasadorIdNombreModel[]>> {
    const { baseURL, version } = environment.restServices.tasadorInterno;

    const endpoint = `${baseURL}${version}${idModelo}`;

     return this.httpClient.get<TasadorResponseModel<TasadorIdNombreModel[]>>(endpoint);
  }

  /**
   * Almacena la oferta seleccionada.
   */
  enviarOferta(oferta: TasadorCotizacionModel): void {
    this.ofertaAceptada.next(oferta);
  }

  /**
   * Limpia la oferta seleccionada al salir del componente
   */
  limpiarOferta(): void {
    const valorTasacionModel: TasadorCotizacionModel = TasadorCotizacionMethods.emptyTasadorCotizacionModel();
    this.ofertaAceptada.next(valorTasacionModel);
  }
}

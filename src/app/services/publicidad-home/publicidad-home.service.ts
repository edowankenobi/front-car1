import { Injectable } from '@angular/core';
import { LoggerService } from '@app/shared/services/logger.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PublicidadHome } from '@app/models/publicidad-home/publicidad-home.model';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class PublicidadHomeService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) { }

  /**
   * Obtiene las publicidades a mostrar en el banner de publicidad del home
   */
  public pubicidadHome(): Observable<PublicidadHome[]> {

    const { baseURL } = environment.restServices.strapi;
    const { publicidadHome } = environment.restServices.strapi;

    const endpoint = `${baseURL}${publicidadHome}`;

    return this.httpClient.get<PublicidadHome[]>(endpoint);
  }
}

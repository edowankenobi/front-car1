import { TestBed } from '@angular/core/testing';

import { PublicidadHomeService } from './publicidad-home.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PublicidadHomeService', () => {
  let service: PublicidadHomeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(PublicidadHomeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

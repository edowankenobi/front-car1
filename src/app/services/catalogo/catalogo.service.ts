import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoggerService } from '@app/shared/services/logger.service';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) { }

  /**
   * Obtiene los datos de todos los vehículos
   */
  public catalogoVehiculos(): Observable<FichaVehiculoModel[]> {

    const { baseURL } = environment.restServices.strapi;
    const { catalogoAutos } = environment.restServices.strapi;

    const endpoint = `${baseURL}${catalogoAutos}`;

    return this.httpClient.get<FichaVehiculoModel[]>(endpoint);
  }

  /**
   * Obtiene los datos de los vehículos según filtro
   * @param listaFiltros filtros indicados para a la petición del catálogo
   */
  public catalogoVehiculosFiltrados(
    params: HttpParams
  ): Observable<FichaVehiculoModel[]> {
    const { baseURL } = environment.restServices.strapi;
    const { catalogoAutos } = environment.restServices.strapi;

    const endpoint = `${baseURL}${catalogoAutos}`;

    const httpOptions = { params };

    return this.httpClient.get<FichaVehiculoModel[]>(endpoint, httpOptions);
  }
}

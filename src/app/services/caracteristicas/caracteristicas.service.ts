import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoggerService } from '@app/shared/services/logger.service';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { TipoModel } from '@app/models/tipo.model';

@Injectable({
  providedIn: 'root'
})
export class CaracteristicasService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) { }

  /**
   * Obtiene los tipos solicitados
   * @param tipo propiedad a solicitar
   */
  private obtenerTipo(tipo: string): Observable<TipoModel[]> {
    const { baseURL } = environment.restServices.strapi;
    const endpoint = `${baseURL}` + tipo;
    return this.httpClient.get<TipoModel[]>(endpoint);
  }

  /**
   * Método encargado de obtener los tipos de transmisión disponibles
   */
  public obtenerTipoTransmision(): Observable<TipoModel[]> {
    const { tiposTransmision } = environment.restServices.strapi;
    return this.obtenerTipo(tiposTransmision);
  }

  /**
   * Método encargado de obtener los tipos de combustible disponibles
   */
  public obtenerTipoCombustible(): Observable<TipoModel[]> {
    const { tiposCombustible } = environment.restServices.strapi;
    return this.obtenerTipo(tiposCombustible);
  }
}

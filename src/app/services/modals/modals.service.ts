import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalsService {

  /**
   * Subject que escucha las acciones del modal
   */
   private modalAction = new BehaviorSubject<boolean>(false);
   modalAction$ = this.modalAction.asObservable();

  constructor() { }

  /**
   * Almacena la acción realizada
   */
     setModalAction(modalAction: boolean): void {
      this.modalAction.next(modalAction);
    }

  /**
   * reinicia la acción realizada
   */
   limpiarModalAction(): void {
    this.modalAction.next(false);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoggerService } from '@app/shared/services/logger.service';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { MarcaModel } from '@app/models/marcas/marca.model';
import { filtrosStrapiUtil } from '@app/util/filtros-strapi.util';

@Injectable({
  providedIn: 'root'
})
export class MarcasService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) { }

  /**
   * Obtiene las primeras 9 marcas existentes en strapi
   */
  public marcasHome(): Observable<MarcaModel[]> {

    const { baseURL } = environment.restServices.strapi;
    const { marcas } = environment.restServices.strapi;
    const { limite } = filtrosStrapiUtil;
    const { limiteMarcasHome } = filtrosStrapiUtil;

    let params: HttpParams = new HttpParams();

    params = params.append(limite, limiteMarcasHome.toString());

    const httpOptions = { params };

    const endpoint = `${baseURL}${marcas}`;

    return this.httpClient.get<MarcaModel[]>(endpoint, httpOptions);
  }

  /**
   * Obtiene las marcas disponibles en Strapi
   */
  public marcas(): Observable<MarcaModel[]> {

    const { baseURL } = environment.restServices.strapi;
    const { marcas } = environment.restServices.strapi;
    const { parametroOrdenamiento } = filtrosStrapiUtil.orden;

    const endpoint = `${baseURL}${marcas}`;

    let params: HttpParams = new HttpParams();

    params = params.append(parametroOrdenamiento, 'Nombre:ASC');

    const httpOptions = { params };

    return this.httpClient.get<MarcaModel[]>(endpoint, httpOptions);
  }
}

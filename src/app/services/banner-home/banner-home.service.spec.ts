import { TestBed } from '@angular/core/testing';

import { BannerHomeService } from './banner-home.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BannerHomeService', () => {
  let service: BannerHomeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(BannerHomeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

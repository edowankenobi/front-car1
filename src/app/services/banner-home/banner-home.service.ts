import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoggerService } from '@app/shared/services/logger.service';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { BannerHome } from '@app/models/banner-home/banner-home.model';

@Injectable({
  providedIn: 'root'
})
export class BannerHomeService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) { }

  /**
   * Obtiene los parámetros del banner del home
   */
  public bannerHome(): Observable<BannerHome> {
    const { baseURL } = environment.restServices.strapi;
    const { bannerHome } = environment.restServices.strapi;

    const endpoint = `${baseURL}${bannerHome}`;

    return this.httpClient.get<BannerHome>(endpoint);
  }
}

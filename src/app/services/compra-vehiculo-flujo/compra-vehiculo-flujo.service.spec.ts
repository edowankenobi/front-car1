import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { CompraVehiculoFlujoResponse } from '@app/models/compraVehiculo/compra-vehiculo-flujo-response.model';
import { CompraVehiculoFlujo } from '@app/models/compraVehiculo/compra-vehiculo-flujo.model';
import { GuardarDetalleRequestModel } from '@app/models/tasador/guardar-detalle-request.model';
import { GuardarDetalleResponse } from '@app/models/tasador/guardar-detalle-response.model';
import { environment } from '@env/environment';

import { CompraVehiculoFlujoService } from './compra-vehiculo-flujo.service';

describe('CompraVehiculoFlujoService', () => {
  let service: CompraVehiculoFlujoService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CompraVehiculoFlujoService);
    injector = getTestBed();
    httpMock = injector.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('enviarDatosFlujoCompra', (done) => {

    let mockResponse = {} as CompraVehiculoFlujoResponse;
    service.enviarDatosFlujoCompra({} as CompraVehiculoFlujo).subscribe((response) => {
      expect(response).toBe(mockResponse);
      done();
    });
    const { baseURL, paso } = environment.restServices.pasoFlujoVenta;
    const endpoint = `${baseURL}${paso}`;
    const req = httpMock.expectOne(endpoint);
    expect(req.request.method).toBe('POST');
    req.flush(mockResponse);
  });

  it('registerDetailedData', (done) => {

    let mockResponse = {} as GuardarDetalleResponse;
    service.registerDetailedData({} as GuardarDetalleRequestModel).subscribe((response) => {
      expect(response).toBe(mockResponse);
      done();
    });
    const { baseURL, detalle } = environment.restServices.pasoFlujoVenta;
    const endpoint = `${baseURL}${detalle}`;
    const req = httpMock.expectOne(endpoint);
    expect(req.request.method).toBe('POST');
    req.flush(mockResponse);
  });

  it('getCompanyInformation', (done) => {

    let mockResponse = {} as GuardarDetalleResponse;
    service.getCompanyInformation('').subscribe((response) => {
      expect(response).toBe(mockResponse);
      done();
    });
    const { baseUrl, empresa } =
      environment.restServices.btnComprasFichaVehiculo;
 
    const endpoint = `${baseUrl}${empresa}?rut=`;
    const req = httpMock.expectOne(endpoint);
    expect(req.request.method).toBe('GET');
    req.flush(mockResponse);
  });
});

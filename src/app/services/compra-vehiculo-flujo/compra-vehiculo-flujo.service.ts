import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CompraVehiculoFlujoResponse } from '@app/models/compraVehiculo/compra-vehiculo-flujo-response.model';
import { InformacionEmpresa } from '@app/models/compraVehiculo/informacion-empresa-response.model';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { CompraVehiculoFlujo } from '../../models/compraVehiculo/compra-vehiculo-flujo.model';
import { GuardarDetalleRequestModel } from '../../models/tasador/guardar-detalle-request.model';
import { GuardarDetalleResponse } from '../../models/tasador/guardar-detalle-response.model';

@Injectable({
  providedIn: 'root',
})
export class CompraVehiculoFlujoService {
  constructor(private httpClient: HttpClient) {}

  /**
   * Envia los datos personales y los registra.
   * @param formulario datos para agendamiento.
   */
  public enviarDatosFlujoCompra(
    formulario: CompraVehiculoFlujo
  ): Observable<CompraVehiculoFlujoResponse> {
    const { baseURL, paso } = environment.restServices.pasoFlujoVenta;
    const endpoint = `${baseURL}${paso}`;
    return this.httpClient.post<CompraVehiculoFlujoResponse>(
      endpoint,
      formulario
    );
  }

  /**
   * Genera folio y registra los datos.
   * @param formulario datos para agendamiento.
   */
  public registerDetailedData(
    data: GuardarDetalleRequestModel
  ): Observable<GuardarDetalleResponse> {
    const { baseURL, detalle } = environment.restServices.pasoFlujoVenta;
    const endpoint = `${baseURL}${detalle}`;
    return this.httpClient.post<GuardarDetalleResponse>(endpoint, data);
  }

  public getCompanyInformation(rut: string): Observable<any> {
    const { baseUrl, empresa } =
      environment.restServices.btnComprasFichaVehiculo;

    const endpoint = `${baseUrl}${empresa}`;

    let params: HttpParams = new HttpParams();

    params = params.append('rut', rut);

    const httpOptions = { params };

    return this.httpClient.get<InformacionEmpresa>(endpoint, httpOptions);
  }
}

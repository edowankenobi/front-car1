import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PeticionEnvioCorreo } from '@app/models/contacto/peticion-envio-correo.model';
import { LoggerService } from '@app/shared/services/logger.service';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { RespuestaEnvioCorreo } from '@app/models/contacto/respuesta-envio-correo.model';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) { }

  /**
   * Envia los datos del formulario de contacto o cotización para envío de correo
   * @param formulario datos formulario.
   */
  public enviarFormularioContacto(
    formulario: PeticionEnvioCorreo
  ): Observable<RespuestaEnvioCorreo> {

    const endpoint = environment.restServices.formularioContacto;
    return this.httpClient.post<any>(endpoint, formulario);
  }
}

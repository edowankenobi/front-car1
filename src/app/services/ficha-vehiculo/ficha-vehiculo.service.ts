import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BotonComprasRequest } from '@app/models/compraVehiculo/boton-compras-request.model';
import { BotonCompras } from '@app/models/compraVehiculo/boton-compras-response.model';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { LoggerService } from '@app/shared/services/logger.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FichaVehiculoService {
  /**
   * Constructor
   * @param logger servicio log.
   * @param httpClient cliente http.
   */
  constructor(private logger: LoggerService, private httpClient: HttpClient) {}

  /**
   * Obtiene la ficha del vehículo en base a la patente
   */
  public fichaVehiculo(patente: string): Observable<FichaVehiculoModel[]> {
    const { baseURL } = environment.restServices.strapi;
    const { catalogoAutos } = environment.restServices.strapi;

    const endpoint = `${baseURL}${catalogoAutos}`;

    let params: HttpParams = new HttpParams();

    params = params.append('Patente', patente);

    const httpOptions = { params };

    return this.httpClient.get<FichaVehiculoModel[]>(endpoint, httpOptions);
  }

  /**
   * Envia los datos personales y los registra.
   * @param formulario datos para agendamiento.
   */
  public getCarFileState(patente: BotonComprasRequest): Observable<any> {
    const { baseUrl, estado } =
      environment.restServices.btnComprasFichaVehiculo;
    const endpoint = `${baseUrl}${estado}`;
    return this.httpClient.post<BotonCompras>(endpoint, patente);
  }
}

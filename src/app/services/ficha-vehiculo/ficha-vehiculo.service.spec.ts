import { TestBed } from '@angular/core/testing';

import { FichaVehiculoService } from './ficha-vehiculo.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FichaVehiculoService', () => {
  let service: FichaVehiculoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(FichaVehiculoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselSwipeDirective } from '@app/directives/carousel-swipe/carousel-swipe.directive';


@NgModule({
  declarations: [CarouselSwipeDirective],
  imports: [
    CommonModule
  ],
  exports: [CarouselSwipeDirective]
})
export class AppSharedModule { }

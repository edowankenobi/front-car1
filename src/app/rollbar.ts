import {
  ErrorHandler,
  Inject,
  Injectable,
  InjectionToken,
} from '@angular/core';
import { environment } from '@env/environment';
import * as Rollbar from 'rollbar';

const rollbarConfig = {
  accessToken: environment.rollbarToken,
  captureUncaught: true,
  captureUnhandledRejections: true,
};
export const RollbarService = new InjectionToken<Rollbar>('rollbar');
@Injectable()
export class RollbarErrorHandler implements ErrorHandler {
  constructor(@Inject(RollbarService) private rollbar: Rollbar) {}
  handleError(err: any): void {
    this.rollbar.error(err.originalError || err);
  }
}
export function rollbarFactory(): Rollbar {
  return new Rollbar(rollbarConfig);
}

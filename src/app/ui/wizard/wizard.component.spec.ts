import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardComponent } from './wizard.component';
import { WizardEnum } from './wizard.enum';
import { WizardModel } from './wizard.model';

describe('WizardComponent', () => {
  let component: WizardComponent;
  let fixture: ComponentFixture<WizardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WizardComponent ]
    })
    .overrideTemplate(WizardComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardComponent);
    component = fixture.componentInstance;
    component.step = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('validateReject', () => {
    component.isReject = true;
    component.validateReject();
    expect(component).toBeTruthy();
  });

  it('validateReject', () => {
    component.isReject = false;
    component.validateReject();
    expect(component).toBeTruthy();
  });

  it('handleStepSelected step 1 APROBADO', () => {
    component.type = WizardEnum.APROBADO;
    component.handleStepSelected(0);
    expect(component).toBeTruthy();
  });

  it('handleStepSelected step 1 ENCURSO', () => {
    component.type = WizardEnum.ENCURSO;
    component.allSteps = [];
    component.allSteps.push({insideStep: 0} as WizardModel)
    component.handleStepSelected(0);
    expect(component).toBeTruthy();
  });

  it('handleStepSelected step 1 RECHAZADO', () => {
    component.type = WizardEnum.RECHAZADO;
    component.allSteps = [];
    component.allSteps.push({insideStep: 0} as WizardModel)
    component.handleStepSelected(0);
    expect(component).toBeTruthy();
  });

  it('handleStepSelected step 2', () => {
    component.step = 1;
    component.allSteps = [];
    component.allSteps.push({insideStep: 2} as WizardModel)
    component.handleStepSelected(0);
    expect(component).toBeTruthy();
  });

  it('setIconCheckapproved', () => {
    component.step = 1;
    component.type = WizardEnum.APROBADO;
    component.allSteps = [];
    component.allSteps.push({insideStep: 1, typeStep: WizardEnum.APROBADO} as WizardModel)
    component.setIconCheckapproved();
    expect(component).toBeTruthy();
  });

  it('setIconCheckapproved APROBADO', () => {
    component.step = 2;
    component.type = WizardEnum.APROBADO;
    component.allSteps = [];
    component.allSteps.push({insideStep: 1, typeStep: WizardEnum.APROBADO} as WizardModel)
    component.setIconCheckapproved();
    expect(component).toBeTruthy();
  });

  it('setIconCheckapproved ENCURSO', () => {
    component.step = 2;
    component.type = WizardEnum.ENCURSO;
    component.allSteps = [];
    component.allSteps.push({insideStep: 1, typeStep: WizardEnum.APROBADO} as WizardModel)
    component.setIconCheckapproved();
    expect(component).toBeTruthy();
  });

  it('setIconCheckapproved RECHAZADO', () => {
    component.step = 2;
    component.type = WizardEnum.RECHAZADO;
    component.allSteps = [];
    component.allSteps.push({insideStep: 1, typeStep: WizardEnum.APROBADO} as WizardModel)
    component.setIconCheckapproved();
    expect(component).toBeTruthy();
  });
});

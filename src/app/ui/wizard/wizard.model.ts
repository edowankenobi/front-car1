import { WizardEnum } from './wizard.enum';

/**
 * Modelo para la caja principal de landing y solicitud retiro
 */
export class WizardModel {
  /**
   * Subtitulo de un paso
   */
  subTitle?: string;
  /**
   * Ruta de imagen de paso
   */
  insideIcon?: string;

  /**
   * Paso
   */
  insideStep!: number;

  /**
   * Tipo de paso
   */
  typeStep?: WizardEnum = WizardEnum.BASE;
}

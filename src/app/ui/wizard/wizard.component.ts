import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WizardEnum } from './wizard.enum';
import { WizardModel } from './wizard.model';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardComponent implements OnInit {
  /**
   * modelo de datos de wizard
   */
  allSteps!: WizardModel[];

  /**
   * paso actual
   */
  @Input()
  step!: number;

  /**
   * tipo
   */
  @Input()
  type!: number;

  /**
   * Es rechazado
   */
  @Input()
  isReject = false;

  /**
   * Flag que indica si un paso esta activo.
   */
  @Input() active = false;

  /**
   * emitter que maneja el paso seleccionado
   */
  @Output() stepSelected = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {
    this.initStepProgress();
    this.setIconCheckapproved();
  }

  initStepProgress(): void {
    this.allSteps = [
      {
        subTitle: 'Datos personales',
        insideStep: 1,
        insideIcon: 'icon-nro_1',
      },
      {
        subTitle: 'Auto en parte de pago',
        insideStep: 2,
        insideIcon: 'icon-nro_2',
      },
      {
        subTitle: 'Financiamiento',
        insideStep: 3,
        insideIcon: 'icon-nro_3',
      },
      {
        subTitle: 'Servicios complementarios',
        insideStep: 4,
        insideIcon: 'icon-nro_4',
      },
      {
        subTitle: 'Forma de entrega',
        insideStep: 5,
        insideIcon: 'icon-nro_5',
      },
    ];
  }

  validateReject(): void {
    if (this.isReject) {
      this.allSteps[4].insideIcon = 'icon-close';
      this.allSteps[4].typeStep = WizardEnum.RECHAZADO;
    } else {
      this.allSteps.pop();
    }
  }

  handleStepSelected($event: number): void {
    this.allSteps.forEach((item) => {
      if (
        this.step > item.insideStep &&
        (this.type === WizardEnum.APROBADO ||
          this.type === WizardEnum.ENCURSO ||
          this.type === WizardEnum.RECHAZADO)
      ) {
        this.stepSelected.emit($event);
      }
    });
  }

  setIconCheckapproved(): void {
    this.allSteps.forEach((item) => {
      if (this.step === item.insideStep) {
        item.typeStep = this.type;
        if (item.typeStep === WizardEnum.APROBADO) {
          item.insideIcon = 'icon-wizard-check';
        }
      }
      if (
        this.step > item.insideStep &&
        (this.type === WizardEnum.APROBADO ||
          this.type === WizardEnum.ENCURSO ||
          this.type === WizardEnum.RECHAZADO)
      ) {
        item.typeStep = WizardEnum.APROBADO;
        item.insideIcon = 'icon-wizard-check';
      }
    });
  }
}

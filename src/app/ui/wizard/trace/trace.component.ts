import { Component, EventEmitter, Input, Output } from '@angular/core';
import { WizardEnum } from '../wizard.enum';
import { WizardModel } from '../wizard.model';

@Component({
  selector: 'app-trace',
  templateUrl: './trace.component.html',
  styleUrls: ['./trace.component.scss'],
})
export class TraceComponent {
  /**
   * Posicion de item del arreglo
   */
  @Input() index!: number;

  /**
   * modelo de datos de wizard
   */
  @Input()
  content!: WizardModel;

  /**
   * flag que indica si es el primer elemento dentro de la iteracion.
   */
  @Input()
  isFirst!: boolean;

  /**
   * flag que indica si es el ultimo elemento dentro de la iteracion.
   */
  @Input()
  isLast!: boolean;

  /**
   * flag que indica si paso esta activado
   */
  @Input()
  active!: boolean;

  wizardEnum = WizardEnum;

  /**
   * emitter que maneja el paso seleccionado
   */
  @Output() stepPosition = new EventEmitter<number>();

  constructor() {}

  emitPosition(): void {
    this.stepPosition.emit(this.index);
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TraceComponent } from './trace.component';

@NgModule({
  declarations: [TraceComponent],
  imports: [CommonModule],
  exports: [TraceComponent],
})
export class TraceModule {}

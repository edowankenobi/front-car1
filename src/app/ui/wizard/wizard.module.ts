import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TraceModule } from './trace/trace.module';
import { WizardComponent } from './wizard.component';

@NgModule({
  declarations: [WizardComponent],
  imports: [CommonModule, TraceModule],
  exports: [WizardComponent],
})
export class WizardModule {}

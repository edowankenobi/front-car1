import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RangeComponent } from './range.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('RangeComponent', () => {
  let component: RangeComponent;
  let fixture: ComponentFixture<RangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RangeComponent],
      imports: [HttpClientTestingModule]
    })
      .overrideTemplate(RangeComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

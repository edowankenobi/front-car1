import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSliderModule } from '@angular-slider/ngx-slider';

import { RangeComponent } from './range.component';
import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from 'ng2-currency-mask';



@NgModule({
  declarations: [RangeComponent],
  imports: [
    CommonModule,
    NgxSliderModule,
    FormsModule,
    CurrencyMaskModule
  ],
  exports: [RangeComponent]
})
export class RangeModule { }

import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, Input, OnInit } from '@angular/core';
import {} from '@angular/forms';
import { Options } from '@angular-slider/ngx-slider';
import { RangeOptions } from '../../models/filtros/range/range-options.model';
import { FiltrosService } from '../../services/filtros/filtros.service';

@Component({
  selector: 'app-range',
  templateUrl: './range.component.html',
  styleUrls: ['./range.component.scss']
})
export class RangeComponent implements OnInit {

  @Input() opciones: RangeOptions;

  minValue: number;
  maxValue: number;
  options: Options;

  constructor(private filtrosService: FiltrosService) {
    this.opciones = {
      origen: '',
      titulo: '',
      minValue: 0,
      maxValue: 0,
      floor: 0,
      ceil: 0,
      step: 0
    };
    this.minValue = 0;
    this.maxValue = 0;
    this.options = {};
  }

  ngOnInit(): void {
    registerLocaleData( es );
    this.minValue = this.opciones.minValue;
    this.maxValue = this.opciones.maxValue;
    this.options = {
      floor: this.opciones.floor,
      ceil: this.opciones.ceil,
      step: this.opciones.step,
      enforceStep: false,
      enforceRange: false,
    };
  }

  enviarValor(): void{
    this.filtrosService.enviarFiltro({
      origen: this.opciones.origen,
      min: this.minValue,
      max: this.maxValue
    });
  }
}

import { Location, registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/app.reducer';
import { HoraModel } from '@app/models/agenda/hora.model';
import { TasadorCotizacionModel } from '@app/models/tasador-interno/tasador.cotizacion.model';
//import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
declare const ga: any;

@Component({
  selector: 'app-exito-ui',
  templateUrl: './exito-ui.component.html',
  styleUrls: ['./exito-ui.component.scss'],
})
export class ExitoUiComponent implements OnInit {


  @Input() horaBlock! : HoraModel;


  horarioElegido: any
  hora: any
  fechaHoraInicio: any
  /**
   * Datos de la tasación
   */
  @Input() datosTasacion!: TasadorCotizacionModel;

  /**
   * Formulario de datos del agendamiento
   */
  @Input() datosPersonalesForm!: FormGroup;

  /**
   * Emmiter de evento de vuelta al formulario
   */
  @Output() pasoTasacion = new EventEmitter<string>();

  /**
   * gatilla registro de datos al redireccionar al home
   */
  @Output() triggetSaveData = new EventEmitter<boolean>();

  @Input() confirmarCita = new EventEmitter();

  @Input() agendamientoForm!: FormGroup;
  userSubcription!: Subscription;
  data: any;
  time: any;
  fecha: any;

  constructor(private route: Router,  private store: Store <AppState>, private location: Location) {
    this.location.replaceState("auto-confirmacion-inspeccion")
    this.location.onUrlChange((url, state) => {
      if(typeof ga == "function"){
        ga('set', 'page', ''+url);
        ga('send', 'pageview' );
      }
    });
  }

  ngOnInit(): void {
    registerLocaleData(es);
    this.userSubcription = this.store.select('user').subscribe(x => this.data = x);
    const x = Object.keys(this.data).map(x => ({ type: x, value: this.data[x] }));
    this.data = x;
    console.log(this.data)
    const time = this.data[3].value
    this.time = time;
    console.log(time)
    const fecha = this.data[4].value
    this.fecha = fecha

  }

  /**
   * Método que redirige al Home
   */
  irHome(): void {
    this.triggetSaveData.emit();
  }

  test(): void {
    alert( this.agendamientoForm.controls.inicio.value)
    debugger;

  }
  /*Redirige a Stock */
  goHome(): void {
    this.triggetSaveData.emit();
    this.route.navigate(['/stock-disponible'])
  }

  /**
   * Emite el evento de regreso al formulario de tasación
   */
  volverATasar(): void {
    this.pasoTasacion.emit('exitoSolicitud');
  }
}

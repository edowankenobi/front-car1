import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExitoUiComponent } from './exito-ui.component';



@NgModule({
  declarations: [ExitoUiComponent],
  imports: [
    CommonModule
  ],
  exports: [ExitoUiComponent]
})
export class ExitoUiModule { }

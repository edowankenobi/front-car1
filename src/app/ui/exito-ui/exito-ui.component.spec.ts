import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitoUiComponent } from './exito-ui.component';

describe('ExitoUiComponent', () => {
  let component: ExitoUiComponent;
  let fixture: ComponentFixture<ExitoUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExitoUiComponent ]
    })
    .overrideTemplate(ExitoUiComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitoUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

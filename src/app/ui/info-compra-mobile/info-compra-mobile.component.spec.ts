import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoCompraMobileComponent } from './info-compra-mobile.component';

describe('InfoCompraComponent', () => {
  let component: InfoCompraMobileComponent;
  let fixture: ComponentFixture<InfoCompraMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoCompraMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoCompraMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { InfoCompraMobileComponent } from '@app/ui/info-compra-mobile/info-compra-mobile.component';

@NgModule({
  declarations: [InfoCompraMobileComponent],
  exports: [InfoCompraMobileComponent]
})
export class InfoCompraMobileModule {}

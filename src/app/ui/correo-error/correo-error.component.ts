import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-correo-error',
  templateUrl: './correo-error.component.html',
  styleUrls: ['./correo-error.component.scss']
})
export class CorreoErrorComponent implements OnInit {

  constructor(private bsModalRef: BsModalRef) { }

  ngOnInit(): void {
  }

  /**
   * Método que cierra el modal
   */
  closeModal(): void{
    this.bsModalRef.hide();
  }

}

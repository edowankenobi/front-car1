import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CorreoErrorComponent } from './correo-error.component';



@NgModule({
  declarations: [CorreoErrorComponent],
  imports: [
    CommonModule
  ],
  exports: [CorreoErrorComponent]
})
export class CorreoErrorModule { }

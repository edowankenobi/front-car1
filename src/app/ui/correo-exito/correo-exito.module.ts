import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CorreoExitoComponent } from './correo-exito.component';



@NgModule({
  declarations: [CorreoExitoComponent],
  imports: [
    CommonModule
  ],
  exports: [CorreoExitoComponent]
})
export class CorreoExitoModule { }

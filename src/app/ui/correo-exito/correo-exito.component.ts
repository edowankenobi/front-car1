import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-correo-exito',
  templateUrl: './correo-exito.component.html',
  styleUrls: ['./correo-exito.component.scss'],
})
export class CorreoExitoComponent implements OnInit {
  /**
   * Correo del cliente ingresado en el formulario
   */
  @Input() emailCliente!: string;

  constructor(private bsModalRef: BsModalRef, private router: Router) {}

  ngOnInit(): void {}

  /**
   * Método que cierra el modal
   */
  closeModal(): void {
    this.bsModalRef.hide();
    this.router.navigate(['/']);
  }
}

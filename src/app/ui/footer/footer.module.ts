import { NgModule } from '@angular/core';
import { FooterComponent } from '@app/ui/footer/footer.component';
import { FooterRoutingModule } from '@app/ui/footer/footer-routing.module';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [FooterComponent],
  imports: [FooterRoutingModule, CommonModule],
  exports: [FooterComponent,CommonModule ]
})
export class FooterModule {}

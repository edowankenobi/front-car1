import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockDisponibleComponent } from '@app/pages/stock-disponible/stock-disponible.component';
import { ContactoGeneralComponent } from '@app/pages/contacto-general/contacto-general.component';
import { SobreNosotrosComponent } from '@app/pages/sobre-nosotros/sobre-nosotros.component';
import { HomeComponent } from '@app/pages/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'stock-disponible', component: StockDisponibleComponent },
  { path: 'contacto', component: ContactoGeneralComponent },
  { path: 'sobre-nosotros', component: SobreNosotrosComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })
  ],
  exports: [RouterModule]
})
export class FooterRoutingModule { }

import { Component, Input, OnInit } from '@angular/core';
import { MethodsUtil } from '@app/util/methods.util';
import { environment } from '@env/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  constructor() {}

  isMobile: boolean = MethodsUtil.isMobile();
  telCompra: any = [];
  randomVende: any[] = [];


  ngOnInit(): void {
    this.telCompra = environment.telefonos.compra[0];
    this.randomVende = MethodsUtil.randomArray(environment.telefonos.vende);
  }

  /**
   * Metodo que maneja el redireccionamiento de las redes sociales.
   */
  doRedirect(event: string): void {
    switch (event) {
      case 'insta':
        window.open('https://www.instagram.com/car1_chile/', '_blank');
        break;
      case 'facebook':
        window.open('https://www.facebook.com/car1chile/', '_blank');
        break;
      case 'linkedin':
        window.open('https://www.linkedin.com/company/car1chile/', '_blank');
        break;
      default:
        break;
    }
  }

  @Input() esMobile = this.isMobile;
}

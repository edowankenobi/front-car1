import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoSucursalMobileComponent } from './info-sucursal-mobile.component';

describe('InfoCompraComponent', () => {
  let component: InfoSucursalMobileComponent;
  let fixture: ComponentFixture<InfoSucursalMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoSucursalMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoSucursalMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

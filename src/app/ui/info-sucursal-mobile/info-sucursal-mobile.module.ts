import { NgModule } from '@angular/core';
import { InfoSucursalMobileComponent } from '@app/ui/info-sucursal-mobile/info-sucursal-mobile.component';

@NgModule({
  declarations: [InfoSucursalMobileComponent],
  exports: [InfoSucursalMobileComponent]
})
export class InfoSucursalMobileModule {}

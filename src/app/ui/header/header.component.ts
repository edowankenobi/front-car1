import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MethodsUtil } from '@app/util/methods.util';
import { environment } from '@env/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('btnMenu') btnMenu!: ElementRef;
  @ViewChild('navMenu') navMenu!: ElementRef;
  isMobile: boolean = MethodsUtil.isMobile();

  get sitioPrivadoUrl(): string {
    return environment.flujoCompra.baseUrl + environment.flujoCompra.login;
  }

  constructor(private router: Router, private elementRef: ElementRef) {
    router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        if (
          this.navMenu.nativeElement.getAttribute('class').indexOf('show') > -1
        ) {
          this.btnMenu.nativeElement.click();
        }
      }
    });
  }

  ngOnInit(): void {}
}

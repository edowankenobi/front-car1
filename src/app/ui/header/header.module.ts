import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { ContactBarComponent } from './contact-bar/contact-bar.component';
import { HeaderRoutingModule } from '@app/ui/header/header-routing.module';

@NgModule({
  declarations: [HeaderComponent, ContactBarComponent],
  imports: [HeaderRoutingModule, CommonModule],
  exports: [HeaderComponent]
})
export class HeaderModule {}

import { Component, OnInit } from '@angular/core';
import { MethodsUtil } from '@app/util/methods.util'

@Component({
  selector: 'app-contact-bar',
  templateUrl: './contact-bar.component.html',
  styleUrls: ['./contact-bar.component.scss']
})
export class ContactBarComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }
  scrollContacto(): void {
    MethodsUtil.instanScroll('container-ayuda');
  }
}

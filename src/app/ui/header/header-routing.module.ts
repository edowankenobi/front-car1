import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactoGeneralComponent } from '@app/pages/contacto-general/contacto-general.component';
import { HomeComponent } from '@app/pages/home/home.component';
import { SobreNosotrosComponent } from '@app/pages/sobre-nosotros/sobre-nosotros.component';
import { StockDisponibleComponent } from '@app/pages/stock-disponible/stock-disponible.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'stock-disponible', component: StockDisponibleComponent },
  { path: 'contacto', component: ContactoGeneralComponent },
  { path: 'sobre-nosotros', component: SobreNosotrosComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class HeaderRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalsService } from '@app/services/modals/modals.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LoadingService } from '../../../services/loading/loading.service';

@Component({
  selector: 'app-modal-quiero-vender',
  templateUrl: './modal-quiero-vender.component.html',
  styleUrls: ['./modal-quiero-vender.component.scss'],
})
export class ModalQuieroVenderComponent implements OnInit {
  /**
   * Modelo de datos de modal vender
   */
  modalHome!: boolean;

  constructor(
    private router: Router,
    public bsModalRef: BsModalRef,
    private modalsService: ModalsService,
    private loadingService: LoadingService
  ) {}

  ngOnInit(): void {}

  /**
   * Metodo que cierra el modal
   */
  closeModal(): void {
    this.modalsService.setModalAction(false);
    this.bsModalRef.hide();
  }

  redirectToTasador(): void {
    this.closeModal();
    this.router.navigateByUrl('/venta-vehiculo');
  }
  redirectToStock(): void {
    this.closeModal();
    this.router.navigateByUrl('/stock-disponible');
  }
}

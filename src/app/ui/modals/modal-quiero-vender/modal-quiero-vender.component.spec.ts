import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { BsModalRef, ModalModule } from 'ngx-bootstrap/modal';
import { ModalQuieroVenderComponent } from './modal-quiero-vender.component';

describe('ModalQuieroVenderComponent', () => {
  let component: ModalQuieroVenderComponent;
  let fixture: ComponentFixture<ModalQuieroVenderComponent>;
  const routerMock = {
    url: '/venta-vehiculo',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalQuieroVenderComponent],
      imports: [HttpClientTestingModule, ModalModule.forRoot()],
      providers: [BsModalRef, { provide: Router, useValue: routerMock }],
    })
      .overrideTemplate(ModalQuieroVenderComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalQuieroVenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

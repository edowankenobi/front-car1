import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TasadorModule } from '@app/pages/tasador/tasador.module';
import { CorreoErrorModule } from '../correo-error/correo-error.module';
import { CorreoExitoModule } from '../correo-exito/correo-exito.module';
import { ModalQuieroVenderComponent } from './modal-quiero-vender/modal-quiero-vender.component';
import { PartePagoModalComponent } from './parte-pago-modal/parte-pago-modal.component';
import { RespuestaEnvioCorreoComponent } from './respuesta-envio-correo/respuesta-envio-correo.component';

@NgModule({
  declarations: [
    PartePagoModalComponent,
    RespuestaEnvioCorreoComponent,
    ModalQuieroVenderComponent,
  ],
  imports: [CommonModule, TasadorModule, CorreoExitoModule, CorreoErrorModule],
  exports: [
    PartePagoModalComponent,
    RespuestaEnvioCorreoComponent,
    ModalQuieroVenderComponent,
  ],
})
export class ModalsModule {}

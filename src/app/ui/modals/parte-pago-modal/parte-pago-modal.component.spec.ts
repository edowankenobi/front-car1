import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { PartePagoModalComponent } from './parte-pago-modal.component';

describe('PartePagoModalComponent', () => {
  let component: PartePagoModalComponent;
  let fixture: ComponentFixture<PartePagoModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartePagoModalComponent ],
      providers: [
        BsModalRef
      ]
    })
    .overrideTemplate(PartePagoModalComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartePagoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

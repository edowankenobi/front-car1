import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ModalsService } from '@app/services/modals/modals.service';
import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';

@Component({
  selector: 'app-parte-pago-modal',
  templateUrl: './parte-pago-modal.component.html',
  styleUrls: ['./parte-pago-modal.component.scss']
})
export class PartePagoModalComponent implements OnInit {

  /**
   * Objeto que contiene la oferta realizada
   */
  oferta!: ValorTasacionModel;

  /**
   * Indica si es flujo de venta vehículo
   */
  esVenta: boolean;

  constructor(public bsModalRef: BsModalRef, private modalsService: ModalsService) {
    this.esVenta = false;
  }

  ngOnInit(): void { }

  /**
   * Método que cierra el modal
   */
  closeModal(): void {
    if (Object.entries(this.oferta).length === 0) {
      this.modalsService.setModalAction(false);
    }
    this.bsModalRef.hide();
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-respuesta-envio-correo',
  templateUrl: './respuesta-envio-correo.component.html',
  styleUrls: ['./respuesta-envio-correo.component.scss']
})
export class RespuestaEnvioCorreoComponent implements OnInit {

  /**
   * Bandera que indica que modal levantar
   */
  isSuccess!: boolean;

  /**
   * Atributo que contiene el correo del cliente recibido desde el formulario de contacto
   */
  emailCliente!: string;

  constructor() { }

  ngOnInit(): void {}

}

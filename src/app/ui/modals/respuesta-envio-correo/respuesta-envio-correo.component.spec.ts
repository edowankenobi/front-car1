import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RespuestaEnvioCorreoComponent } from './respuesta-envio-correo.component';

describe('RespuestaEnvioCorreoComponent', () => {
  let component: RespuestaEnvioCorreoComponent;
  let fixture: ComponentFixture<RespuestaEnvioCorreoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RespuestaEnvioCorreoComponent ]
    })
      .overrideTemplate(RespuestaEnvioCorreoComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RespuestaEnvioCorreoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ErrorUiComponent } from './error-ui.component';

describe('ErrorUiComponent', () => {
  let component: ErrorUiComponent;
  let fixture: ComponentFixture<ErrorUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErrorUiComponent ],
      imports: [RouterTestingModule]
    })
    .overrideTemplate(ErrorUiComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

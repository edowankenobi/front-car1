import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorUiComponent } from './error-ui.component';



@NgModule({
  declarations: [ErrorUiComponent],
  imports: [
    CommonModule
  ],
  exports: [ErrorUiComponent]
})
export class ErrorUiModule { }

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-ui',
  templateUrl: './error-ui.component.html',
  styleUrls: ['./error-ui.component.scss']
})
export class ErrorUiComponent implements OnInit {

  /**
   * Emmiter de evento para ir al paso confirmar
   */
  @Output() confirmarCita = new EventEmitter();

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  /**
   * Método que redirige al Home
   */
  irHome(): void {
    this.router.navigate(['/']);
  }

  /**
   * Emite el evento de confirmar proceso de venta vehículo
   */
  confirmar(): void {
    this.confirmarCita.emit();
  }
}

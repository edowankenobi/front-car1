import { NgModule } from '@angular/core';
import { InfoCompraComponent } from '@app/ui/info-compra/info-compra.component';

@NgModule({
  declarations: [InfoCompraComponent],
  exports: [InfoCompraComponent]
})
export class InfoCompraModule {}

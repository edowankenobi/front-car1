import { FichaVehiculoModel } from "@app/models/ficha-vehiculo/ficha-vehiculo.model";
import { FiltraCatalogoUtil } from "./filtra-catalogo.util";
import { filtrosStrapiUtil } from "./filtros-strapi.util";

describe('CompraVehiculoFlujoService', () => {
    const vehiculo: FichaVehiculoModel = {
        tipo_de_vehiculo: { Nombre: 'ABC'},
        marca: { Nombre: 'ABC'},
        tipo_de_transmision: { Tipo: 'ABC'},
        tipo_de_combustible: { Tipo: 'ABC'},
        Precio: '1000',
        AnioFabricacion: '2020',
        Kilometraje: '2000'
    } as FichaVehiculoModel;
    const vehiculoB: FichaVehiculoModel = {
        tipo_de_vehiculo: { Nombre: 'ABC'},
        marca: { Nombre: 'ABC'},
        tipo_de_transmision: { Tipo: 'ABC'},
        tipo_de_combustible: { Tipo: 'ABC'},
        Precio: '1200',
        AnioFabricacion: '2021',
        Kilometraje: '3000'
    } as FichaVehiculoModel;
    it('filtrarVehiculos tipoVehiculo', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.tipoVehiculo.nombreMenu, nombre: 'ABC'});
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(1);
    });
    it('filtrarVehiculos tipoVehiculo NOK', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.tipoVehiculo.nombreMenu, nombre: 'ABC'});
        vehiculosTotales.push({} as FichaVehiculoModel);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos marca', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.marca.nombreMenu, nombre: 'ABC'});
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(1);
    });
    it('filtrarVehiculos marca NOK', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.marca.nombreMenu, nombre: 'ABC'});
        vehiculosTotales.push({} as FichaVehiculoModel);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos transmision', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.transmision.nombreMenu, nombre: 'ABC'});
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(1);
    });
    it('filtrarVehiculos transmision NOK', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.transmision.nombreMenu, nombre: 'ABC'});
        vehiculosTotales.push({} as FichaVehiculoModel);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos combustible', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.combustible.nombreMenu, nombre: 'ABC'});
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(1);
    });
    it('filtrarVehiculos combustible NOK', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.combustible.nombreMenu, nombre: 'ABC'});
        vehiculosTotales.push({} as FichaVehiculoModel);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos precio', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.precio.nombreMenu, min: '1', max: '2'});
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos precio NOK', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.precio.nombreMenu, min: '1', max: '2'});
        vehiculosTotales.push({} as FichaVehiculoModel);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos anio', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.anio.nombreMenu, min: '1', max: '2'});
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos anio NOK', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.anio.nombreMenu, min: '1', max: '2'});
        vehiculosTotales.push({} as FichaVehiculoModel);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos kilometros', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.kilometros.nombreMenu, min: '1', max: '2'});
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos kilometros NOK', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.tabs.kilometros.nombreMenu, min: '1', max: '2'});
        vehiculosTotales.push({} as FichaVehiculoModel);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(0);
    });
    it('filtrarVehiculos origen precio ascendiente up', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.precio.ascendiente});
        vehiculosTotales.push(vehiculo);
        vehiculosTotales.push(vehiculoB);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen precio ascendiente down', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.precio.ascendiente});
        vehiculosTotales.push(vehiculoB);
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen precio descendiente up', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.precio.descendiente});
        vehiculosTotales.push(vehiculo);
        vehiculosTotales.push(vehiculoB);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen precio descendiente down', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.precio.descendiente});
        vehiculosTotales.push(vehiculoB);
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen kilometraje ascendiente up', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.kilometraje.ascendiente});
        vehiculosTotales.push(vehiculo);
        vehiculosTotales.push(vehiculoB);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen kilometraje ascendiente down', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.kilometraje.ascendiente});
        vehiculosTotales.push(vehiculoB);
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen kilometraje descendiente up', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.kilometraje.descendiente});
        vehiculosTotales.push(vehiculo);
        vehiculosTotales.push(vehiculoB);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen kilometraje descendiente down', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.kilometraje.descendiente});
        vehiculosTotales.push(vehiculoB);
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen fecha ascendiente up', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.fecha.ascendiente});
        vehiculosTotales.push(vehiculo);
        vehiculosTotales.push(vehiculoB);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen fecha ascendiente down', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.fecha.ascendiente});
        vehiculosTotales.push(vehiculoB);
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen fecha descendiente up', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.fecha.descendiente});
        vehiculosTotales.push(vehiculo);
        vehiculosTotales.push(vehiculoB);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });
    it('filtrarVehiculos origen fecha descendiente down', () => {
        const filtrosSeleccionados = [];
        const vehiculosTotales: FichaVehiculoModel[] = [];
        filtrosSeleccionados.push({origen: filtrosStrapiUtil.orden.origen, nombre: filtrosStrapiUtil.orden.fecha.descendiente});
        vehiculosTotales.push(vehiculoB);
        vehiculosTotales.push(vehiculo);
        expect(FiltraCatalogoUtil.filtrarVehiculos(filtrosSeleccionados, vehiculosTotales).length).toEqual(2);
    });

});

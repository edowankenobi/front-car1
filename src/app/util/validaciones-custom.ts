import { AbstractControl } from '@angular/forms';


export class ValidacionesCustom {
    /**
     * Método que valida patente antigua, formato AA1111
     * @param patente patente a validar.
     */
    static validaPatenteAntigua(patente: AbstractControl): { [key: string]: boolean } | null {
        if (patente && patente.value && Object.keys(patente).length) {
            const regex1 = new RegExp('^[A-Za-z]{2}[0-9]{4}$');
            if (regex1.test(patente.value)) {
                return { patenteAntigua: true };
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}

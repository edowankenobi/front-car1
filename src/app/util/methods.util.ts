export class MethodsUtil {
    static instanScroll(classSelector: string): void {
      let elements = document.getElementsByClassName(classSelector);
      if (elements != null && elements != undefined) {
        elements[0].scrollIntoView();
      }
    }
    static instanScrollById(id: string): void {
      let element = document.getElementById(id);
      if (element != null && element != undefined) {
        element.scrollIntoView();
      }
    }
    static isMobile(): boolean {
      return window.screen.width <= 575;
    }

    static screenWidth(): number {
      //575?
      return window.screen.width;
    }
    static randomArray(array: any[]): any[] {
      if(array.length <= 1) {
        return array;
      }
      var currentIndex = array.length, temporaryValue, randomIndex;
      while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }
      return array;
    }
    static generateTimePath(): Number {
      return new Date().getTime();
    }
    static validateTimePath(time1: number, time2: number): boolean {
      // dif en ms
      const result = ((time2 - time1) / 1000);
      return result <= 30 && result >= 0;
    }
} 
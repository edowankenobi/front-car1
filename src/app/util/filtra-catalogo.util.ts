import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { filtrosStrapiUtil } from '@app/util/filtros-strapi.util';
/**
 * Clase utilitaria de parseo de parámetros
 */
export class FiltraCatalogoUtil {
  public static filtrarVehiculos(
    filtrosSeleccionados: any[],
    vehiculosTotales: FichaVehiculoModel[]
  ): FichaVehiculoModel[] {
    let vehiculosFiltrados: FichaVehiculoModel[] = vehiculosTotales;
    for (const filtro of filtrosSeleccionados) {
      switch (filtro.origen) {
        case filtrosStrapiUtil.tabs.tipoVehiculo.nombreMenu: {
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return vehiculo.tipo_de_vehiculo?.Nombre === filtro.nombre;
          });
          break;
        }
        case filtrosStrapiUtil.tabs.marca.nombreMenu: {
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return vehiculo.marca?.Nombre === filtro.nombre;
          });
          break;
        }
        case filtrosStrapiUtil.tabs.transmision.nombreMenu: {
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return vehiculo.tipo_de_transmision?.Tipo === filtro.nombre;
          });
          break;
        }
        case filtrosStrapiUtil.tabs.combustible.nombreMenu: {
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return vehiculo.tipo_de_combustible?.Tipo === filtro.nombre;
          });
          break;
        }
        case filtrosStrapiUtil.tabs.precio.nombreMenu: {
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return Number(vehiculo.Precio) >= Number(filtro.min);
          });
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return Number(vehiculo.Precio) <= Number(filtro.max);
          });
          break;
        }
        case filtrosStrapiUtil.tabs.anio.nombreMenu: {
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return Number(vehiculo.AnioFabricacion) >= Number(filtro.min);
          });
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return Number(vehiculo.AnioFabricacion) <= Number(filtro.max);
          });
          break;
        }
        case filtrosStrapiUtil.tabs.kilometros.nombreMenu: {
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return Number(vehiculo.Kilometraje) >= Number(filtro.min);
          });
          vehiculosFiltrados = vehiculosFiltrados.filter((vehiculo) => {
            return Number(vehiculo.Kilometraje) <= Number(filtro.max);
          });
          break;
        }
        case filtrosStrapiUtil.orden.origen: {
          switch (filtro.nombre) {
            case filtrosStrapiUtil.orden.precio.ascendiente: {
              vehiculosFiltrados.sort((a, b) =>
                Number(a.Precio) > Number(b.Precio) ? 1 : -1
              );
              break;
            }
            case filtrosStrapiUtil.orden.precio.descendiente: {
              vehiculosFiltrados.sort((a, b) =>
                Number(a.Precio) < Number(b.Precio) ? 1 : -1
              );
              break;
            }
            case filtrosStrapiUtil.orden.kilometraje.ascendiente: {
              vehiculosFiltrados.sort((a, b) =>
                Number(a.Kilometraje) > Number(b.Kilometraje) ? 1 : -1
              );
              break;
            }
            case filtrosStrapiUtil.orden.kilometraje.descendiente: {
              vehiculosFiltrados.sort((a, b) =>
                Number(a.Kilometraje) < Number(b.Kilometraje) ? 1 : -1
              );
              break;
            }
            case filtrosStrapiUtil.orden.fecha.ascendiente: {
              vehiculosFiltrados.sort((a, b) => {
                return (
                  <any>new Date(b.published_at!) -
                  <any>new Date(a.published_at!)
                );
              });
              break;
            }
            case filtrosStrapiUtil.orden.fecha.descendiente: {
              vehiculosFiltrados.sort((a, b) => {
                return (
                  <any>new Date(a.published_at!) -
                  <any>new Date(b.published_at!)
                );
              });
              break;
            }
          }
        }
      }
    }
    return vehiculosFiltrados;
  }
}

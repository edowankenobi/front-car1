export const filtrosStrapiUtil = {
  limite: '_limit',
  limiteMarcasHome: 9,
  busqueda: '_where[_or]',
  rango: '_where',
  orden: {
    parametroOrdenamiento: '_sort',
    origen: 'orden',
    precio: {
      origen: 'precioOrden',
      ascendiente: 'Precio:ASC',
      descendiente: 'Precio:DESC'
    },
    kilometraje: {
      origen: 'kilometrajeOrden',
      ascendiente: 'Kilometraje:ASC',
      descendiente: 'Kilometraje:DESC'
    },
    fecha: {
      origen: 'fechaOrden',
      ascendiente: 'published_at:ASC',
      descendiente: 'published_at:DESC'
    },
    kilometrajeTasacion: {
      ascendiente: 'Valor:ASC',
      descendiente: 'Valor:DESC'
    }
  },
  tabs: {
    marca: {
      nombreMenu: 'marca',
      nombreURL: '[marca.Nombre]'
    },
    tipoVehiculo: {
      nombreMenu: 'tipo',
      nombreURL: '[tipo_de_vehiculo.Nombre]'
    },
    transmision: {
      nombreMenu: 'transmision',
      nombreURL: '[tipo_de_transmision.Tipo]'
    },
    combustible: {
      nombreMenu: 'combustible',
      nombreURL: '[tipo_de_combustible.Tipo]'
    },
    precio: {
      nombreMenu: 'precio',
      mayorIgual: '[Precio_gte]',
      menorIgual: '[Precio_lte]'
    },
    anio: {
      nombreMenu: 'anio',
      mayorIgual: '[AnioFabricacion_gte]',
      menorIgual: '[AnioFabricacion_lte]'
    },
    kilometros: {
      nombreMenu: 'kilometros',
      mayorIgual: '[Kilometraje_gte]',
      menorIgual: '[Kilometraje_lte]'
    }
  }
};

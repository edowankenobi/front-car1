import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ContactoGeneralModule } from '@app/pages/contacto-general/contacto-general.module';
import { FichaVehiculoIframeModule } from '@app/pages/ficha-vehiculo-iframe/ficha-vehiculo-iframe.module';
import { FichaVehiculoModule } from '@app/pages/ficha-vehiculo/ficha-vehiculo.module';
import { SobreNosotrosModule } from '@app/pages/sobre-nosotros/sobre-nosotros.module';
import { StockDisponibleModule } from '@app/pages/stock-disponible/stock-disponible.module';
import { TasadorModule } from '@app/pages/tasador/tasador.module';
import { VentaVehiculoModule } from '@app/pages/venta-vehiculo/venta-vehiculo.module';
import { FooterModule } from '@app/ui/footer/footer.module';
import { environment } from '@env/environment';
import { StoreModule } from '@ngrx/store';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxGoogleAnalyticsModule } from 'ngx-google-analytics';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './pages/home/home.module';
import { LandingSellInModule } from './pages/landing-sell-in/landing-sell-in.module';
import { LandingSellOutModule } from './pages/landing-sell-out/landing-sell-out.module';
import { NuevoTasadorModule } from './pages/nuevo-tasador/nuevo-tasador.module';
// import { RollbarErrorHandler, rollbarFactory, RollbarService } from './rollbar';
import { LoadingService } from './services/loading/loading.service';
import { HeaderModule } from './ui/header/header.module';
import { RangeModule } from './ui/range/range.module';
import { SpinnerComponent } from './util/spinner/spinner.component';
import { appReducers } from './app.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { PruebasModule } from './pages/pruebas/pruebas.module';
import { AppSharedModule } from './app.shared.module';
import { FullViewImageMobileModule } from '@app/pages/full-view-image-mobile/full-view-image-mobile.module';
registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [AppComponent, SpinnerComponent],
  imports: [
    AppSharedModule,
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    FichaVehiculoModule,
    FichaVehiculoIframeModule,
    StockDisponibleModule,
    HeaderModule,
    FooterModule,
    HttpClientModule,
    RangeModule,
    ContactoGeneralModule,
    SobreNosotrosModule,
    VentaVehiculoModule,
    TasadorModule,
    NuevoTasadorModule,
    LandingSellInModule,
    LandingSellOutModule,
    FullViewImageMobileModule,
    PruebasModule,
    ModalModule.forRoot(),
    NgxGoogleAnalyticsModule.forRoot(environment.gtagManager.id),
    StoreModule.forRoot(appReducers),
    StoreDevtoolsModule.instrument({
      logOnly: environment.production, // Restrict extension to log-only mode
      
    }), 
    
  ],
  providers: [
    LoadingService,
    DatePipe,
    // { provide: ErrorHandler, useClass: RollbarErrorHandler },
    // { provide: RollbarService, useFactory: rollbarFactory },
    { provide: LOCALE_ID, useValue: 'es' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

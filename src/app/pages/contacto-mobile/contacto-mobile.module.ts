import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactoMobileComponent } from '@app/pages/contacto-mobile/contacto-mobile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TasadorModule } from '@app/pages/tasador/tasador.module';
import { ModalsModule } from '@app/ui/modals/modals.module';

@NgModule({
  declarations: [ContactoMobileComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TasadorModule,
    ModalsModule,
    
  ],
  exports: [ContactoMobileComponent]
})
export class ContactoMobileModule {}

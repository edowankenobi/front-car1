import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-tabs-ficha-iframe',
  templateUrl: './tabs-ficha-iframe.component.html',
  styleUrls: ['./tabs-ficha-iframe.component.scss']
})
export class TabsFichaIframeComponent implements OnInit {
  constructor() { }

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  get existenDocumentos(): boolean {
    return (this.fichaVehiculo.documentos_auto?.length || 0) > 0;
  }

  ngOnInit(): void { }
}

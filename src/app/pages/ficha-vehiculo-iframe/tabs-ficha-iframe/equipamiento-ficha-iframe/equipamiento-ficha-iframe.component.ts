import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-equipamiento-ficha-iframe',
  templateUrl: './equipamiento-ficha-iframe.component.html',
  styleUrls: ['./equipamiento-ficha-iframe.component.scss']
})
export class EquipamientoFichaIframeComponent implements OnInit {
  constructor() { }

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void { }
}

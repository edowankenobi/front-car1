import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipamientoFichaIframeComponent } from './equipamiento-ficha-iframe.component';

describe('EquipamientoFichaIframeComponent', () => {
  let component: EquipamientoFichaIframeComponent;
  let fixture: ComponentFixture<EquipamientoFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipamientoFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipamientoFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

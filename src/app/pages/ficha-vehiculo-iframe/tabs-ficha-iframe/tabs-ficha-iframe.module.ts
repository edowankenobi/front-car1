import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsFichaIframeComponent } from '@app/pages/ficha-vehiculo-iframe/tabs-ficha-iframe/tabs-ficha-iframe.component';
import { DetalleFichaIframeComponent } from '@app/pages/ficha-vehiculo-iframe/tabs-ficha-iframe/detalle-ficha-iframe/detalle-ficha-iframe.component';
import { EquipamientoFichaIframeComponent } from '@app/pages/ficha-vehiculo-iframe/tabs-ficha-iframe/equipamiento-ficha-iframe/equipamiento-ficha-iframe.component';
import { InformeFichaIframeComponent } from '@app/pages/ficha-vehiculo-iframe/tabs-ficha-iframe/informe-ficha-iframe/informe-ficha-iframe.component';

@NgModule({
  declarations: [
    TabsFichaIframeComponent,
    DetalleFichaIframeComponent,
    EquipamientoFichaIframeComponent,
    InformeFichaIframeComponent
  ],
  imports: [CommonModule],
  exports: [TabsFichaIframeComponent]
})
export class TabsFichaIframeModule { }

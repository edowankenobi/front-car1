import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleFichaIframeComponent } from './detalle-ficha-iframe.component';

describe('DetalleFichaIframeComponent', () => {
  let component: DetalleFichaIframeComponent;
  let fixture: ComponentFixture<DetalleFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

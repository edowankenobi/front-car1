import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-detalle-ficha-iframe',
  templateUrl: './detalle-ficha-iframe.component.html',
  styleUrls: ['./detalle-ficha-iframe.component.scss']
})
export class DetalleFichaIframeComponent implements OnInit {
  constructor() { }

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void { }
}

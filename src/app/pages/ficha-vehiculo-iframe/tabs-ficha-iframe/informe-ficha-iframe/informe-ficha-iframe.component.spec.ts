import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformeFichaIframeComponent } from './informe-ficha-iframe.component';

describe('InformeFichaIframeComponent', () => {
  let component: InformeFichaIframeComponent;
  let fixture: ComponentFixture<InformeFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformeFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformeFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

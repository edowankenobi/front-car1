import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { TipoTModel } from '@app/models/tipoT.model';
import { environment } from '@env/environment';

@Component({
  selector: 'app-informe-ficha-iframe',
  templateUrl: './informe-ficha-iframe.component.html',
  styleUrls: ['./informe-ficha-iframe.component.scss']
})
export class InformeFichaIframeComponent implements OnInit {
  constructor() { }

  @Input() fichaVehiculo: FichaVehiculoModel = {};
  enlaces: TipoTModel<string, string>[] = [];

  ngOnInit(): void {
    this.cargaDocs();
  }
  private cargaDocs(): void {
    environment.documentos.informes.tipos
      .filter((value) => this.fichaVehiculo.documentos_auto?.find(c => c.id == value.id) != undefined)
      .sort(c => c.orden)
      .forEach(c => this.enlaces.push({
        val1: c.nombre,
        val2: `${environment.documentos.informes.baseURL}${c.path}${c.prefijo}${this.fichaVehiculo.Patente}${c.extension}`
      }));
  }
}
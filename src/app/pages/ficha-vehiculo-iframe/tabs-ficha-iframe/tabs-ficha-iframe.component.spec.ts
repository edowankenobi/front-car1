import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsFichaIframeComponent } from './tabs-ficha-iframe.component';

describe('TabsFichaIframeComponent', () => {
  let component: TabsFichaIframeComponent;
  let fixture: ComponentFixture<TabsFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabsFichaIframeComponent ]
    })
      .overrideTemplate(TabsFichaIframeComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

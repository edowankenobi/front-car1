import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoPartePagoComponent } from './auto-parte-pago.component';

describe('AutoPartePagoComponent', () => {
  let component: AutoPartePagoComponent;
  let fixture: ComponentFixture<AutoPartePagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoPartePagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoPartePagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { environment } from '@env/environment';
import { TasadorService } from '@app/services/tasador/tasador.service';
import { KilometrajeTasadorModel } from '@app/models/tasador/kilometraje-tasador.model';
import { MarcaTasadorModel } from '@app/models/tasador/marca-tasador.model';
import { ModeloTasadorModel } from '@app/models/tasador/modelo-tasador.model';
import { VersionTasadorModel } from '@app/models/tasador/patente/version-tasador.model';
import { AppService } from '@app/services/auto-parte-pago/auto-parte-pago.service';
import { PreguntaAppModel } from '@app/models/auto-parte-pago/pregunta.model';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { IdValueModel } from '@app/models/idvalue.model';
import { Observable } from 'rxjs';
import { EnumApp } from '@app/models/enum/enums.model';
import { NgRecaptcha3Service } from 'ng-recaptcha3';
import { LoadingService } from '@app/services/loading/loading.service';
import { AppResumenModel } from '@app/models/auto-parte-pago/app.resumen.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { RespuestaEnvioCorreoComponent } from '@app/ui/modals/respuesta-envio-correo/respuesta-envio-correo.component';
import { AceptaApp } from '@app/models/auto-parte-pago/app.acepta.model';

@Component({
  selector: 'app-auto-parte-pago',
  templateUrl: './auto-parte-pago.component.html',
  styleUrls: ['./auto-parte-pago.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 1500, noPause: false, showIndicators: false, indicatorsByChunk: false, noWrapSlides: false } }
 ]
})
export class AutoPartePagoComponent implements OnInit {

 activeSlide: number = 0;
 bsModalRef!: BsModalRef;

  constructor(
    public tasador: TasadorService,
    public appService: AppService,
    private recaptcha3: NgRecaptcha3Service,
    private modalService: BsModalService,
    private loadingService: LoadingService
  ) { }

  esbusquedaPatente: boolean = false;
  selectPatente: string = "";
  infoError: string = "";

  selectAgno: number = -1;
  selectKm: number = -1;
  selectMarca: number = -1;
  selectModelo: number = -1;
  selectVersion: number = -1;

  agnos: number[] = [];
  kilometraje: KilometrajeTasadorModel[] = [];
  marcas: MarcaTasadorModel[] = [];
  modelos: ModeloTasadorModel[] = [];
  versiones: VersionTasadorModel[] = [];

  preguntas: PreguntaAppModel[] = [];
  respuestas: IdValueModel<number, number>[] = [];
  disableBtn: boolean = false;

  vista: EnumApp = new EnumApp;
  vistaDisplay: number = this.vista.formulario;

  tasacionAppId: number = 0;
  resumen: AppResumenModel = {};

  esPrenda: boolean = false;
  appAceptada: boolean = false;

  set cambiarVista(idVista: number) {
    this.vistaDisplay = idVista;
  }

  @Output() aceptaAppEmit = new EventEmitter();

  ngOnInit(): void {
    this.inicia();
  }
  inicia(): void {
    this.checkChangeData('todos');
    this.getKilometraje();
  }
  changeAnio(): void {
    this.getMarca();
    this.checkChangeData("anio");
  }
  changeMarca(): void {
    this.getModelo();
    this.checkChangeData("marca");
  }
  changeModelo(): void {
    this.getVersion();
    this.checkChangeData("modelo");
  }
  getAnio(): void {
    this.tasador.obtenerAnios(0).subscribe(data => {
      if(data.codigo == "200") {
        this.agnos = data.agno;
      }
    });
  }
  getKilometraje(): void {
    this.tasador.kilometrajeSeleccionable().subscribe(data => {
      if(data != null && data != undefined && data.length) {
        this.kilometraje = data;
      }
    });
  }
  getMarca(): void {
    this.loadingService.show(true);
    this.tasador.obtenerMarcas(this.selectAgno.toString()).subscribe(data => {
      if(data.codigo == "200") {
        this.marcas = data.marcas;
        this.selectMarca = -1;
        this.loadingService.hide();
      }
    });
  }
  getModelo(): void {
    this.loadingService.show(true);
    this.tasador.obtenerModelos(this.selectMarca, this.selectAgno.toString()).subscribe(data => {
      if(data.codigo == "200") {
        this.modelos = data.modelos;
        this.selectModelo = -1;
        this.loadingService.hide();
      }
    });
  }
  getVersion(): void {
    this.loadingService.show(true);
    this.tasador.obtenerVersiones(this.selectModelo, this.selectAgno).subscribe(data => {
      if(data.codigo == "200") {
        this.versiones = data.versiones.filter(c => c.id != 0);
        this.selectVersion = -1;
        this.loadingService.hide();
      }
    });
  }
  checkChangeData(type: string): void {
    if (type == "todos") {
      if(this.agnos.length < 2) {
        this.getAnio();
      }
      if(this.preguntas.length <= 0 || this.respuestas.length > 0) {
        this.obtenerPreguntas();
      }
      this.selectAgno = -1;
      this.selectKm = -1;
      this.selectPatente = "";
      this.respuestas = [];
      this.activeSlide = 0;
      this.esPrenda = false;
      this.esbusquedaPatente = false;
      if(this.vistaDisplay !== this.vista.formulario) {
        this.cambiarVista = this.vista.formulario;
      }
    }
    if (type == "anio" || type == "todos") {
      if(this.selectAgno == -1) {
        this.selectMarca = -1;
        this.marcas = [];
      }
    }
    if (type == "anio" || type == "marca" || type == "todos") {
      this.selectModelo = -1;
      this.selectVersion = -1;
      this.modelos = [];
      this.versiones = [];
    }
    if (type == "modelo") {
      this.selectVersion = -1;
      this.versiones = [];
    }
  }
  consultarPatente(): void {
      this.esbusquedaPatente = true;
      this.infoError = "";
      if (this.selectPatente.length < 6) {
        this.esbusquedaPatente = false;
        this.infoError = "Ingresa una placa patente";
        return;
      }
      this.loadingService.show(true);
      this.tasador.datosPorPatente(this.selectPatente).subscribe(
      data => {
        if (data.codigo == "200") {
          this.agnos = [data.datosPorPlaca.year];
          this.selectAgno = data.datosPorPlaca.year;

          this.marcas.push({
            id: data.datosPorPlaca.brandID,
            nombre: data.datosPorPlaca.brandName
          });
          this.selectMarca = data.datosPorPlaca.brandID;

          this.modelos.push({
            id: data.datosPorPlaca.modelID,
            nombre: data.datosPorPlaca.modelName
          });
          this.selectModelo = data.datosPorPlaca.modelID;

          data.datosPorPlaca.versions.forEach((version) => {
            if(version.id == 0) return;
            this.versiones.push({
              id: version.id,
              nombre: version.nombre,
              anio: 0,
              cilindrada: 0,
              idModelo: 0,
              popularidad: 0,
              tipoCombustible: 0,
              traccion: 0,
              transmision: 0
            });
          });
        } else {
          this.esbusquedaPatente = false;
          this.infoError = "No se encontró información para la patente";
        }
        this.loadingService.hide();
      },
      error => {
        this.esbusquedaPatente = false;
        this.controlError(error);
      });
  }
  limpiarPatente(): void {
    this.checkChangeData("todos");
  }
  obtenerPreguntas(): void {
    this.appService.preguntas().subscribe(data => {
      this.preguntas = data;
    });
  }
  ultimaAlternativa(): boolean {
    const next = this.activeSlide + 1;
    return next >= this.preguntas.length;
  }
  encuestaCompletada(): boolean {
    return this.preguntas.length > 0 && this.preguntas.length === this.respuestas.length;
  }
  datosCompletos(): boolean {
    return this.selectVersion > 0 && this.selectKm > 0
          && this.selectAgno > 0 && this.selectMarca > -1
          && this.selectModelo > 0;
  }
  nextSlide(): void {
    //verifica seleccion de alternativa
    if(this.respuestas.length - 1 == this.activeSlide) {
      this.disableBtn = true;
      this.verificaRespuesta().subscribe(data => {
        if (data.value === 4) {
          // si manejarExcepcion es verdadero interrumpe la operación
          if (this.manejarExcepcion(data.id)) return;
        }
        if(data.value === 1 || data.value === 4) {
          //verifica ultima alternativa
          if(!this.ultimaAlternativa()) {
            this.activeSlide = this.activeSlide + 1;
          }
        } else {
          console.log("Encuesta Rechazada");
          this.cambiarVista = this.vista.rechaza;
        }
        this.disableBtn = false;
      });
    }
  }
  seleccionOpcion(idPregunta: number, idAlternativa: number): void {
    const seleccion = this.respuestas.find(c => c.id == idPregunta);
    if(seleccion != null || seleccion !== undefined) {
      this.respuestas = this.respuestas.filter(c => c.id !== seleccion.id);
    }
    this.respuestas.push({ id: idPregunta, value: idAlternativa });
  }
  verificaRespuesta(): Observable<IdValueModel<number,number>> {
    return this.appService.verifica(this.respuestas);
  }
  manejarExcepcion(idPregunta: number): boolean {
    let retorno = false;
    switch (idPregunta) {
      case 6: //vehiculo en prenda
        this.esPrenda = true;
      break;
      default:
        console.log("excepcion no controlada: " + idPregunta);
        retorno = true;
      break;
    }
    return retorno;
  }
  obtenerValorTasador(): void {
    if(!this.datosCompletos()){
      console.log('Completar datos');
      return;
    }
    this.loadingService.show(true);
    this.recaptcha3.getToken().then((captcha: string) => {
      this.tasador.obtenerValorTasacion(
        this.selectMarca,
        this.selectModelo,
        this.selectAgno,
        this.selectVersion,
        0,
        this.selectKm.toString(),
        captcha
      ).subscribe(
        respuesta => {
          if (respuesta.codigo == '200') {
            this.guardarApp(respuesta.tasacionVehiculo.valorTasacion);
          } else {
            this.controlError('Error tasacion');
          }
          console.log('Tasado', respuesta);
        });
    })
    .catch(error => {
      this.controlError(error);
    });
  }
  guardarApp(valorTasacion?: number): void {
    const marca = this.marcas.find(c => c.id == this.selectMarca);
    const modelo = this.modelos.find(c => c.id == this.selectModelo);
    const version = this.versiones.find(c => c.id == this.selectVersion);

    if(marca === undefined || modelo === undefined || version === undefined || valorTasacion === undefined) {
      this.controlError(['error de datos; marca modelo version valorTasacion', marca, modelo, version, valorTasacion]);
      return;
    }

    this.appService.guardar({
      patente: this.selectPatente != '' ? this.selectPatente : undefined,
      anio: this.selectAgno,
      marca: marca.nombre,
      marcaId: marca.id,
      modelo: modelo.nombre,
      modeloId: modelo.id,
      version: version.nombre,
      versionId: version.id,
      kilometraje: this.selectKm,
      origenId: environment.restServices.autoPartePago.origen,
      valorTasacion: valorTasacion,
      encuesta: this.respuestas
    }).subscribe(
      tasacionAppId => {
      this.tasacionAppId = tasacionAppId;
      this.obtenerResumen();
      },
      error => {
        this.controlError(error);
      }
    );
  }
  obtenerResumen(): void {
    this.appService.resumen(this.tasacionAppId)
    .subscribe(
    resumen => {
      this.resumen = resumen;
      this.cambiarVista = this.vista.oferta;
      //TODO: hacer scroll
      this.loadingService.hide();
      this.scroll('scroll-view');
    },
    error => {
      this.controlError(error);
    });
  }
  aceptarApp(): void {
    this.loadingService.show(true);
    this.appService.aceptar(this.tasacionAppId).subscribe(
      resp => {
        if (!resp) {
          this.controlError("aceptación service error");
          return;
        }
        this.appAceptada = true;
        const values: AceptaApp = {
          tasacionAppId: this.tasacionAppId,
          resumen: this.resumen
        };
        this.aceptaAppEmit.emit(values);
        this.loadingService.hide();
      },
      error => {
        this.controlError(error);
      }
    );
  }
  controlError(error: any, hideLoading: boolean = true): void {
    console.log(error);
    if(hideLoading) {
      this.loadingService.hide();
    }
    this.showModalError();
  }
  showModalError(): void {
    const initialState = {
      isSuccess: false,
      emailCliente: '',
    };
    this.bsModalRef = this.modalService.show(
      RespuestaEnvioCorreoComponent,
      {
        initialState,
        backdrop: 'static',
        keyboard: false,
        class: 'modal-lg',
      }
    );
  }
  scroll(classSelector: string): void {
    let elements = document.getElementsByClassName(classSelector);
    if (elements != null && elements != undefined) {
      elements[0].scrollIntoView();
    }
  }
}

import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { environment } from '@env/environment';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MethodsUtil } from '@app/util/methods.util';

@Component({
  selector: 'app-carrusel-ficha-iframe',
  templateUrl: './carrusel-ficha-iframe.component.html',
  styleUrls: ['./carrusel-ficha-iframe.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 5000, pauseOnFocus: true, noPause: false, showIndicators: true, indicatorsByChunk: false, noWrapSlides: false } }
 ]
})
export class CarruselFichaIframeComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  @Input() fichaVehiculo: FichaVehiculoModel = {};
  slides: string[] = [];
  activeSlide: number = 0;

  ngOnInit(): void {
  }
  ngOnChanges(changes: SimpleChanges) {
    const fichaVeh: FichaVehiculoModel = changes.fichaVehiculo.currentValue;
    if (fichaVeh != undefined && fichaVeh != null) {
      this.generateSlides();
    }
  }
  generateSlides(): void {
    const nfotos = this.fichaVehiculo.NFotos ?? 0;
    if (nfotos <= 0) return;
    for (let i = 0; i < nfotos; i++){
      const img = `${environment.fotos.baseURL}${this.fichaVehiculo.Patente}/${environment.fotos.nombre.replace('{number}', i.toString())}`;
      this.slides.push(img);
    }
  }
  redirectFullView(): void {
    this.router.navigate([`/photo/${this.fichaVehiculo.Patente}/${this.getStringNumber(this.fichaVehiculo.NFotos ?? 0)}${this.getStringNumber(this.activeSlide)}${MethodsUtil.generateTimePath()}`]);
  }
  getStringNumber(numb: number): string {
    let stringNumber = numb.toString();
    if(stringNumber.length == 1) {
      return '0' + stringNumber;
    }
    return stringNumber;
  }
}

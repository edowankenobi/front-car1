import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarruselFichaIframeComponent } from './carrusel-ficha-iframe.component';

describe('CarruselFichaIframeComponent', () => {
  let component: CarruselFichaIframeComponent;
  let fixture: ComponentFixture<CarruselFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarruselFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarruselFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

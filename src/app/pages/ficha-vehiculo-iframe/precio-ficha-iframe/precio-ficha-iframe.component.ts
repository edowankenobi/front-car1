import { Component, Input, OnInit } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { MethodsUtil } from '@app/util/methods.util';

@Component({
  selector: 'app-precio-ficha-iframe',
  templateUrl: './precio-ficha-iframe.component.html',
  styleUrls: ['./precio-ficha-iframe.component.scss']
})
export class PrecioFichaIframeComponent implements OnInit {

  @Input() fichaVehiculo: FichaVehiculoModel = {};
  isMobile: boolean = MethodsUtil.isMobile();
  
  constructor() { }

  ngOnInit(): void {
  }

  get isFinanced(): boolean {
    return this.fichaVehiculo.CuotaMonto != null
      && this.fichaVehiculo.Cuotas != null
      && this.fichaVehiculo.CAE != null
      && this.fichaVehiculo.CTC != null
  }

  get pie(): string {
    return "40% de pie";
  }

}

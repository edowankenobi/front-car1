import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecioFichaIframeComponent } from './precio-ficha-iframe.component';

describe('PrecioFichaIframeComponent', () => {
  let component: PrecioFichaIframeComponent;
  let fixture: ComponentFixture<PrecioFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrecioFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecioFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

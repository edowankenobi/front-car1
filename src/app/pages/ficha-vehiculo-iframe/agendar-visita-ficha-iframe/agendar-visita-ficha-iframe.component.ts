import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';

@Component({
  selector: 'app-agendar-visita-ficha-iframe',
  templateUrl: './agendar-visita-ficha-iframe.component.html',
  styleUrls: ['./agendar-visita-ficha-iframe.component.scss']
})
export class AgendarVisitaFichaIframeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  agendaHubspot(): void {
    window.open(environment.hubspot.agenda, '_blank');
  }
}

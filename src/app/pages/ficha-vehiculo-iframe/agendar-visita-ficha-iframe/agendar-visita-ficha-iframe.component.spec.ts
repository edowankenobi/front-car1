import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendarVisitaFichaIframeComponent } from './agendar-visita-ficha-iframe.component';

describe('AgendarVisitaFichaIframeComponent', () => {
  let component: AgendarVisitaFichaIframeComponent;
  let fixture: ComponentFixture<AgendarVisitaFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgendarVisitaFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendarVisitaFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

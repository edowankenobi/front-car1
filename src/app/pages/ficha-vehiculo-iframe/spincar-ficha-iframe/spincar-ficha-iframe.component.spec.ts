import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpincarFichaIframeComponent } from './spincar-ficha-iframe.component';

describe('SpincarFichaIframeComponent', () => {
  let component: SpincarFichaIframeComponent;
  let fixture: ComponentFixture<SpincarFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpincarFichaIframeComponent ]
    })
      .overrideTemplate(SpincarFichaIframeComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpincarFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

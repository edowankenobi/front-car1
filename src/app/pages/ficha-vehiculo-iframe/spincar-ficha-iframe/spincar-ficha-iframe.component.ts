import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-spincar-ficha-iframe',
  templateUrl: './spincar-ficha-iframe.component.html',
  styleUrls: ['./spincar-ficha-iframe.component.scss']
})
export class SpincarFichaIframeComponent implements OnInit, OnChanges {



  constructor() {
  }

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  content = '';


  ngOnInit(): void {

  }

  ngOnChanges(): void {
    if (this.fichaVehiculo.Patente) {
      this.content = '<iframe src="https://spins.spincar.com/car1chile/'
        + this.fichaVehiculo.Patente ?.toLowerCase()
        + '" frameborder="0" width="100%" height="100%" allowfullscreen="allowfullscreen"'
        + '></iframe>';
    }
  }
}

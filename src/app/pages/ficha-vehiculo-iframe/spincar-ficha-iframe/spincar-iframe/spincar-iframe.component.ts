import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-spincar-iframe',
  templateUrl: './spincar-iframe.component.html',
  styleUrls: ['./spincar-iframe.component.scss']
})
export class SpincarIframeComponent implements OnInit {

  innerContent: any;

  @Input('content')
  set setContent(content: string) {
    this.innerContent = this.sanitizer.bypassSecurityTrustHtml(content);
  }
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
  }

}

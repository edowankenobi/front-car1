import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpincarIframeComponent } from './spincar-iframe.component';

describe('SpincarIframeComponent', () => {
  let component: SpincarIframeComponent;
  let fixture: ComponentFixture<SpincarIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpincarIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpincarIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

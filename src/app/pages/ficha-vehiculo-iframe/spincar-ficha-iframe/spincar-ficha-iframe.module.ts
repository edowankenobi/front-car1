import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpincarIframeComponent } from '@app/pages/ficha-vehiculo-iframe/spincar-ficha-iframe/spincar-iframe/spincar-iframe.component';
import { SpincarFichaIframeComponent } from '@app/pages/ficha-vehiculo-iframe/spincar-ficha-iframe/spincar-ficha-iframe.component';



@NgModule({
  declarations: [SpincarFichaIframeComponent, SpincarIframeComponent],
  imports: [
    CommonModule
  ],
  exports: [SpincarFichaIframeComponent]
})
export class SpincarFichaIframeModule { }

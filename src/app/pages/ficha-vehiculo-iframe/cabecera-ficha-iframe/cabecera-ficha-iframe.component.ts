import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, Input, OnInit } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { MethodsUtil } from '@app/util/methods.util';
import { environment } from '@env/environment';

@Component({
  selector: 'app-cabecera-ficha-iframe',
  templateUrl: './cabecera-ficha-iframe.component.html',
  styleUrls: ['./cabecera-ficha-iframe.component.scss'],
})
export class CabeceraFichaIframeComponent implements OnInit {
  constructor() {}

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};
  isMobile: boolean = MethodsUtil.isMobile();

  ngOnInit(): void {
    registerLocaleData(es);
  }

  get urlCompra(): string {
    return environment.flujoCompra.baseUrl + environment.flujoCompra.comprar + this.fichaVehiculo.Patente;
  }

  get isFinanced(): boolean {
    return this.fichaVehiculo.CuotaMonto != null
      && this.fichaVehiculo.Cuotas != null
      && this.fichaVehiculo.CAE != null
      && this.fichaVehiculo.CTC != null
  }

 get pie(): string {
   return "40% de pie";
 }

  irContacto(): void {
    const contactoRef = document.getElementById('contacto-ficha');
    if (contactoRef) {
      contactoRef.scrollIntoView({ behavior: 'smooth' });
    }
  }
}

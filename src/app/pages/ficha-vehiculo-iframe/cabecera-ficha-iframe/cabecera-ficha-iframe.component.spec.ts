import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CabeceraFichaIframeComponent } from './cabecera-ficha-iframe.component';

describe('CabeceraFichaIframeComponent', () => {
  let component: CabeceraFichaIframeComponent;
  let fixture: ComponentFixture<CabeceraFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CabeceraFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CabeceraFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

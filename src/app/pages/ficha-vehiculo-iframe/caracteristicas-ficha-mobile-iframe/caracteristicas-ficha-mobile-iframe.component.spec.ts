import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracteristicasFichaMobileIframeComponent } from './caracteristicas-ficha-mobile-iframe.component';

describe('CaracteristicasFichaIframeComponent', () => {
  let component: CaracteristicasFichaMobileIframeComponent;
  let fixture: ComponentFixture<CaracteristicasFichaMobileIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaracteristicasFichaMobileIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaracteristicasFichaMobileIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-caracteristicas-ficha-mobile-iframe',
  templateUrl: './caracteristicas-ficha-mobile-iframe.component.html',
  styleUrls: ['./caracteristicas-ficha-mobile-iframe.component.scss']
})
export class CaracteristicasFichaMobileIframeComponent implements OnInit {

  public textoInspeccion: string;
  public garantia: string;
  constructor() {
    this.textoInspeccion ="en mas de 160 puntos";
    this.garantia = "por 6 meses";
   }
  
  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void { registerLocaleData(es); }

  get aplicaGarantia():boolean {  
    return parseInt(this.fichaVehiculo?.AnioFabricacion || '0') >= 2015;
   } 
  
   get sistemaDeAire(): boolean{
     return this.fichaVehiculo.sistema_de_aire != undefined && this.fichaVehiculo.sistema_de_aire != null;
   }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracteristicasFichaIframeComponent } from './caracteristicas-ficha-iframe.component';

describe('CaracteristicasFichaIframeComponent', () => {
  let component: CaracteristicasFichaIframeComponent;
  let fixture: ComponentFixture<CaracteristicasFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaracteristicasFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaracteristicasFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FichaVehiculoIframeComponent } from '@app/pages/ficha-vehiculo-iframe/ficha-vehiculo-iframe.component';
import { CabeceraFichaIframeComponent } from '@app/pages/ficha-vehiculo-iframe/cabecera-ficha-iframe/cabecera-ficha-iframe.component';
import { DescripcionFichaIframeComponent } from '@app/pages/ficha-vehiculo-iframe/descripcion-ficha-iframe/descripcion-ficha-iframe.component';
import { CaracteristicasFichaIframeComponent } from '@app/pages/ficha-vehiculo-iframe/caracteristicas-ficha-iframe/caracteristicas-ficha-iframe.component';
import { CaracteristicasFichaMobileIframeComponent } from '@app/pages/ficha-vehiculo-iframe/caracteristicas-ficha-mobile-iframe/caracteristicas-ficha-mobile-iframe.component';
import { InfoCompraModule } from '@app/ui/info-compra/info-compra.module';
import { InfoCompraMobileModule } from '@app/ui/info-compra-mobile/info-compra-mobile.module';
import { ContactoModule } from '@app/pages/contacto/contacto.module';
import { ContactoMobileModule } from '@app/pages/contacto-mobile/contacto-mobile.module';
import { TabsFichaIframeModule } from '@app/pages/ficha-vehiculo-iframe/tabs-ficha-iframe/tabs-ficha-iframe.module';
import { SpincarFichaIframeModule } from '@app/pages/ficha-vehiculo-iframe/spincar-ficha-iframe/spincar-ficha-iframe.module';
import { FinanciamientoIframeComponent } from './financiamiento-iframe/financiamiento-iframe.component';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { AutoPartePagoComponent } from './auto-parte-pago/auto-parte-pago.component';
import { AgendarVisitaFichaIframeComponent } from './agendar-visita-ficha-iframe/agendar-visita-ficha-iframe.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { SolicitudFormComponent } from './financiamiento-iframe/solicitud-form/solicitud-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CarruselFichaIframeComponent } from './carrusel-ficha-iframe/carrusel-ficha-iframe.component';
import { PrecioFichaIframeComponent } from './precio-ficha-iframe/precio-ficha-iframe.component';
import { ButtonMobileFichaIframeComponent } from './button-mobile-ficha-iframe/button-mobile-ficha-iframe.component';
import { InfoSucursalMobileModule } from '@app/ui/info-sucursal-mobile/info-sucursal-mobile.module';
import { AppSharedModule } from '@app/app.shared.module';

@NgModule({
  declarations: [
    FichaVehiculoIframeComponent,
    CabeceraFichaIframeComponent,
    DescripcionFichaIframeComponent,
    CaracteristicasFichaIframeComponent,
    CaracteristicasFichaMobileIframeComponent,
    FinanciamientoIframeComponent,
    AutoPartePagoComponent,
    AgendarVisitaFichaIframeComponent,
    SolicitudFormComponent,
    CarruselFichaIframeComponent,
    PrecioFichaIframeComponent,
    ButtonMobileFichaIframeComponent
  ],
  imports: [
    CommonModule,
    InfoCompraModule, 
    InfoCompraMobileModule,
    TabsFichaIframeModule, 
    ContactoModule, 
    ContactoMobileModule,
    InfoSucursalMobileModule,
    SpincarFichaIframeModule, 
    CollapseModule, 
    FormsModule, 
    TextMaskModule, 
    CarouselModule,
    ReactiveFormsModule,
    AppSharedModule]
})
export class FichaVehiculoIframeModule { }

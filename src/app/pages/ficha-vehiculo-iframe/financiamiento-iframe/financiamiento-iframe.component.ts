import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { environment } from '@env/environment';
//import { AbstractControl } from '@angular/forms';
//import { formatNumber } from '@angular/common';
import { FinanciamientoConfigModel } from '@app/models/financiamientoConfig.model';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { FinanciamientoService } from '@app/services/financiamiento/financiamiento.service';
import { FinanciamientoModel } from '@app/models/financiamiento/financiamiento.model';
import { LoadingService } from '@app/services/loading/loading.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { RespuestaEnvioCorreoComponent } from '@app/ui/modals/respuesta-envio-correo/respuesta-envio-correo.component';
import { FinanciamientoResponseModel } from '@app/models/financiamiento/financiamiento.response.model';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { AceptaApp } from '@app/models/auto-parte-pago/app.acepta.model';

const numberMask = createNumberMask({
  prefix: '$',
  suffix: '',
  thousandsSeparatorSymbol: '.'
});

@Component({
  selector: 'app-financiamiento-iframe',
  templateUrl: './financiamiento-iframe.component.html',
  styleUrls: ['./financiamiento-iframe.component.scss']
})
export class FinanciamientoIframeComponent implements OnInit {
  public mask = numberMask;
  bsModalRef!: BsModalRef;

  constructor(
    private financiamientoService: FinanciamientoService,
    private loadingService: LoadingService,
    private modalService: BsModalService
  ) { }

  @Input() fichaVehiculo: FichaVehiculoModel = {};
  CuotaMonto: number = 0;
  Cuotas: number = 0;
  CTC: number = 0;
  CAE: number = 0;

  valorPie: number = 0;
  valorPieInput: string = '0';
  porcentajePie: number = 0;
  porcentajePieResultadoFinal: string = '';
  cuotasSeleccion: number = 0;
  porcentajePieDisplay: string = "0%";
  isCollapsedSimulator: boolean = true;
  tasacionAppId?: number;
  mensajeAppPieIrregular?: string;

  seguroDesgravamen: boolean = false;
  seguroCesantia: boolean = false;

  autoParteDePago: boolean = false;
  solicitar: boolean = false;

  private pieInicialGenerado = false;

  get financiamientoConfig(): FinanciamientoConfigModel {
    return environment.financiamiento;
  }
  set porcentajePieFinal(pie: number) {
    this.porcentajePieResultadoFinal = pie.toFixed(2);
  }

  ngOnInit(): void {
    this.porcentajePie = this.financiamientoConfig.pie.auto;
    this.cuotasSeleccion = this.financiamientoConfig.cuotas.defecto;
    this.seguroCesantia = this.financiamientoConfig.seguros.cesantia.checked;
    this.seguroDesgravamen = this.financiamientoConfig.seguros.desgravamen.checked;
    this.porcentajePieFinal = this.porcentajePie;
  }
  ngOnChanges(changes: SimpleChanges) {
    const fichaVeh: FichaVehiculoModel = changes.fichaVehiculo.currentValue;
    if (fichaVeh != undefined && fichaVeh != null) {
      this.CuotaMonto = fichaVeh.CuotaMonto ?? 0;
      this.Cuotas = fichaVeh.Cuotas ?? 0;
      this.CTC = fichaVeh.CTC ?? 0;
      this.CAE = fichaVeh.CAE ?? 0;
      this.calcularPieInical(fichaVeh.Precio ?? "0");
    }
    this.calcularPorcentajePie();
}
  collapseToggleSimulator(): void {
    this.isCollapsedSimulator = !this.isCollapsedSimulator;
  }
  calcularPieInical(precio: string): void {
    if (!this.pieInicialGenerado && precio != undefined) {
      this.valorPie = (parseInt(precio ?? "0") * this.financiamientoConfig.pie.auto) / 100;
      this.valorPieInput = this.valorPie.toString();
      this.pieInicialGenerado = true;
    }
  }
  changeValorPie(): void {
    let valor = parseInt(this.valorPieInput.replace('$', '').split('.').join(''));
    this.valorPie = valor;
    this.calcularPorcentajePie();
  }
  calcularPorcentajePie(): void {
    this.porcentajePie = (this.valorPie * 100) / parseInt(this.fichaVehiculo.Precio ?? "1");
    if (this.porcentajePie < this.financiamientoConfig.pie.minimo) {
      this.porcentajePieDisplay = "Menor al " + this.financiamientoConfig.pie.minimo + "%";
    } else if (this.porcentajePie > this.financiamientoConfig.pie.maximo) {
      this.porcentajePieDisplay = "Mayor al " + this.financiamientoConfig.pie.maximo + "%";
    } else {
      this.porcentajePieDisplay = this.porcentajePie.toFixed(2) + "%";
    }
  }
  simulacion(): void {
    this.loadingService.show(true);
    const finanModel: FinanciamientoModel = {
      marca: this.fichaVehiculo.marca?.Nombre ?? '',
      modelo: this.fichaVehiculo.ModeloComercial ?? '',
      anno: parseInt(this.fichaVehiculo.AnioFabricacion ?? '0'),
      precio: parseInt(this.fichaVehiculo.Precio ?? '0'),
      montoPie: this.valorPie,
      plazo: this.cuotasSeleccion,
      seguros: {
        cesantia: this.seguroCesantia,
        desgravamen: this.seguroDesgravamen
      }
    };
    this.financiamientoService.simulacion(finanModel).subscribe(
    (response: FinanciamientoResponseModel) => {
      if(response.success) {
        if (response.body.cuotaTrinidad.error) {
          console.log(response.body.cuotaTrinidad.mensaje);
          return;
        }
        this.CuotaMonto = parseInt(response.body.cuotaTrinidad.valorCuota);
        this.Cuotas = this.cuotasSeleccion;
        this.CAE = response.body.cuotaTrinidad.valorCae;
        this.CTC = response.body.cuotaTrinidad.totalCredito;
        this.porcentajePieFinal = this.porcentajePie;
        this.scroll('finan-title');
      } else {
        this.showModalError();
        console.log(response.return.code, response.return.message);
      }
      this.loadingService.hide();
    },
    (error: any) => {
      console.log(error);
      this.loadingService.hide();
      this.showModalError();
    });
  }
  aceptaApp(event: AceptaApp): void {
    this.valorPie = event.resumen.valorFinal ?? 0;
    this.valorPieInput = event.resumen.valorFinal?.toString() ?? '0';
    this.calcularPorcentajePie();
    this.tasacionAppId = event.tasacionAppId;

    let esIrregular: boolean = false;
    if(this.porcentajePie > this.financiamientoConfig.pie.maximo) {
      const valorMax = (parseInt(this.fichaVehiculo.Precio ?? "0") * this.financiamientoConfig.pie.maximo) / 100;
      this.mensajeAppPieIrregular = `tienes un excedente de $${((event.resumen.valorFinal ?? 0) - valorMax).toLocaleString('es')}`;
      this.valorPie = valorMax;
      this.valorPieInput = valorMax.toString();
      esIrregular = true;
    } else if (this.porcentajePie < this.financiamientoConfig.pie.minimo) {
      const valorMin = (parseInt(this.fichaVehiculo.Precio ?? "0") * this.financiamientoConfig.pie.minimo) / 100;
      this.mensajeAppPieIrregular = `tienes un monto faltante de $${(valorMin - (event.resumen.valorFinal ?? 0)).toLocaleString('es')}`;
      this.valorPie = valorMin;
      this.valorPieInput = valorMin.toString();
      esIrregular = true;
    }
    if (esIrregular) {
      this.calcularPorcentajePie();
    }
    this.simulacion();
  }
  habilitaSilumacion(): boolean {
    return this.porcentajePie > this.financiamientoConfig.pie.maximo
    || this.porcentajePie < this.financiamientoConfig.pie.minimo;
  }
  showModalError(): void {
    const initialState = {
      isSuccess: false,
      emailCliente: '',
    };
    this.bsModalRef = this.modalService.show(
      RespuestaEnvioCorreoComponent,
      {
        initialState,
        backdrop: 'static',
        keyboard: false,
        class: 'modal-lg',
      }
    );
  }
  scroll(classSelector: string): void {
    let elements = document.getElementsByClassName(classSelector);
    if (elements != null && elements != undefined) {
      elements[0].scrollIntoView();
    }
  }
}
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { FinanciamientoService } from '@app/services/financiamiento/financiamiento.service';
import { IdValueModel } from '@app/models/idvalue.model';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { validateRut } from '@fdograph/rut-utilities';
import { NgRecaptcha3Service } from 'ng-recaptcha3';
import { LoadingService } from '@app/services/loading/loading.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { RespuestaEnvioCorreoComponent } from '@app/ui/modals/respuesta-envio-correo/respuesta-envio-correo.component';
import { AceptaFinanModel } from '@app/models/financiamiento/financiamiento.acepta.model';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { EnumSolicitudFinanForm, EnumAceptacionFinan } from '@app/models/enum/enums.model';

const numberMask = createNumberMask({
  prefix: '$',
  suffix: '',
  thousandsSeparatorSymbol: '.'
});
const phoneMask = createNumberMask({
  prefix: '+',
  thousandsSeparatorSymbol: ''
});
let clearNumberMask: (number: string) => string;

@Component({
  selector: 'app-solicitud-form',
  templateUrl: './solicitud-form.component.html',
  styleUrls: ['./solicitud-form.component.scss']
})
export class SolicitudFormComponent implements OnInit {
  public numbermask = numberMask;
  public phonemask = phoneMask;
  solicitudForm!: FormGroup;
  isSubmitted: boolean = false;
  bsModalRef!: BsModalRef;

  constructor(
    private formBuilder: FormBuilder,
    private recaptcha3: NgRecaptcha3Service,
    private loadingService: LoadingService,
    private modalService: BsModalService,
    private financiamientoService: FinanciamientoService
  ) {
    clearNumberMask = this.clearNumberMask;
   }

  @Input() fichaVehiculo: FichaVehiculoModel = {};
  @Input() CuotasSolicitud: number = 0;
  @Input() PieSolicitud: number = 0;
  @Input() CesantiaSolicitud: boolean = false;
  @Input() DesgravamenSolicitud: boolean = true;
  @Input() IdApp?: number;

  tipoNacionalidad: IdValueModel<number, string>[] = [];
  tipoPersona: IdValueModel<number, string>[] = [];
  tipoContrato: IdValueModel<number, string>[] = [];
  tipoRenta: IdValueModel<number, string>[] = [];

  vista: EnumSolicitudFinanForm = new EnumSolicitudFinanForm;
  vistaDisplay: number = this.vista.formulario;

  enumAceptacion: EnumAceptacionFinan = new EnumAceptacionFinan;
  errorCaptcha: boolean = false;

  ngOnInit(): void {
    this.iniciaServicio();
    this.iniciaFormulario();
  }
  iniciaServicio(): void {
    this.financiamientoService.configuracion().subscribe(config => {
      this.tipoNacionalidad = config.tipoNacionalidad;
      this.tipoPersona = config.tipoCliente;
      this.tipoContrato = config.tipoContrato;
      this.tipoRenta = config.tipoRenta;
    });
  }
  iniciaFormulario(): void {
    this.solicitudForm = this.formBuilder.group({
      rut: ['',[this.validaRut, Validators.required]],
      nombre: ['',[Validators.required]],
      apepat: ['',[Validators.required]],
      apemat: ['',[Validators.required]],
      nacionalidad: ['',[Validators.required]],
      direccion: ['',[Validators.required]],
      numero: ['',[Validators.required]],
      tipoPersona: ['',[Validators.required]],
      tipoContrato: ['',[Validators.required]],
      tipoRenta: ['',[Validators.required]],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern(
            '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'
          ),
          Validators.minLength(2),
        ],
      ],
      telefono: [
        '',
        [
          Validators.required,
          Validators.pattern('^([+])([0-9]+$)'),
          Validators.minLength(12),
        ],
      ],
      antiguedad: [
        '',
        [Validators.pattern('^([0-9]+$)'), Validators.maxLength(3), Validators.required]
      ],
      renta: [
        '',
        [this.numberMaskValidator, Validators.minLength(5), Validators.maxLength(11), Validators.required]
      ]
    });
  }
  realizaSolicitud(): void {
    this.errorCaptcha = false;
    this.isSubmitted = true;
    if (this.solicitudForm.valid) {
      this.loadingService.show(true);
      this.recaptcha3.getToken().then((captcha: string) => {
        this.enviarAceptacion(captcha);
      })
      .catch(err => {
        this.errorCaptcha = true;
        console.log(err);
        this.loadingService.hide();
        this.showModalError();
      });
    }
  }
  enviarAceptacion(captcha: string): void {
    const aceptaModel: AceptaFinanModel = {
      montoAFinanciar: parseInt(this.fichaVehiculo.Precio ?? "0"),
      nacionalidad: this.solicitudForm.controls.nacionalidad.value,
      tipoCliente: this.solicitudForm.controls.tipoPersona.value,
      tipoContrato: this.solicitudForm.controls.tipoContrato.value,
      tipoRenta: this.solicitudForm.controls.tipoRenta.value,
      renta: parseInt(clearNumberMask(this.solicitudForm.controls.renta.value)),
      antiguedad: this.solicitudForm.controls.antiguedad.value,
      pie: this.PieSolicitud,
      plazo: this.CuotasSolicitud,
      vehiculo: {
        marca: this.fichaVehiculo.marca?.Nombre ?? "",
        modelo: this.fichaVehiculo.ModeloComercial ?? "",
        anio: this.fichaVehiculo.AnioFabricacion?.toString() ?? "0",
        precio: this.fichaVehiculo.Precio ?? "0"
      },
      seguros: {
        desgravamen: this.DesgravamenSolicitud,
        cesantia: this.CesantiaSolicitud
      },
      persona: {
        rut: this.solicitudForm.controls.rut.value,
        nombre: this.solicitudForm.controls.nombre.value,
        primerApellido: this.solicitudForm.controls.apepat.value,
        segundoApellido: this.solicitudForm.controls.apemat.value,
        direccion: this.solicitudForm.controls.direccion.value,
        numero: this.solicitudForm.controls.numero.value,
        correo: this.solicitudForm.controls.email.value,
        telefono: this.solicitudForm.controls.telefono.value
      },
      IdApp: this.IdApp,
      captcha: captcha
    };
    this.financiamientoService.aceptacion(aceptaModel).subscribe(
      resp => {
        switch (resp) {
          case this.enumAceptacion.correcta:
            this.isSubmitted = false;
            this.solicitudForm.reset();
            this.vistaDisplay = this.vista.exito;
            this.scroll('scroll-view', 200);
            this.loadingService.hide();
            break;
          case this.enumAceptacion.errorCaptcha:
            this.errorCaptcha = true;
            break;
          case this.enumAceptacion.errorBD:
          case this.enumAceptacion.errorServicio:
          case this.enumAceptacion.rechazada:
            console.log(resp);
            this.loadingService.hide();
            this.showModalError();
            break;
        }
      },
      error => {
        console.log(error);
        this.loadingService.hide();
        this.showModalError();
      }
    );
  }
  numberMaskValidator(control: FormControl): ValidationErrors | null {
    if (control.value) {
      let value = clearNumberMask(control.value);
      const matches = value.match('^([0-9]+$)');
      const valueNumber = parseInt(value);
      return !isNaN(valueNumber) && matches ? null : { 'invalidNumberMask': true };
    } else {
      return null;
    }
  }
  validaRut(control: FormControl): ValidationErrors | null {
    if (control.value) {
      return validateRut(control.value) ? null : { invalidRut: true };
    } else {
      return null;
    }
  }
  clearNumberMask(number: string): string {
    if(number == undefined || number == null) {
      return number;
    }
    return number.replace('$', '').split('.').join('').trim();
  }
  validator(formControlName: string, ErrorTypeName: string): boolean {
    const errors = this.solicitudForm.get(formControlName)?.errors;
    if(errors != undefined && errors != null) {
      return errors[ErrorTypeName] && this.isSubmitted;
    }
    return false;
  }
  showModalError(): void {
    const initialState = {
      isSuccess: false,
      emailCliente: '',
    };
    this.bsModalRef = this.modalService.show(
      RespuestaEnvioCorreoComponent,
      {
        initialState,
        backdrop: 'static',
        keyboard: false,
        class: 'modal-lg',
      }
    );
  }
  scroll(classSelector: string, timeout: number = 0): void {
    setTimeout(() => {
      let elements = document.getElementsByClassName(classSelector);
      if (elements != null && elements != undefined) {
        elements[0].scrollIntoView();
      }
    }, timeout)
  }
}

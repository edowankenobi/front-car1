import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanciamientoIframeComponent } from './financiamiento-iframe.component';

describe('FinanciamientoIframeComponent', () => {
  let component: FinanciamientoIframeComponent;
  let fixture: ComponentFixture<FinanciamientoIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinanciamientoIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanciamientoIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaVehiculoIframeComponent } from './ficha-vehiculo-iframe.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ActivatedRouteMock } from '@app/mocks/activated-route.mock';
import { Observable, of } from 'rxjs';
import { PdfService } from '@app/services/pdf/pdf.service';
import { TerminosCondicionesResponseModel } from '@app/models/pdf/terminos-condiciones-response.model';

fdescribe('FichaVehiculoIframeComponent', () => {
  let component: FichaVehiculoIframeComponent;
  let fixture: ComponentFixture<FichaVehiculoIframeComponent>;
  const activatedRouteMock = {
    queryParams: of({}),
    get params(): Observable<Params> {
      return new Observable((observer) => {
        observer.next({ patente: '200' } as Params);
        observer.complete();
      });
    },
  };

  const pdfServiceMock = {
    obtenerPdfTerminosCondiciones(): Observable<TerminosCondicionesResponseModel> {
      return new Observable((observer) => {
        observer.next({url: 'abc'} as TerminosCondicionesResponseModel);
        observer.complete();
      });
    }
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FichaVehiculoIframeComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: Router, useValue: {}},
        { provide: ActivatedRoute, useValue: activatedRouteMock },
        { provide: PdfService, useValue: pdfServiceMock} 
      ]
    })
      .overrideTemplate(FichaVehiculoIframeComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaVehiculoIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

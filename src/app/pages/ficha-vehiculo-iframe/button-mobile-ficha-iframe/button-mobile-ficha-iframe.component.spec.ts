import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonMobileFichaIframeComponent } from './button-mobile-ficha-iframe.component';

describe('ButtonMobileFichaIframeComponent', () => {
  let component: ButtonMobileFichaIframeComponent;
  let fixture: ComponentFixture<ButtonMobileFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonMobileFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonMobileFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

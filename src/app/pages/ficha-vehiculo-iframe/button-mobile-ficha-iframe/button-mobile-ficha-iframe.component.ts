import { Component, HostListener, Input, OnInit, SimpleChanges } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { environment } from '@env/environment';

@Component({
  selector: 'app-button-mobile-ficha-iframe',
  templateUrl: './button-mobile-ficha-iframe.component.html',
  styleUrls: ['./button-mobile-ficha-iframe.component.scss']
})
export class ButtonMobileFichaIframeComponent implements OnInit {

  constructor() { }

  @Input() fichaVehiculo: FichaVehiculoModel = {};
  
  divContacto: any;
  visibleContacto: boolean = false;
  whatsappURL?: string = 'https://api.whatsapp.com/send?phone={0}&text={1}';

  ngOnInit(): void {
    this.divContacto = document.getElementById("contacto-ficha");
  }
  ngOnChanges(changes: SimpleChanges) {
    const fichaVeh: FichaVehiculoModel = changes.fichaVehiculo.currentValue;
    if (fichaVeh != undefined && fichaVeh.marca != undefined) {
      this.generateWhatsapp();
    }
  }
  generateWhatsapp(): void {
    if(!environment.telefonos.compra.length) {
      this.whatsappURL = undefined;
      return;
    }
    const phonenumber = this.randomPhone(environment.telefonos.compra);
    let text = `¡Hola! Me gustaría cotizar el ${this.fichaVehiculo.marca?.Nombre.toLocaleUpperCase()} ${this.fichaVehiculo.Modelo?.toLocaleUpperCase()} ${this.fichaVehiculo.AnioFabricacion} publicado en Car1.cl`;
    const url = new URL(
      this.whatsappURL
      ?.replace('{0}', phonenumber.interno)
      ?.replace('{1}', text) || ''
    );
    this.whatsappURL = url.href;
  }
  randomPhone(array: any[]): any {
    const arrayFiltro = array.filter(val => val.esficha);
    if(arrayFiltro.length == 1) {
      return arrayFiltro[0];
    }
    const randomIndex = Math.floor(Math.random() * arrayFiltro.length);
    return arrayFiltro[randomIndex];
  }
  @HostListener("window:scroll", [''])
  onWindowsScroll() {
    const verticalOffset = Math.round(window.pageYOffset 
    || document.documentElement.scrollTop 
    || document.body.scrollTop || 0);

    this.visibleContacto = 
      verticalOffset >= this.divContacto.offsetTop 
      && verticalOffset <= (this.divContacto.offsetTop + this.divContacto.offsetHeight);
  }
  contactoBtn(): void {
    scrollTo(0, this.divContacto.offsetTop);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { FichaVehiculoService } from '@app/services/ficha-vehiculo/ficha-vehiculo.service';
import { LoadingService } from '@app/services/loading/loading.service';
import { PdfService } from '@app/services/pdf/pdf.service';
import { MethodsUtil } from '@app/util/methods.util';

@Component({
  selector: 'app-ficha-vehiculo-iframe',
  templateUrl: './ficha-vehiculo-iframe.component.html',
  styleUrls: ['./ficha-vehiculo-iframe.component.scss'],
})
export class FichaVehiculoIframeComponent implements OnInit {
  /**
   * Datos del vehículo
   */
  fichaVehiculo: FichaVehiculoModel = {};

  /**
   * Patente del vehículo a consultar
   */
  patente!: '111111';

  /**
   * url pdf Términos y Condiciones
   */
  urlPdfTerminosCondiciones!: string;

  isMobile: boolean = MethodsUtil.isMobile();
  screenWidth: number = MethodsUtil.screenWidth();
  constructor(
    private fichaVehiculoService: FichaVehiculoService,
    private route: ActivatedRoute,
    private router: Router,
    private loadingService: LoadingService,
    private pdfService: PdfService
  ) {}

  ngOnInit(): void {

    this.obtainPatentInfo();
    this.obtenerUrlPdf();
  }

  /**
   * Método encargado de inicializar los datos del vehículo
   */
  inicializarFicha(): void {
    this.loadingService.show();
    this.fichaVehiculoService
      .fichaVehiculo(this.patente)
      .subscribe((response) => {
        if(this.existsResponse(response)) {
          this.loadingService.hide();
          this.fichaVehiculo = response[0];
        } else {
          this.navigateStock();
        }
      },
      error => {
        console.error(error);
        this.navigateStock();
      });
  }
  navigateStock(): void {
    this.router.navigate(['/stock-disponible']);
  }
  existsResponse(data: FichaVehiculoModel[]): boolean {
    return data != null && data != undefined && data.length > 0
         && data[0] != null && data[0] != undefined
         && data[0].id != null && data[0].id != undefined && data[0].id > 0;
  }

  /**
   * Método encargado de obtener la patente de la URL
   */
  obtainPatentInfo(): void {
    this.loadingService.show();
    this.route.params.subscribe((params: Params) => {
      this.loadingService.hide();
      this.patente = params.patente;
      this.inicializarFicha();
    });
  }

  /**
   * Método encargado de retornar la URL del pdf de términos y condiciones
   */
  obtenerUrlPdf(): void {
    this.pdfService.obtenerPdfTerminosCondiciones().subscribe((response) => {
      this.urlPdfTerminosCondiciones = response.url;
    });
  }

  activaApp(): boolean {
    return (
      this.existsValue(this.fichaVehiculo.CuotaMonto)
      && this.existsValue(this.fichaVehiculo.Cuotas)
      && this.existsValue(this.fichaVehiculo.CTC)
      && this.existsValue(this.fichaVehiculo.CAE)
    );
  }
  existsValue<T>(input: T): boolean {
    return input !== undefined && input !== null;
  }
}

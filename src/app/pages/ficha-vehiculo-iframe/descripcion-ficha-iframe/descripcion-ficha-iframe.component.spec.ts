import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescripcionFichaIframeComponent } from './descripcion-ficha-iframe.component';

describe('DescripcionFichaIframeComponent', () => {
  let component: DescripcionFichaIframeComponent;
  let fixture: ComponentFixture<DescripcionFichaIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescripcionFichaIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescripcionFichaIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-descripcion-ficha-iframe',
  templateUrl: './descripcion-ficha-iframe.component.html',
  styleUrls: ['./descripcion-ficha-iframe.component.scss']
})
export class DescripcionFichaIframeComponent implements OnInit {
  constructor() { }

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void { }
}

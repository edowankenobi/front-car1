import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PruebasComponent } from './pruebas.component';


@NgModule({
  declarations: [PruebasComponent],
  imports: [
    CommonModule
  ],
  exports: [PruebasComponent]
})
export class PruebasModule { }

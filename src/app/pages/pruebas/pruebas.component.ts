import { Component, OnInit } from '@angular/core';
import { TasadorService } from '@app/services/tasador-interno/tasador-interno.service'

@Component({
  selector: 'app-pruebas',
  templateUrl: './pruebas.component.html',
  styleUrls: ['./pruebas.component.scss']
})
export class PruebasComponent implements OnInit {

  constructor(
    private tasador: TasadorService
  ) { }

  ngOnInit(): void {
    this.tasador.obtenerAnios().subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log("esto es un error", error);
      });
    this.tasador.kilometrajeSeleccionable().subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log("esto es un error", error);
      });
    this.tasador.obtenerMarcas(10).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log("esto es un error", error);
      });
    this.tasador.obtenerModelos(650).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log("esto es un error", error);
      });
    this.tasador.obtenerVersiones(5411).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log("esto es un error", error);
      });
    this.tasador.obtenerValorTasacion(24504, 5, "").subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log("esto es un error", error);
      });
    this.tasador.datosPorPatente('LDYW40').subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log("esto es un error", error);
      });
  }

}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-venta-vehiculo',
  templateUrl: './venta-vehiculo.component.html',
  styleUrls: ['./venta-vehiculo.component.scss'],
})
export class VentaVehiculoComponent implements OnInit, OnDestroy {
  /**
   * Indica si es flujo de venta vehículo
   */
  esVenta: boolean;

  constructor(private metaService: Meta) {
    this.esVenta = true;
  }

  ngOnInit(): void {
    this.setMetaData();
  }

  ngOnDestroy(): void {
    this.metaService.removeTag('name=\'description\'');
    this.metaService.removeTag('name=\'description\'');
    this.metaService.removeTag('name=\'keywords\'');
  }

  /**
   * Metodo que setea meta data especifica al flujo.
   */
  setMetaData(): void {
    this.metaService.addTags([
      {
        name: 'title',
        content: 'Vende tu auto usado. Cotiza online, mira el precio ahora',
      },
      {
        name: 'description',
        content:
          'Te compramos tu auto al contado. Obtén el precio online en minutos de tu auto, camioneta o SUV. Cotiza sin compromiso, pagamos muy buenos precios para ti. ',
      },
      {
        name: 'keywords',
        content:
          'autos usados, compra de autos usados, compra de camionetas usadas, compra de vehículos usados',
      },
    ]);
  }
}

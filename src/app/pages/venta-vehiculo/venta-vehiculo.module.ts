import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VentaVehiculoComponent } from '@app/pages/venta-vehiculo/venta-vehiculo.component';
import { TasadorModule } from '@app/pages/tasador/tasador.module';

@NgModule({
  declarations: [VentaVehiculoComponent],
  imports: [CommonModule, TasadorModule],
  exports: [VentaVehiculoComponent]
})
export class VentaVehiculoModule { }

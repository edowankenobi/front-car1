import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-caracteristicas-ficha',
  templateUrl: './caracteristicas-ficha.component.html',
  styleUrls: ['./caracteristicas-ficha.component.scss']
})
export class CaracteristicasFichaComponent implements OnInit {
  constructor() {}

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void { registerLocaleData( es ); }
}

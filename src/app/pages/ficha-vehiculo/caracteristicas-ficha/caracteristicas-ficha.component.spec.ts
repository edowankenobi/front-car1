import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracteristicasFichaComponent } from './caracteristicas-ficha.component';

describe('CaracteristicasFichaComponent', () => {
  let component: CaracteristicasFichaComponent;
  let fixture: ComponentFixture<CaracteristicasFichaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaracteristicasFichaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaracteristicasFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

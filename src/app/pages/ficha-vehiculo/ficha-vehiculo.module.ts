import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactoModule } from '@app/pages/contacto/contacto.module';
import { FichaVehiculoComponent } from '@app/pages/ficha-vehiculo/ficha-vehiculo.component';
import { TabsFichaModule } from '@app/pages/ficha-vehiculo/tabs-ficha/tabs-ficha.module';
import { InfoCompraModule } from '@app/ui/info-compra/info-compra.module';
import { WizardModule } from '../../ui/wizard/wizard.module';
import { CabeceraFichaComponent } from './cabecera-ficha/cabecera-ficha.component';
import { CaracteristicasFichaComponent } from './caracteristicas-ficha/caracteristicas-ficha.component';
import { DescripcionFichaComponent } from './descripcion-ficha/descripcion-ficha.component';
import { SpincarFichaModule } from './spincar-ficha/spincar-ficha.module';

@NgModule({
  declarations: [
    FichaVehiculoComponent,
    CabeceraFichaComponent,
    DescripcionFichaComponent,
    CaracteristicasFichaComponent,
  ],
  imports: [
    CommonModule,
    InfoCompraModule,
    TabsFichaModule,
    ContactoModule,
    SpincarFichaModule,
    ReactiveFormsModule,
    WizardModule,
  ],
})
export class FichaVehiculoModule {}

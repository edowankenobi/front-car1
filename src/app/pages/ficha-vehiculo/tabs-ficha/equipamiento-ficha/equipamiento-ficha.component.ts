import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-equipamiento-ficha',
  templateUrl: './equipamiento-ficha.component.html',
  styleUrls: ['./equipamiento-ficha.component.scss']
})
export class EquipamientoFichaComponent implements OnInit {
  constructor() {}

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void {}
}

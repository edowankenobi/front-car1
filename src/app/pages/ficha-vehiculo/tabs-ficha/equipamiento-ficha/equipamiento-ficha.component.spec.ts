import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipamientoFichaComponent } from './equipamiento-ficha.component';

describe('EquipamientoFichaComponent', () => {
  let component: EquipamientoFichaComponent;
  let fixture: ComponentFixture<EquipamientoFichaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipamientoFichaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipamientoFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

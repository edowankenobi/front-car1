import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-tabs-ficha',
  templateUrl: './tabs-ficha.component.html',
  styleUrls: ['./tabs-ficha.component.scss']
})
export class TabsFichaComponent implements OnInit {
  constructor() {}

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void {}
}

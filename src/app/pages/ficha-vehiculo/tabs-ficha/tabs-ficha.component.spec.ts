import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsFichaComponent } from './tabs-ficha.component';

describe('TabsFichaComponent', () => {
  let component: TabsFichaComponent;
  let fixture: ComponentFixture<TabsFichaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabsFichaComponent ]
    })
    .overrideTemplate(TabsFichaComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

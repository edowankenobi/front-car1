import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-detalle-ficha',
  templateUrl: './detalle-ficha.component.html',
  styleUrls: ['./detalle-ficha.component.scss']
})
export class DetalleFichaComponent implements OnInit {
  constructor() {}

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void {}
}

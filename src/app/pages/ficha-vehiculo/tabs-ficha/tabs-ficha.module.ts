import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleFichaComponent } from '@app/pages/ficha-vehiculo/tabs-ficha/detalle-ficha/detalle-ficha.component';
import { EquipamientoFichaComponent } from '@app/pages/ficha-vehiculo/tabs-ficha/equipamiento-ficha/equipamiento-ficha.component';
import { TabsFichaComponent } from '@app/pages/ficha-vehiculo/tabs-ficha/tabs-ficha.component';

@NgModule({
  declarations: [
    TabsFichaComponent,
    DetalleFichaComponent,
    EquipamientoFichaComponent
  ],
  imports: [CommonModule],
  exports: [TabsFichaComponent]
})
export class TabsFichaModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaVehiculoComponent } from './ficha-vehiculo.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { FichaVehiculoService } from '@app/services/ficha-vehiculo/ficha-vehiculo.service';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { BotonComprasRequest } from '@app/models/compraVehiculo/boton-compras-request.model';
import { BotonCompras } from '@app/models/compraVehiculo/boton-compras-response.model';

describe('FichaVehiculoComponent', () => {
  let component: FichaVehiculoComponent;
  let fixture: ComponentFixture<FichaVehiculoComponent>;
  const activatedRouteMock = {
    queryParams: of({
      patente: 'ZBC123'
    }),
    get params(): Observable<Params> {
      return new Observable((observer) => {
        observer.next({ patente: '200' } as Params);
        observer.complete();
      });
    },
  };

  const fichaVehiculoServiceMock = {
    fichaVehiculo(patente: string): Observable<FichaVehiculoModel[]> {
      return new Observable((observer) => {
        observer.next([{ patente: '200' } as FichaVehiculoModel]);
        observer.complete();
      });
    },
    getCarFileState(patente: BotonComprasRequest): Observable<BotonCompras> {
      return new Observable((observer) => {
        observer.next({ codigo: '200', estado: 1 } as BotonCompras);
        observer.complete();
      });
      
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FichaVehiculoComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteMock },
        { provide: FichaVehiculoService, useValue: fichaVehiculoServiceMock },
      ]
    })
      .overrideTemplate(FichaVehiculoComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create', () => {
    component.inicializarFicha();
    expect(component).toBeTruthy();
  });
});

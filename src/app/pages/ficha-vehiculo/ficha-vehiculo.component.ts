import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BotonComprasRequest } from '@app/models/compraVehiculo/boton-compras-request.model';
import { BotonCompras } from '@app/models/compraVehiculo/boton-compras-response.model';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { FichaVehiculoService } from '@app/services/ficha-vehiculo/ficha-vehiculo.service';
import { LoadingService } from '@app/services/loading/loading.service';

@Component({
  selector: 'app-ficha-vehiculo',
  templateUrl: './ficha-vehiculo.component.html',
  styleUrls: ['./ficha-vehiculo.component.scss'],
})
export class FichaVehiculoComponent implements OnInit {
  /**
   * Datos del vehículo
   */
  fichaVehiculo: FichaVehiculoModel = {};

  /**
   * Patente del vehículo a consultar
   */
  patente = '111111';

  /**
   * Flag que deshabilita btn comprar.
   */
  disableBtnComprar: boolean;

  botonComprasRequest!: BotonComprasRequest;

  constructor(
    private fichaVehiculoService: FichaVehiculoService,
    private route: ActivatedRoute,
    private loadingService: LoadingService
  ) {
    this.disableBtnComprar = false;
  }

  ngOnInit(): void {
    this.inicializarFicha();
  }

  /**
   * Método encargado de inicializar los datos del vehículo
   */
  inicializarFicha(): void {
    this.loadingService.show();
    this.fichaVehiculoService
      .fichaVehiculo(this.patente)
      .subscribe((response) => {
        this.loadingService.hide();
        this.fichaVehiculo = response[0];
      });
  }

  /**
   * Metodo que consulta servicio de estado de ficha vehiculo.
   */
  checkCarFileState(request: BotonComprasRequest): void {
    this.fichaVehiculoService
      .getCarFileState(request)
      .subscribe((response: BotonCompras) => {
        response.codigo === '200' && response.estado === 1
          ? (this.disableBtnComprar = true)
          : (this.disableBtnComprar = false);
      });
  }
}

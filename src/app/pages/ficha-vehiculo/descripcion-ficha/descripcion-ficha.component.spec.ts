import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescripcionFichaComponent } from './descripcion-ficha.component';

describe('DescripcionFichaComponent', () => {
  let component: DescripcionFichaComponent;
  let fixture: ComponentFixture<DescripcionFichaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescripcionFichaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescripcionFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

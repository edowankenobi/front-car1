import { Component, OnInit, Input } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-descripcion-ficha',
  templateUrl: './descripcion-ficha.component.html',
  styleUrls: ['./descripcion-ficha.component.scss']
})
export class DescripcionFichaComponent implements OnInit {
  constructor() {}

  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void {}
}

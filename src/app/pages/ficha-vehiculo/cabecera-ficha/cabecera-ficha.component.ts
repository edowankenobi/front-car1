import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BotonComprasRequest } from '@app/models/compraVehiculo/boton-compras-request.model';
import { BotonCompras } from '@app/models/compraVehiculo/boton-compras-response.model';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { FichaVehiculoService } from '@app/services/ficha-vehiculo/ficha-vehiculo.service';
import { LoadingService } from '../../../services/loading/loading.service';

@Component({
  selector: 'app-cabecera-ficha',
  templateUrl: './cabecera-ficha.component.html',
  styleUrls: ['./cabecera-ficha.component.scss'],
})
export class CabeceraFichaComponent implements OnInit {
  /**
   * Datos del vehículo.
   */
  @Input() fichaVehiculo: FichaVehiculoModel = {};

  /**
   * Flag que indica cuando mostrar la barra de informacion flotante.
   */
  floatingBar: boolean;

  /**
   * Flag que deshabilita btn comprar.
   */
  @Input() disableBtnComprar: boolean;

  /**
   * consturctor
   * @param router router service.
   * @param fichaVehiculoService ficha vehiculo service.
   */
  constructor(
    private router: Router,
    private fichaVehiculoService: FichaVehiculoService,
    private loadingService: LoadingService
  ) {
    this.floatingBar = false;
    this.disableBtnComprar = false;
  }

  ngOnInit(): void {
    registerLocaleData(es);
  }

  /**
   * Metodo que obtiene la url del auto seleccionado.
   * @returns string url
   */
  getCarPictureUrl(): string {
    let url = '';
    const fotos = this.fichaVehiculo.Foto;
    if (fotos && fotos[0].url) {
      url = fotos[0].url;
    }
    return url;
  }

  /**
   * Listener que se encarga de manipular eventos en base al scroll
   * @param $event evento scroll
   */
  // tslint:disable-next-line: typedef
  @HostListener('window:scroll', ['$event']) onScrollEvent($event: any) {
    const scrollOffset = $event.srcElement.children[0].scrollTop;
    scrollOffset > 140 ? (this.floatingBar = true) : (this.floatingBar = false);
  }

  /**
   * Flag que validad la cantidad de caracteres del titulo del vehiculo
   * y en base a eso retorna un true.
   * @returns boolean
   */
  tittleDetectorClassModifier(): boolean {
    let modifier = false;
    if (this.fichaVehiculo.Modelo && this.fichaVehiculo.Modelo?.length > 24) {
      modifier = true;
    }
    return modifier;
  }

  /**
   * Metodo que hace llamado del servicio de btn de compra de vehiculo para consultar estado,
   * y redirigir a al flujo de compra del mismo.
   */
  redirectToCompra(): void {
    this.loadingService.show(true);
    const botonCompraRequest = new BotonComprasRequest(
      String(this.fichaVehiculo.Patente),
      0
    );
    this.fichaVehiculoService
      .getCarFileState(botonCompraRequest)
      .subscribe((response: BotonCompras) => {
        this.loadingService.hide();
        if (response.codigo === '200' && response.estado === 0) {
          const urlCompraVehiculo = `ficha-vehiculo/${[
            this.fichaVehiculo.Patente,
          ]}/compra-vehiculo`;
          this.router.navigate([urlCompraVehiculo]);
        }
      });
  }
}

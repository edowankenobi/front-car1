import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { BotonCompras } from '@app/models/compraVehiculo/boton-compras-response.model';
import { GuardarDetalleRequestModel } from '@app/models/tasador/guardar-detalle-request.model';
import { FichaVehiculoService } from '@app/services/ficha-vehiculo/ficha-vehiculo.service';
import { Observable } from 'rxjs';
import { CabeceraFichaComponent } from './cabecera-ficha.component';

describe('CabeceraFichaComponent', () => {
  let component: CabeceraFichaComponent;
  let fixture: ComponentFixture<CabeceraFichaComponent>;
  const routerMock = {};

  const fichaVehiculoServiceMock = {
    registerDetailedData(
      data: GuardarDetalleRequestModel
    ): Observable<BotonCompras> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as BotonCompras);
        observer.complete();
      });
    },
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CabeceraFichaComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: Router, useValue: routerMock },
        {
          provide: FichaVehiculoService,
          useValue: fichaVehiculoServiceMock,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CabeceraFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('debe detectar el titulo', () => {
    component.fichaVehiculo.Modelo =
      'Hola este modelo de auto es un modelo autentico';
    component.tittleDetectorClassModifier();
    expect(component).toBeTruthy();
    expect(component.tittleDetectorClassModifier).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SpincarComponent } from './spincar.component';

describe('SpincarComponent', () => {
  let component: SpincarComponent;
  let fixture: ComponentFixture<SpincarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SpincarComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpincarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

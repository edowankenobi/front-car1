import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-spincar',
  templateUrl: './spincar.component.html',
  styleUrls: ['./spincar.component.scss'],
})
export class SpincarComponent implements OnInit {
  innerContent: any;

  @Input('content')
  set setContent(content: string) {
    this.innerContent = this.sanitizer.bypassSecurityTrustHtml(content);
  }
  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit(): void {}
}

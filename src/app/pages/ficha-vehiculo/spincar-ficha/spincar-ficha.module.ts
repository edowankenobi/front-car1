import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SpincarFichaComponent } from '@app/pages/ficha-vehiculo/spincar-ficha/spincar-ficha.component';
import { SpincarComponent } from '@app/pages/ficha-vehiculo/spincar-ficha/spincar/spincar.component';

@NgModule({
  declarations: [SpincarFichaComponent, SpincarComponent],
  imports: [CommonModule],
  exports: [SpincarFichaComponent],
})
export class SpincarFichaModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpincarFichaComponent } from './spincar-ficha.component';

describe('SpincarFichaComponent', () => {
  let component: SpincarFichaComponent;
  let fixture: ComponentFixture<SpincarFichaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpincarFichaComponent ]
    })
    .overrideTemplate(SpincarFichaComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpincarFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

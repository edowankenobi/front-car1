import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoTasadorComponent } from './nuevo-tasador.component';

describe('NuevoTasadorComponent', () => {
  let component: NuevoTasadorComponent;
  let fixture: ComponentFixture<NuevoTasadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoTasadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoTasadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nuevo-tasador',
  templateUrl: './nuevo-tasador.component.html',
  styleUrls: ['./nuevo-tasador.component.scss']
})
export class NuevoTasadorComponent implements OnInit {

  /**
   * Atributo que indica el paso en el que se encuentra la tasación
   */
  //pasoTasacion = 'formulario';
  pasoTasacion = 'agenda';

  constructor() { }

  ngOnInit(): void {
  }

  manejarPaso($event:string):void {
    this.pasoTasacion = $event;
  }


}

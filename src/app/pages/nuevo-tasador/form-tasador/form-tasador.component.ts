import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form-tasador',
  templateUrl: './form-tasador.component.html',
  styleUrls: ['./form-tasador.component.scss']
})
export class FormTasadorComponent implements OnInit {

    /**
   * Indicador del paso en el que se encuentra el proceso de tasación
   */
     @Output() pasoTasacion = new EventEmitter<string>();
  

  constructor() { 
  }

  
  ngOnInit(): void {
  }

  siguiente():void {
    this.pasoTasacion.emit('oferta');
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTasadorComponent } from './form-tasador.component';

describe('FormTasadorComponent', () => {
  let component: FormTasadorComponent;
  let fixture: ComponentFixture<FormTasadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTasadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTasadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.scss']
})
export class ResumenComponent implements OnInit {
  
  /**
   * Indicador del paso en el que se encuentra el proceso de tasación
   */
   @Output() pasoTasacion = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  volver():void {
    this.pasoTasacion.emit('formulario');
  }

}

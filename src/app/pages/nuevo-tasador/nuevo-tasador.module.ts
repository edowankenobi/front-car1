import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormTasadorComponent } from './form-tasador/form-tasador.component';
import { NuevoTasadorComponent } from './nuevo-tasador.component';
import { DatosPersonaComponent } from './datos-persona/datos-persona.component';
import { AgendaComponent } from './agenda/agenda.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ResumenComponent } from './resumen/resumen.component';

@NgModule({
  declarations: [NuevoTasadorComponent, FormTasadorComponent, DatosPersonaComponent, AgendaComponent, ResumenComponent],
  imports: [CommonModule, BsDatepickerModule.forRoot(),],
  exports: [NuevoTasadorComponent],
})
export class NuevoTasadorModule {}

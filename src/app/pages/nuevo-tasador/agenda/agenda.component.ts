import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {
    
  /**
   * Indicador del paso en el que se encuentra el proceso de tasación
   */
   @Output() pasoTasacion = new EventEmitter<string>();
  
  /**
    * Indica el día actual usado en el calendario
    */
  minDate = new Date();
    
  /**
    * Indica el día actual o seleccionado en el calendario
    */
  @Input() dayValue!: Date;

  /**
      * Indica el día actual usado en el calendario
      */
  mostrarLocal: Boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  siguiente():void {
    this.pasoTasacion.emit('resumen');
  }

  volver():void {
    this.pasoTasacion.emit('oferta');
  }

  enLocal(): void {
    this.mostrarLocal = this.mostrarLocal ? false : true;
  }

}

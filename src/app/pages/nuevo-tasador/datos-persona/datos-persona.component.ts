import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-datos-persona',
  templateUrl: './datos-persona.component.html',
  styleUrls: ['./datos-persona.component.scss']
})
export class DatosPersonaComponent implements OnInit {

  /**
   * Indicador del paso en el que se encuentra el proceso de tasación
   */
  @Output() pasoTasacion = new EventEmitter<string>();
  
  constructor() { }

  ngOnInit(): void {
  }

  siguiente():void {
    this.pasoTasacion.emit('agenda');
  }

  volver():void {
    this.pasoTasacion.emit('formulario');
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SwiperModule,
  SwiperConfigInterface,
  SWIPER_CONFIG
} from 'ngx-swiper-wrapper';
import { BuscadorTipoVehiculoComponent } from '@app/pages/home/buscador-tipo-vehiculo/buscador-tipo-vehiculo.component';
import { BuscadorTipoVehiculoRoutingModule } from '@app/pages/home/buscador-tipo-vehiculo/buscador-tipo-vehiculo-routing.module';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: false
};

@NgModule({
  declarations: [BuscadorTipoVehiculoComponent],
  imports: [CommonModule, BuscadorTipoVehiculoRoutingModule, SwiperModule],
  exports: [BuscadorTipoVehiculoComponent],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class BuscadorTipoVehiculoModule {}

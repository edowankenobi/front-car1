import { Component, OnInit, Input } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { TipoVehiculo } from '@app/models/tipo-vehiculo/tipo-vehiculo.model';
import { FiltrosService } from '@app/services/filtros/filtros.service';
import { filtrosStrapiUtil } from '@app/util/filtros-strapi.util';

@Component({
  selector: 'app-buscador-tipo-vehiculo',
  templateUrl: './buscador-tipo-vehiculo.component.html',
  styleUrls: ['./buscador-tipo-vehiculo.component.scss']
})
export class BuscadorTipoVehiculoComponent implements OnInit {
  /**
   * Opciones de turno
   */
  @Input() tipoVehiculos: TipoVehiculo[] = [];

  public config: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 6,
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: true,
    loop: true,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      425: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 20
      },
      1200: {
        slidesPerView: 5,
        spaceBetween: 20
      }
    }
  };

  constructor(private filtrosService: FiltrosService) {}

  ngOnInit(): void {}

  filtroSeleccionado(filtro: string): void {
    this.filtrosService.enviarFiltro({
      origen: filtrosStrapiUtil.tabs.tipoVehiculo.nombreMenu,
      nombre: filtro
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscadorTipoVehiculoComponent } from './buscador-tipo-vehiculo.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BuscadorTipoVehiculoComponent', () => {
  let component: BuscadorTipoVehiculoComponent;
  let fixture: ComponentFixture<BuscadorTipoVehiculoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuscadorTipoVehiculoComponent],
      imports: [HttpClientTestingModule]
    })
      .overrideTemplate(BuscadorTipoVehiculoComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscadorTipoVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

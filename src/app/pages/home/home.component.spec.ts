import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BsModalRef,
  BsModalService,
  ModalModule,
  ModalOptions,
} from 'ngx-bootstrap/modal';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  const mockModal = {
    show<T = Object>(
      content:
        | string
        | TemplateRef<any>
        | {
            new (...args: any[]): T;
          },
      config?: ModalOptions<T>
    ): BsModalRef<T> {
      return {} as BsModalRef<T>;
    },
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [HttpClientTestingModule, ModalModule.forRoot()],
      providers: [BsModalRef, { provide: BsModalService, useValue: mockModal }],
    })
      .overrideTemplate(HomeComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

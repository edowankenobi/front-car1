import { Component, OnInit } from '@angular/core';
import { BannerHome } from '@app/models/banner-home/banner-home.model';
import { MarcaModel } from '@app/models/marcas/marca.model';
import { PublicidadHome } from '@app/models/publicidad-home/publicidad-home.model';
import { TipoVehiculo } from '@app/models/tipo-vehiculo/tipo-vehiculo.model';
import { BannerHomeService } from '@app/services/banner-home/banner-home.service';
import { LoadingService } from '@app/services/loading/loading.service';
import { MarcasService } from '@app/services/marcas/marcas.service';
import { PublicidadHomeService } from '@app/services/publicidad-home/publicidad-home.service';
import { TipoVehiculoService } from '@app/services/tipo-vehiculo/tipo-vehiculo.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalQuieroVenderComponent } from '../../ui/modals/modal-quiero-vender/modal-quiero-vender.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  /**
   * Arreglo de tipo de vehículos a mostrar en el carrusel
   */
  tipoVehiculos: TipoVehiculo[] = [];

  /**
   * Arreglo de publicidades a mostrar en el carrusel
   */
  publicidadHome: PublicidadHome[] = [];

  /**
   * Parámetros del banner a mostrar.
   */
  bannerHome: BannerHome = {};

  /**
   * Arreglo de marcas a mostrar en módulo de información de marcas
   */
  marcas: MarcaModel[] = [];

  /**
   * referencia al modal
   */
  bsModalRef!: BsModalRef;

  constructor(
    private bannerHomeService: BannerHomeService,
    private tipoVehiculoService: TipoVehiculoService,
    private publicidadHomeService: PublicidadHomeService,
    private marcasService: MarcasService,
    private loadingService: LoadingService,
    private modalService: BsModalService
  ) {
    this.inicializarBanner();
    this.inicializaCarruseles();
    this.inicializarMarcas();
  }

  /**
   * Método encargado de inicializar los parámetros del banner del home.
   */
  inicializarBanner(): void {
    this.loadingService.show();
    this.bannerHomeService.bannerHome().subscribe((response) => {
      this.bannerHome = response;
      this.loadingService.hide();
    });
  }
  /**
   * Método encargado de inicializar los carruseles del home.
   */
  inicializaCarruseles(): void {
    this.tipoVehiculoService.tipoVehiculos().subscribe((response) => {
      this.tipoVehiculos = response;
    });

    this.loadingService.show();
    this.publicidadHomeService.pubicidadHome().subscribe((response) => {
      this.publicidadHome = response;
      this.loadingService.hide();
    });
  }

  /**
   * Método encargado de inicializar las marcas mostradas en el buscador de marcas
   */
  inicializarMarcas(): void {
    this.loadingService.show();
    this.marcasService.marcasHome().subscribe((response) => {
      this.marcas = response;
      this.loadingService.hide();
    });
  }
  /**
   * Inicializa modal de quiero vender
   */
  initModalVender(): void {
    this.loadingService.show();
    const initialState = {
      modalHome: true,
    };
    this.bsModalRef = this.modalService.show(ModalQuieroVenderComponent, {
      initialState,
      backdrop: 'static',
      keyboard: false,
      class: 'modal-lg modal-dialog-centered',
    });

    setTimeout(() => {
      this.loadingService.hide();
    }, 2000);
  }

  ngOnInit(): void {
    //se elimina la carga del modal
    this.initModalVender();
  }
}

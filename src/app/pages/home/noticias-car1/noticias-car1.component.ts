import { Component, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';

@Component({
  selector: 'app-noticias-car1',
  templateUrl: './noticias-car1.component.html',
  styleUrls: ['./noticias-car1.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 5000, pauseOnFocus: true, noPause: false, showIndicators: true, indicatorsByChunk: false, noWrapSlides: false } }
 ]
})
export class NoticiasCar1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticiasCar1Component } from './noticias-car1.component';

describe('NoticiasCar1Component', () => {
  let component: NoticiasCar1Component;
  let fixture: ComponentFixture<NoticiasCar1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoticiasCar1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticiasCar1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

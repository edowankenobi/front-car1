import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BuscadorMarcasModule } from '@app/pages/home/buscador-marcas/buscador-marcas.module';
import { BuscadorTipoVehiculoModule } from '@app/pages/home/buscador-tipo-vehiculo/buscador-tipo-vehiculo.module';
import { InfoCompraModule } from '@app/ui/info-compra/info-compra.module';
import { ModalsModule } from '@app/ui/modals/modals.module';
import { AppSharedModule } from '@app/app.shared.module';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import {
  SwiperConfigInterface,
  SwiperModule,
  SWIPER_CONFIG,
} from 'ngx-swiper-wrapper';
import { BannerComponent } from './banner/banner.component';
import { CarruselPublicidadComponent } from './carrusel-publicidad/carrusel-publicidad.component';
import { HomeComponent } from './home.component';
import { NoticiasCar1Component } from './noticias-car1/noticias-car1.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: false,
};

@NgModule({
  declarations: [HomeComponent, BannerComponent, CarruselPublicidadComponent, NoticiasCar1Component],
  imports: [
    CommonModule,
    SwiperModule,
    InfoCompraModule,
    BuscadorTipoVehiculoModule,
    BuscadorMarcasModule,
    ModalsModule,
    CarouselModule,
    AppSharedModule
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    },
  ],
})
export class HomeModule {}

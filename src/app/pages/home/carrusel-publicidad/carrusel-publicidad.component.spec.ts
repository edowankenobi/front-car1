import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CarruselPublicidadComponent } from './carrusel-publicidad.component';

describe('CarruselPublicidadComponent', () => {
  let component: CarruselPublicidadComponent;
  let fixture: ComponentFixture<CarruselPublicidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarruselPublicidadComponent ],
      imports: [RouterTestingModule]
    })
      .overrideTemplate(CarruselPublicidadComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarruselPublicidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicidadHome } from '@app/models/publicidad-home/publicidad-home.model';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-carrusel-publicidad',
  templateUrl: './carrusel-publicidad.component.html',
  styleUrls: ['./carrusel-publicidad.component.scss'],
})
export class CarruselPublicidadComponent implements OnInit {
  /**
   * Opciones de turno
   */
  @Input() publicidadHome: PublicidadHome[] = [];

  /**
   * Flag que indica si redireccionamiento viene desde el banner
   */
  redirectFromBanner: boolean;

  public config: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 3,
    keyboard: true,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false,
    spaceBetween: 30,
    loop: true,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      550: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
    },
  };

  /**
   * Constructor
   * @param router router.
   */
  constructor(private router: Router) {
    this.redirectFromBanner = false;
  }

  ngOnInit(): void {}

  /**
   * Metodo que ordena el arreglo del banner, dejando el banner con orden 1 de primero.
   * @returns Banner array
   */
  orderBanner(): PublicidadHome[] {
    let orderedPublicidadHome: PublicidadHome[] = [];
    this.publicidadHome.forEach((item, i) => {
      if (item.Orden === 1) {
        this.publicidadHome.splice(i, 1);
        this.publicidadHome.unshift(item);
        orderedPublicidadHome = this.publicidadHome;
      }
    });
    return orderedPublicidadHome;
  }

  /**
   * Metodo que se gatilla al interactuar con las imagenes del banner.
   * @param index posicion del elemento.
   */
  doBannerInteract(index: number): void {
    const contactRedirectValidationByName =
      !!this.publicidadHome[index].Imagen.caption &&
      this.publicidadHome[index].Imagen.caption === 'banner-contacto';
    if (contactRedirectValidationByName) {
      this.router.navigateByUrl('/contacto', {
        state: { flag: this.redirectFromBanner, name: 'indentifier' },
      });
    }

    const stockRedirectValidationByName =
      !!this.publicidadHome[index].Imagen.caption &&
      this.publicidadHome[index].Imagen.caption === 'banner-stock-disponible';

      if (stockRedirectValidationByName) {
        this.router.navigateByUrl('/stock-disponible', {
          state: { flag: this.redirectFromBanner, name: 'indentifier' },
        });
      }

    const ventaRedirectValidationByName =
      !!this.publicidadHome[index].Imagen.caption &&
      this.publicidadHome[index].Imagen.caption === 'banner-venta';

      if (ventaRedirectValidationByName) {
        this.router.navigateByUrl('/venta-vehiculo', {
          state: { flag: this.redirectFromBanner, name: 'indentifier' },
        });
      }

  }
}

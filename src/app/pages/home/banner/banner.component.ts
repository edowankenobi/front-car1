import { Component, OnInit, Input, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BannerHome } from '@app/models/banner-home/banner-home.model';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  constructor(@Inject(DOCUMENT) private document: Document) {}

  /**
   * Parámetros de banner
   */
  @Input() bannerHome: BannerHome = {};

  ngOnInit(): void {}

  /**
   * Método de redirección en botones del banner.
   * @param url URL de cada botón
   */
  redirecciona(url: any): void {
    if (url != null) {
      this.document.location.href = url;
    }
  }

  /**
   * Método encargado de retornar la URL de la imagen configurada en Strapi
   */
  getUrl(): string {
    return 'url(' + this.bannerHome?.Imagen?.[0].url + ')';
  }
}

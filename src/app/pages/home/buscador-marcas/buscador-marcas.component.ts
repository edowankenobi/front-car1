import { Component, OnInit, Input } from '@angular/core';
import { MarcaModel } from '@app/models/marcas/marca.model';
import { FiltrosService } from '@app/services/filtros/filtros.service';
import { filtrosStrapiUtil } from '@app/util/filtros-strapi.util';

@Component({
  selector: 'app-buscador-marcas',
  templateUrl: './buscador-marcas.component.html',
  styleUrls: ['./buscador-marcas.component.scss']
})
export class BuscadorMarcasComponent implements OnInit {
  constructor(private filtrosService: FiltrosService) {}

  /**
   * Parámetros de marcas
   */
  @Input() marcas: MarcaModel[] = [];

  ngOnInit(): void {}

  filtroSeleccionado(filtro: string): void {
    this.filtrosService.enviarFiltro({
      origen: filtrosStrapiUtil.tabs.marca.nombreMenu,
      nombre: filtro
    });
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockDisponibleComponent } from '@app/pages/stock-disponible/stock-disponible.component';

const routes: Routes = [
  { path: 'stock-disponible', component: StockDisponibleComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })
  ],
  exports: [RouterModule]
})
export class BuscadorMarcasRoutingModule {}

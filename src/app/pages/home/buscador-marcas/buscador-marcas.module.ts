import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SwiperModule,
  SwiperConfigInterface,
  SWIPER_CONFIG
} from 'ngx-swiper-wrapper';
import { BuscadorMarcasRoutingModule } from '@app/pages/home/buscador-marcas/buscador-marcas-routing.module';
import { BuscadorMarcasComponent } from '@app/pages/home/buscador-marcas/buscador-marcas.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: false
};

@NgModule({
  declarations: [BuscadorMarcasComponent],
  imports: [CommonModule, BuscadorMarcasRoutingModule, SwiperModule],
  exports: [BuscadorMarcasComponent],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class BuscadorMarcasModule {}

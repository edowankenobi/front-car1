import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscadorMarcasComponent } from './buscador-marcas.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BuscadorMarcasComponent', () => {
  let component: BuscadorMarcasComponent;
  let fixture: ComponentFixture<BuscadorMarcasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BuscadorMarcasComponent],
      imports: [HttpClientTestingModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscadorMarcasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

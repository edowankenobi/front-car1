import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TasadorComponent } from '@app/pages/tasador/tasador.component';
import { BloqueHorarioPipe } from '@app/pipes/bloque-horario/bloque-horario.pipe';
import { ErrorUiModule } from '@app/ui/error-ui/error-ui.module';
import { ExitoUiModule } from '@app/ui/exito-ui/exito-ui.module';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { esLocale } from 'ngx-bootstrap/locale';
import { AgendamientoComponent } from './agendamiento/agendamiento.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { FormularioTasacionComponent } from './formulario-tasacion/formulario-tasacion.component';
import { FormularioTasacionAppComponent } from './formulario-tasacion-app/formulario-tasacion-app.component';
import { ResultadoTasacionComponent } from './resultado-tasacion/resultado-tasacion.component';
import { ResultadoTasacionAppComponent } from './resultado-tasacion-app/resultado-tasacion-app.component';
import { ResumenInspeccionComponent } from './resumen-inspeccion/resumen-inspeccion.component';
import { AppRoutingModule } from '../../app-routing.module';
import es from '@angular/common/locales/es';
import { NuestrosClientesModule } from './nuestros-clientes/nuestros-clientes.module';
import { PasosComponent } from './pasos/pasos.component';

defineLocale('es', esLocale);


@NgModule({
  declarations: [
    TasadorComponent,
    FormularioTasacionComponent,
    FormularioTasacionAppComponent,
    ResultadoTasacionComponent,
    ResultadoTasacionAppComponent,
    AgendamientoComponent,
    DatosPersonalesComponent,
    ResumenInspeccionComponent,
    BloqueHorarioPipe,
    PasosComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ErrorUiModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ExitoUiModule,
    AppRoutingModule,
    NuestrosClientesModule
  ],
  exports: [TasadorComponent],
  providers: [DatePipe],
})
export class TasadorModule {
  constructor(private bsLocaleService: BsLocaleService) {
    this.bsLocaleService.use('es');
  }
}

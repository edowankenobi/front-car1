import { DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DiasFeriadosResponseModel } from '@app/models/agenda/dias-feriados-response.model';
import { FechasDisponiblesResponseModel } from '@app/models/agenda/fechas-disponibles-response.model';
import { PeticionEnvioCita } from '@app/models/agenda/peticion-envio-cita.model';
import { RespuestaEnvioCita } from '@app/models/agenda/respuesta-envio-cita.model';
import { TerminosCondicionesResponseModel } from '@app/models/pdf/terminos-condiciones-response.model';
import { AnioTasadorResponseModel } from '@app/models/tasador/anio-tasador-response.model';
import { GuardarDetalleRequestModel } from '@app/models/tasador/guardar-detalle-request.model';
import { GuardarDetalleResponse } from '@app/models/tasador/guardar-detalle-response.model';
import { KilometrajeTasadorModel } from '@app/models/tasador/kilometraje-tasador.model';
import { MarcaTasadorResponseModel } from '@app/models/tasador/marca-tasador-response.model';
import { MarcaTasadorModel } from '@app/models/tasador/marca-tasador.model';
import { ModeloTasadorResponseModel } from '@app/models/tasador/modelo-tasador-response.model';
import { ModeloTasadorModel } from '@app/models/tasador/modelo-tasador.model';
import { PatenteTasadorResponseModel } from '@app/models/tasador/patente/patente-tasador-response.model';
import { PatenteTasadorModel } from '@app/models/tasador/patente/patente-tasador.model';
import { ValorTasacionResponseModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion-response.model';
import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';
import { VersionTasadorModel } from '@app/models/tasador/patente/version-tasador.model';
import { VersionTasadorResponseModel } from '@app/models/tasador/version-tasador-response.model';
import { AgendaService } from '@app/services/agenda/agenda.service';
import { CompraVehiculoFlujoService } from '@app/services/compra-vehiculo-flujo/compra-vehiculo-flujo.service';
import { PdfService } from '@app/services/pdf/pdf.service';
import { TasadorService } from '@app/services/tasador/tasador.service';
import { NgRecaptcha3Service } from 'ng-recaptcha3';
import { BsModalRef, BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { TasadorComponent } from './tasador.component';

describe('TasadorComponent', () => {
  let component: TasadorComponent;
  let fixture: ComponentFixture<TasadorComponent>;
  let formBuilder: FormBuilder;
  const routerMock = {
    url: 'ficha-vehiculo-iframe',
  };

  const recaptchaMock = {
    init(val: any) {},
    getToken() {
      return new Promise((resolve, reject) => {
        resolve('token2');
      });
    },
    destroy() {},
  };

  const agendaServiceMock = {
    enviarFormularioCita(
      formulario: PeticionEnvioCita
    ): Observable<RespuestaEnvioCita> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as RespuestaEnvioCita);
        observer.complete();
      });
    },
    obtenerDiasFeriados(): Observable<DiasFeriadosResponseModel> {
      return new Observable((observer) => {
        observer.next({
          codigo: '200',
          feriados: ['2020-10-20'],
        } as DiasFeriadosResponseModel);
        observer.complete();
      });
    },
    obtenerHorasDisponibles(
      dia: string
    ): Observable<FechasDisponiblesResponseModel> {
      return new Observable((observer) => {
        observer.next({
          codigo: '200',
          horas: { horasDisponibles: [{ inicioBloque: '12:12:12' }] },
        } as FechasDisponiblesResponseModel);
        observer.complete();
      });
    },
  };

  const compraVehiculoFlujoServiceMock = {
    registerDetailedData(
      data: GuardarDetalleRequestModel
    ): Observable<GuardarDetalleResponse> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as GuardarDetalleResponse);
        observer.complete();
      });
    },
  };

  const tasadorServiceMock = {
    kilometrajeSeleccionable(): Observable<KilometrajeTasadorModel[]> {
      return new Observable((observer) => {
        observer.next([{ id: 1 } as KilometrajeTasadorModel]);
        observer.complete();
      });
    },
    obtenerMarcas(): Observable<MarcaTasadorResponseModel> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as MarcaTasadorResponseModel);
        observer.complete();
      });
    },
    datosPorPatente(placa: string): Observable<PatenteTasadorResponseModel> {
      return new Observable((observer) => {
        observer.next({
          codigo: '200',
          datosPorPlaca: { modelID: 1, modelName: 'abc' },
        } as PatenteTasadorResponseModel);
        observer.complete();
      });
    },
    obtenerValorTasacion(
      idMarca: number,
      idModelo: number,
      anio: number,
      idVersion: number,
      idRequested: number,
      kilometraje: string,
      captcha: string
    ): Observable<ValorTasacionResponseModel> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as ValorTasacionResponseModel);
        observer.complete();
      });
    },
    obtenerModelos(idMarca: number): Observable<ModeloTasadorResponseModel> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as ModeloTasadorResponseModel);
        observer.complete();
      });
    },
    obtenerAnios(idModelo: number): Observable<AnioTasadorResponseModel> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as AnioTasadorResponseModel);
        observer.complete();
      });
    },
    obtenerVersiones(
      idModelo: number,
      idAnio: number
    ): Observable<VersionTasadorResponseModel> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as VersionTasadorResponseModel);
        observer.complete();
      });
    },
  };

  const pdfServiceMock = {
    obtenerPdfTerminosCondiciones(): Observable<TerminosCondicionesResponseModel> {
      return new Observable((observer) => {
        observer.next({} as TerminosCondicionesResponseModel);
        observer.complete();
      });
    },
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TasadorComponent],
      imports: [HttpClientTestingModule, ModalModule.forRoot()],
      providers: [
        FormBuilder,
        BsModalService,
        BsModalRef,
        DatePipe,
        { provide: Router, useValue: routerMock },
        { provide: NgRecaptcha3Service, useValue: recaptchaMock },
        { provide: AgendaService, useValue: agendaServiceMock },
        {
          provide: CompraVehiculoFlujoService,
          useValue: compraVehiculoFlujoServiceMock,
        },
        { provide: TasadorService, useValue: tasadorServiceMock },
        { provide: PdfService, useValue: pdfServiceMock },
      ],
    })
      .overrideTemplate(TasadorComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TasadorComponent);
    formBuilder = TestBed.inject(FormBuilder);
    component = fixture.componentInstance;
    component.patenteForm = formBuilder.group({
      patente: [],
    });
    component.agendamientoForm = formBuilder.group({
      inicio: [],
      fin: [],
      idProveedor: [],
      nombreProveedor: [],
    });
    component.datosPersonalesForm = formBuilder.group({
      nombre: [],
      apellidos: [],
      email: [],
      telefono: [],
      patente: [],
    });
    component.tasadorForm = formBuilder.group({
      marca: [],
      modelo: [],
      anio: [],
      version: [],
      kilometraje: [],
      captcha: [],
      email: [],
    });
    component.oferta = new ValorTasacionModel();
    component.esVenta = true;
    component.detailedDataSaved = new GuardarDetalleRequestModel();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('confirmarCita', () => {
    component.detailedDataSaved = new GuardarDetalleRequestModel();
    //component.datosTasacion = new ValorTasacionModel();
    component.confirmarCita();
    expect(component).toBeTruthy();
  });

  it('pasoSeleccionado', () => {
    component.pasoSeleccionado('paso1');
    expect(component.pasoTasacion).toEqual('paso1');
  });

  it('pasoSeleccionado', () => {
    component.pasoSeleccionado('paso1');
    expect(component.pasoTasacion).toEqual('paso1');
  });

  it('volverATasar resultado', () => {
    component.volverATasar('resultado');
    expect(component.pasoTasacion).toEqual('formulario');
  });

  it('volverATasar datosPersonales', () => {
    component.volverATasar('datosPersonales');
    expect(component.pasoTasacion).toEqual('formulario');
  });

  it('volverATasar exito', () => {
    component.volverATasar('exito');
    expect(component.pasoTasacion).toEqual('formulario');
  });

  it('finalizeSuccessfullProcess', () => {
    component.finalizeSuccessfullProcess();
    expect(component).toBeTruthy();
  });

  it('createDetailedDataToSaveFromTasacion', () => {
    component.createDetailedDataToSaveFromTasacion();
    expect(component).toBeTruthy();
  });

  it('buscarPatente', () => {
    component.buscarPatente();
    expect(component).toBeTruthy();
  });

  it('realizaTasacion', () => {
    component.realizaTasacion();
    expect(component).toBeTruthy();
  });

  it('aceptaOferta', () => {
    component.aceptaOferta();
    expect(component).toBeTruthy();
  });

  it('completarDatosFormulario', () => {
    component.oferta = new ValorTasacionModel();
    component.oferta.patente = 'ABC123';
    component.completarDatosFormulario();
    expect(component).toBeTruthy();
  });

  it('pasoDatosPersonales', () => {
    component.pasoDatosPersonales(true);
    expect(component).toBeTruthy();
  });

  it('verifyStep', () => {
    component.pasoTasacion = 'agendamiento';
    component.verifyStep();
    expect(component).toBeTruthy();
  });

  it('pasoAgendamiento esFlujoAlternativo false', () => {
    component.esFlujoAlternativo = false;
    component.pasoAgendamiento();
    expect(component).toBeTruthy();
  });

  it('pasoAgendamiento esFlujoAlternativo true', () => {
    component.esFlujoAlternativo = true;
    //component.datosVehiculo = {} as PatenteTasadorModel;
    component.pasoAgendamiento();
    expect(component).toBeTruthy();
  });

  it('pasoAgendamiento datosVehiculo null', () => {
    component.esFlujoAlternativo = true;
    component.datosVehiculo = undefined;
    component.pasoAgendamiento();
    expect(component).toBeTruthy();
  });

  it('pasoAgendamiento pasoResumen', () => {
    component.pasoResumen();
    expect(component).toBeTruthy();
  });

  it('onChangeMarca', () => {
    component.marcas = [{ id: 1 } as MarcaTasadorModel];
    component.onChangeMarca();
    expect(component).toBeTruthy();
  });

  it('onChangeModelo', () => {
    component.modelos = [{ id: 1 } as ModeloTasadorModel];
    component.onChangeModelo();
    expect(component).toBeTruthy();
  });

  it('onChangeAnio', () => {
    component.onChangeAnio();
    expect(component).toBeTruthy();
  });

  it('onChangeVersion', () => {
    component.versiones = [{ id: 1 } as VersionTasadorModel];
    component.onChangeVersion();
    expect(component).toBeTruthy();
  });

  it('onChangeKilometraje', () => {
    component.tasadorForm.controls.kilometraje.setValue(1);
    component.onChangeKilometraje();
    expect(component).toBeTruthy();
  });

  it('obtenerHorasDisponibles', () => {
    component.obtenerHorasDisponibles('2020-01-01');
    expect(component).toBeTruthy();
  });

  it('checkTerminosClicked', () => {
    component.checkTerminosClicked(true);
    expect(component).toBeTruthy();
  });
});

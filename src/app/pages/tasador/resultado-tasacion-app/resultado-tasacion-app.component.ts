import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TasadorCotizacionModel } from '@app/models/tasador-interno/tasador.cotizacion.model';
import { ModalsService } from '@app/services/modals/modals.service';

@Component({
  selector: 'app-resultado-tasacion-app',
  templateUrl: './resultado-tasacion-app.component.html',
  styleUrls: ['./resultado-tasacion-app.component.scss'],
})
export class ResultadoTasacionAppComponent implements OnInit {
  /**
   * Datos de la tasación
   */
  @Input() datosTasacion!: TasadorCotizacionModel;

  /**
   * Emmiter de evento de vuelta al formulario
   */
  @Output() pasoTasacion = new EventEmitter<string>();

  /**
   * Emmiter de evento de oferta aceptada
   */
  @Output() aceptaOfertaEmit = new EventEmitter();

  /**
   * Indica si es flujo de venta
   */
  @Input() esVenta!: boolean;

  constructor(private modalsService: ModalsService) {}

  ngOnInit(): void {
    registerLocaleData(es);
  }

  /**
   * Emite el evento de regreso al formulario
   */
  volverATasar(): void {
    this.pasoTasacion.emit('resultado');
  }

  /**
   * Emite el evento de aceptar oferta
   */
  aceptaOferta(): void {
    this.aceptaOfertaEmit.emit();
    this.modalsService.setModalAction(true);
  }
}

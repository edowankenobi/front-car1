import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoTasacionAppComponent } from './resultado-tasacion-app.component';

describe('ResultadoTasacionAppComponent', () => {
  let component: ResultadoTasacionAppComponent;
  let fixture: ComponentFixture<ResultadoTasacionAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadoTasacionAppComponent ]
    })
    .overrideTemplate(ResultadoTasacionAppComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoTasacionAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

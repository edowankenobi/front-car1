import { DatePipe } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Location, registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { HoraModel } from '@app/models/agenda/hora.model';
//import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';
import { AgendaService } from '@app/services/agenda/agenda.service';
import { LoadingService } from '@app/services/loading/loading.service';
import { Observable, Subscription } from 'rxjs';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { deLocale } from 'ngx-bootstrap/locale';
defineLocale('es', deLocale);
import * as user from '@app/store/user.actions'
import { Store } from '@ngrx/store';
import { AppState } from '@app/app.reducer';
import { environment } from '../../../../environments/environment.dev';
import { PeticionAgendaModel, PropiedadAgendaModel } from '@app/models/hubspot/peticion-agenda.model';
import { HubspotService } from '@app/services/hubspot/hubspot.services';
import { TasadorCotizacionModel } from '@app/models/tasador-interno/tasador.cotizacion.model';
declare const ga: any;

@Component({
  selector: 'app-agendamiento',
  templateUrl: './agendamiento.component.html',
  styleUrls: ['./agendamiento.component.scss'],
})
export class AgendamientoComponent implements OnInit, OnDestroy {
  locale = 'es' ;
   /*Subscripcion de datos */
   userSubcription!: Subscription;
  /**
   * Indica el día actual usado en el calendario
   */
  minDate = new Date();
  maxDate = new Date();
  year: any;
  fecha: any;
  dia: any;
  hora: any;
  reserva: any;


  /**
   * Arreglo de días feriados(deshabilitados) usados en el calendario
   */
  disabledDates: Date[];

  /**
   * Formulario de agendamiento
   */
  @Input() agendamientoForm!: FormGroup;

  /**
   * Emmiter de evento del paso al agendamiento de inspección
   */
  @Output() pasoDatosPersonales = new EventEmitter();

  /**
   * Emmiter de evento del paso al resumen de inspección
   */
  @Output() pasoResumen = new EventEmitter();

  /**
   * Arreglo con los horarios disponibles horario AM
   */
  @Input() horasDisponiblesAM!: HoraModel[];

  /**
   * Arreglo con los horarios disponibles horario PM
   */
  @Input() horasDisponiblesPM!: HoraModel[];

  /**
   * Emmiter de evento para obtener las horas disponibles
   */
  @Output() horasDisponibles = new EventEmitter();

  /**
   * Indicador de confirmación de cita de inspección
   */
  @Output() confirmarCita = new EventEmitter();

  @Output () horaBlock = new EventEmitter();

  /**
   * Controla el llamado al servicio de horas disponibles al momento de cargar el componente
   */
  contador: number;

  /**
   * Indica el día actual o seleccionado en el calendario
   */
  @Input() dayValue!: Date;

  /**
   * Suscripción con el arreglo de los días feriaodos
   */
  diasFeriadosArray: Subscription | undefined;

  /**
   * Datos de la tasación
  */
  @Input() datosTasacion!: TasadorCotizacionModel;

    /**
   * Indica que el identificador del negocio
   */
  @Input() idDeal: string = '';



  constructor(
    private loadingService: LoadingService,
    private agendaService: AgendaService,
    private store: Store<AppState>,
    private location: Location,
    private datePipe: DatePipe,
    private hubspotService: HubspotService
  ) {
    this.location.replaceState("auto-agenda-inspeccion")
    this.location.onUrlChange((url, state) => {
      if(typeof ga == "function"){
        ga('set', 'page', ''+url);
        ga('send', 'pageview' );
      }
    });
    this.contador = 0;
    this.disabledDates = [];
    this.diasFeriadosArray = this.agendaService.diasFeriados$.subscribe(
      (res) => {
        this.disabledDates = res;
      }
    );
      /*Calendar Fechas del día de hoy */
    this.year = this.minDate.getFullYear()
    let days: Array<string> = ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"]
    let meses: Array<string> = ["", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Octubre", "Noviembre", "Diciembre"]
    this.fecha = days[this.minDate.getDay()] + ", " + this.minDate.getDate() + " de " + meses[this.minDate.getMonth()]


      /*Selección de maximo tres días */
    // this.maxDate.setDate(this.maxDate.getDate() + 3);
    this.getMaxDate();
  }

  ngOnInit(): void {
    registerLocaleData(es);
  }

  ngOnDestroy(): void {
    if (this.diasFeriadosArray !== undefined) {
      this.diasFeriadosArray.unsubscribe();
    }
  }

    /**
   * Metodo que obtiene el dia max de fechas a seleccionar, de manera
   * que solo podran ser seleccionados 3 dias habiles mas el dia actual.
   */
     getMaxDate(): void {
      let contDias: number = 0;
      this.maxDate.setDate(this.minDate.getDate());
      while (contDias < environment.diasHabiles) {
        this.maxDate.setDate(this.maxDate.getDate() + 1);
        if(this.esDiaHabil(this.maxDate)){
          contDias++;
        }
      }
    }

      /**
   * Indica si la fecha enviada corresponde a un feriado.
   */
  esDiaHabil(fecha: Date): boolean {
    let respuesta:boolean = true;

    if(String(fecha).includes('Sun')){
      respuesta = false;
    }

    if (respuesta){
      for (let feriado of this.disabledDates) {
        if (String(this.datePipe.transform(feriado, 'dd-MM-yyyy')) ===
            String(this.datePipe.transform(fecha, 'dd-MM-yyyy'))){
          respuesta = false;
          break;
        }
      }
    }
    return respuesta;
  }

  /**
   * Emite el evento de enviar al formulario de datos personales
   */
  volver(): void {
    this.pasoDatosPersonales.emit();
  }

  
actualizaNegocioHubspot(idNegocio:string) : Observable<any> {

  console.log("entra a la actualización");
  const requestAgenda: PeticionAgendaModel = 
  {
    dealsId: idNegocio.toString(),
    inspector: this.agendamientoForm.controls.nombreProveedor.value,
    propiedades: {
      agenda_inspeccion: this.agendamientoForm.controls.fechaInspeccion.value,
      hora_de_visita: this.agendamientoForm.controls.inicioBloque.value  
    }
  };

  console.log(requestAgenda);
   
  return this.hubspotService.agendaNegocioHubspotSellIn(requestAgenda);
}
  /**
   * Emite el evento de confirmar proceso de venta vehículo
   */
  confirmar(): void {
    
    this.actualizaNegocioHubspot(this.idDeal).subscribe(response=>{
      this.confirmarCita.emit();
    });
  }
  /**
   * Método que indica la fecha seleccionada desde el calendario
   */
  onDayChange(fecha: Date): void {
    this.contador++;
    if (this.contador > 1) {
      this.loadingService.show(true);
      const dia = fecha.toISOString().split('T');
      this.horasDisponibles.emit(dia[0]);
      this.dia =  this.horasDisponibles.emit(dia[0]);
      this.agendamientoForm.reset();
    }

    // const dia = fecha.toISOString().split('T');
    // this.horasDisponibles.emit(dia[0]);
    // this.dia =  this.horasDisponibles.emit(dia[0]);

  }

  /**
   * Método que indica el bloque horario seleccionado
   */
  horaSeleccionada(hora: HoraModel): void {
    this.horaBlock.emit(hora.inicioBloque)
    console.log(hora.inicioBloque)
    const inicioBloque =  user.userData({content: hora.inicioBloque})
    this.store.dispatch(inicioBloque);

    this.resetHoraSeleccionada();
    hora.bloqueSeleccionado = true;
    const x = hora.inicio
    this.hora = x

    this.agendamientoForm.controls.idProveedor.setValue(hora.idProveedor);
    this.agendamientoForm.controls.nombreProveedor.setValue(
      hora.nombreProveedor
    );
    this.agendamientoForm.controls.inicio.setValue(hora.inicio);
    this.agendamientoForm.controls.fin.setValue(hora.fin);
    this.agendamientoForm.controls.inicioBloque.setValue(hora.inicioBloque);
    this.agendamientoForm.controls.fechaInspeccion.setValue(
      hora.inicio.split('T')[0]
    );
  }

  /**
   * Método que inicializa bloque seleccionado
   */
  resetHoraSeleccionada(): void {
    if (this.horasDisponiblesAM.length > 0) {
      this.horasDisponiblesAM.forEach((hora) => {
        hora.bloqueSeleccionado = false;
        console.log(hora.bloqueSeleccionado)
      });
    }

    if (this.horasDisponiblesPM.length > 0) {
      this.horasDisponiblesPM.forEach((hora) => {
        hora.bloqueSeleccionado = false;
        console.log(hora.bloqueSeleccionado)
      });
    }
  }

  /**
   * Retorna si el botón siguiente debe estar deshabilitado o no
   */
  confirmarDisabled(): boolean {
    return this.agendamientoForm.invalid;
  }
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { HoraModel } from '@app/models/agenda/hora.model';

import { AgendamientoComponent } from './agendamiento.component';

describe('AgendamientoComponent', () => {
  let component: AgendamientoComponent;
  let fixture: ComponentFixture<AgendamientoComponent>;
  let formBuilder: FormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgendamientoComponent ],
      imports: [HttpClientTestingModule],
      providers: [FormBuilder]
    })
    .overrideTemplate(AgendamientoComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendamientoComponent);
    formBuilder = TestBed.inject(FormBuilder);
    component = fixture.componentInstance;
    component.agendamientoForm = formBuilder.group({
      idProveedor: [],
      nombreProveedor: [],
      inicio: [],
      fin: [],
      inicioBloque: [],
      fechaInspeccion: [],
    });
    component.horasDisponiblesAM = [{} as HoraModel];
    component.horasDisponiblesPM = [{} as HoraModel];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnDestroy', () => {
    component.ngOnDestroy();
    expect(component).toBeTruthy();
  });

  it('ngOnDestroy', () => {
    component.diasFeriadosArray = undefined;
    component.ngOnDestroy();
    expect(component).toBeTruthy();
  });

  it('volver', () => {
    component.volver();
    expect(component).toBeTruthy();
  });

  it('confirmar', () => {
    component.confirmar();
    expect(component).toBeTruthy();
  });

  it('onDayChange', () => {
    component.onDayChange(new Date());
    expect(component).toBeTruthy();
  });

  it('onDayChange', () => {
    component.contador = 1;
    component.onDayChange(new Date());
    expect(component).toBeTruthy();
  });

  it('horaSeleccionada', () => {
    component.horaSeleccionada({inicio: '12:12:12T12'} as HoraModel);
    expect(component).toBeTruthy();
  });

  it('resetHoraSeleccionada', () => {
    component.resetHoraSeleccionada();
    expect(component).toBeTruthy();
  });

  it('resetHoraSeleccionada', () => {
    component.horasDisponiblesAM = [];
    component.horasDisponiblesPM = [];
    component.resetHoraSeleccionada();
    expect(component).toBeTruthy();
  });

  it('confirmarDisabled empty', () => {
    component.confirmarDisabled();
    expect(component).toBeTruthy();
  });
});

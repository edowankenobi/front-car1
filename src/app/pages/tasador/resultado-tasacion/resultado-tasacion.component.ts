import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';
import { ModalsService } from '@app/services/modals/modals.service';

@Component({
  selector: 'app-resultado-tasacion',
  templateUrl: './resultado-tasacion.component.html',
  styleUrls: ['./resultado-tasacion.component.scss'],
})
export class ResultadoTasacionComponent implements OnInit {
  /**
   * Datos de la tasación
   */
  @Input() datosTasacion!: ValorTasacionModel;

  /**
   * Emmiter de evento de vuelta al formulario
   */
  @Output() pasoTasacion = new EventEmitter<string>();

  /**
   * Emmiter de evento de oferta aceptada
   */
  @Output() aceptaOfertaEmit = new EventEmitter();

  /**
   * Indica si es flujo de venta
   */
  @Input() esVenta!: boolean;

  /**
   * Emmiter de evento del paso al agendamiento de inspección
   */
  @Output() pasoDatosPersonales = new EventEmitter();

  constructor(private modalsService: ModalsService) {}

  ngOnInit(): void {
    registerLocaleData(es);
  }

  /**
   * Emite el evento de regreso al formulario
   */
  volverATasar(): void {
    this.pasoTasacion.emit('resultado');
  }

  /**
   * Emite el evento de aceptar oferta
   */
  aceptaOferta(): void {
    this.aceptaOfertaEmit.emit();
    this.modalsService.setModalAction(true);
  }

  /**
   * Emite el evento de enviar al formulario de agendamiento
   */
  agendarInspeccion(): void {
    this.pasoDatosPersonales.emit();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoTasacionComponent } from './resultado-tasacion.component';

describe('ResultadoTasacionComponent', () => {
  let component: ResultadoTasacionComponent;
  let fixture: ComponentFixture<ResultadoTasacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadoTasacionComponent ]
    })
    .overrideTemplate(ResultadoTasacionComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoTasacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

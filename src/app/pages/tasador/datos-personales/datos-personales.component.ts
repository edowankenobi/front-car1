import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
//import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';
import { LoadingService } from '@app/services/loading/loading.service';
import { ModalsService } from '@app/services/modals/modals.service';
import { TasadorService } from '@app/services/tasador/tasador.service';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '@app/app.reducer';
import es from '@angular/common/locales/es';
import  { Location, registerLocaleData } from '@angular/common';
import { HubspotService } from '@app/services/hubspot/hubspot.services';
import { PeticionContacto } from '@app/models/hubspot/peticion-contacto.model';
import { TasadorCotizacionModel } from '@app/models/tasador-interno/tasador.cotizacion.model';
declare const ga: any;

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.scss'],
})
export class DatosPersonalesComponent implements OnInit {
     /**
   * Datos de la tasación
   */
    @Input() datosTasacion!: TasadorCotizacionModel;
  /**
   * Formulario de patente
   */
  @Input() patenteForm!: FormGroup;

  /**
   * Formulario de tasación
   */
  @Input() tasadorForm!: FormGroup;

  /**
   * Formulario de datos personales
   */
  @Input() datosPersonalesForm!: FormGroup;

  /**
   * Emmiter de evento de vuelta al formulario
   */
  @Output() pasoTasacion = new EventEmitter<string>();

  /**
   * Emmiter de evento para ir al paso agendamiento
   */
  @Output() pasoAgendamiento = new EventEmitter();

  /**
   * Emmiter de evento para obtener las horas disponibles
   */
  @Output() horasDisponibles = new EventEmitter();

  /**
   * Indica si datos del vehículo con la patente son correctos
   */
  datosCorrectosPatente!: boolean;

  /**
   * Indicador busqueda de patente sin resultado
   */
  patenteSinDatos!: boolean;

  /**
   * Indica si es flujo alternativo SIN tasación
   */
  @Input() esFlujoAlternativo!: boolean;

     /**
      * Emmiter de evento de oferta aceptada
      */
     @Output() aceptaOfertaEmit = new EventEmitter();

     /**
      * Indica si es flujo de venta
      */
     @Input() esVenta!: boolean;

     /*Subcripcion */
     userSubcription!: Subscription;
     data: any;
     phone: any;
     email: any;
     patente: any;
     disablePatente: boolean;

  constructor(
    private tasadorService: TasadorService,
    private loadingService: LoadingService,
    private modalsService: ModalsService,
    private store: Store <AppState>,
    private location: Location,
    private hubspotService : HubspotService
  ) {
    this.datosCorrectosPatente = true;
    this.disablePatente = false;
    this.location.replaceState("auto-recibe-oferta")
    this.location.onUrlChange((url, state) => {
      if(typeof ga == "function"){
        ga('set', 'page', ''+url);
        ga('send', 'pageview' );
      }
    });
  }

  ngOnInit(): void {
    registerLocaleData(es);
    this.userSubcription = this.store.select('user').subscribe(x => this.data = x);
    const x = Object.keys(this.data).map(x => ({ type: x, value: this.data[x] }));
    this.data = x;

    const phone = this.data[1].value
    this.phone = phone

    const email = this.data[0].value
    this.email = email

    this.patente =
      this.data[2].value !== undefined && this.data[2].value !== null && this.data[2].value !== ''
      ? this.data[2].value
      : '';
    this.disablePatente = this.patente !== '';
  }

  /**
   * Emite el evento de regreso al formulario de tasación
   */
  volverATasar(): void {
    this.pasoTasacion.emit('datosPersonales');
    this.modalsService.setModalAction(true);

  }

  /**
   * Emite el evento de enviar al formulario de agendamiento
   */
  agendamiento(): void {
    this.pasoAgendamiento.emit();
  }

  /**
   * Retorna si el botón siguiente debe estar deshabilitado o no
   */
  siguienteDisabled(): boolean {
    return this.datosPersonalesForm.invalid;
  }

  /**
   * Emite el evento para continuar con la vista de agendamiento
   */
  siguiente(): void 
  {
    
    this.registraContactoHubspot().subscribe(response => {    
      
      this.loadingService.show(true);
      this.obtenerHorasDisponibles();
      this.pasoAgendamiento.emit();
    });
  }

  registraContactoHubspot() : Observable<any> {
    const requestContacto = new PeticionContacto(
      this.datosPersonalesForm.controls.nombre.value,
      this.datosPersonalesForm.controls.apellidos.value,
      '',
      this.tasadorForm.controls.email.value,
      this.tasadorForm.controls.telefono.value
    );

    return this.hubspotService.enviarContactoHubspotSellIn(requestContacto);
  }

  /**
   * Método que valida que los datos de la patente correspondan al vehículo
   */
  validaPatente(): void {
    if (this.datosPersonalesForm.controls.patente.valid) {
      this.loadingService.show(true);
      this.datosCorrectosPatente = true;
      this.patenteSinDatos = false;
      this.tasadorService
        .datosPorPatente(this.datosPersonalesForm.controls.patente.value)
        .subscribe((response) => {
          if (response != null && response.codigo === '200') {
            if (
              response.datosPorPlaca.brandID ===
                Number(this.tasadorForm.controls.marca.value) &&
              response.datosPorPlaca.modelID ===
                Number(this.tasadorForm.controls.modelo.value) &&
              response.datosPorPlaca.year ===
                Number(this.tasadorForm.controls.anio.value)
            ) {
              this.obtenerHorasDisponibles();
            } else {
              this.loadingService.hide();
              this.datosCorrectosPatente = false;
            }
          } else {
            this.loadingService.hide();
            this.patenteSinDatos = true;
            throw new Error(
              '[datosPersonalesComponent][validarPatente] Error al validar patente'
            );
          }
        });
    }
  }

  /**
   * Emite el evento que obtiene las horas disponibles para la fecha actual
   */
  obtenerHorasDisponibles(): void {
    const dia = new Date().toISOString().split('T');
    this.horasDisponibles.emit(dia[0]);
  }

  /**
   * Método que limpia mensaje de error cuando la patente no coincide con la info del vehículo
   */
  limpiaMensajePatente(): void {
    this.datosCorrectosPatente = true;
    this.patenteSinDatos = false;
  }


    /**
     * Emite el evento de aceptar oferta
     */
    aceptaOferta(): void {
      this.aceptaOfertaEmit.emit();
      this.modalsService.setModalAction(true);
    }

    /**
     * Emite el evento de enviar al formulario de agendamiento
     */
    // agendarInspeccion(): void {
    //   this.pasoDatosPersonales.emit();
    // }
}

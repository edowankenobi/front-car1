import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { PatenteTasadorResponseModel } from '@app/models/tasador/patente/patente-tasador-response.model';
import { TasadorService } from '@app/services/tasador/tasador.service';
import { Observable } from 'rxjs';

import { DatosPersonalesComponent } from './datos-personales.component';

describe('DatosPersonalesComponent', () => {
  let component: DatosPersonalesComponent;
  let fixture: ComponentFixture<DatosPersonalesComponent>;
  let formBuilder: FormBuilder;


  const tasadorServiceMock = {
    datosPorPatente(
      placa: string
    ): Observable<PatenteTasadorResponseModel> {
      return new Observable((observer) => {
        observer.next({ codigo: '200', datosPorPlaca: { brandID: 1, modelID: 1, year: 2020} } as PatenteTasadorResponseModel);
        observer.complete();
      });
    },
  }
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosPersonalesComponent ],
      imports: [ HttpClientTestingModule ],
      providers: [
        FormBuilder,
        { provide: TasadorService, useValue: tasadorServiceMock},]
    })
    .overrideTemplate(DatosPersonalesComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosPersonalesComponent);
    formBuilder = TestBed.inject(FormBuilder);
    component = fixture.componentInstance;
    component.datosPersonalesForm = formBuilder.group({
      nombre: [],
      apellidos: [],
      email: [],
      telefono: [],
      patente: [],
    });
    component.tasadorForm = formBuilder.group({
      marca: ['', []],
      modelo: ['', []],
      anio: ['', []],
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('volverATasar', () => {
    component.volverATasar();
    expect(component).toBeTruthy();
  });

  it('agendamiento', () => {
    component.agendamiento();
    expect(component).toBeTruthy();
  });

  it('siguienteDisabled', () => {
    component.siguienteDisabled();
    expect(component).toBeTruthy();
  });

  it('siguiente', () => {
    component.siguiente();
    expect(component).toBeTruthy();
  });

  it('validaPatente', () => {
    component.validaPatente();
    expect(component).toBeTruthy();
  });

  it('validaPatente if true', () => {
    component.tasadorForm = formBuilder.group({
      marca: [1, []],
      modelo: [1, []],
      anio: [2020, []],
    });
    component.validaPatente();
    expect(component).toBeTruthy();
  });

  it('obtenerHorasDisponibles', () => {
    component.obtenerHorasDisponibles();
    expect(component).toBeTruthy();
  });

  it('limpiaMensajePatente', () => {
    component.limpiaMensajePatente();
    expect(component).toBeTruthy();
  });
});

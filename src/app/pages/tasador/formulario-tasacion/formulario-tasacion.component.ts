import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { KilometrajeTasadorModel } from '@app/models/tasador/kilometraje-tasador.model';
import { MarcaTasadorModel } from '@app/models/tasador/marca-tasador.model';
import { ModeloTasadorModel } from '@app/models/tasador/modelo-tasador.model';
import { VersionTasadorModel } from '@app/models/tasador/patente/version-tasador.model';
import { ModalsService } from '@app/services/modals/modals.service';
import { Subscription } from 'rxjs';
import * as user from '@app/store/user.actions';
import { Store } from '@ngrx/store';
import { AppState } from '@app/app.reducer';
import { Location, registerLocaleData } from '@angular/common';

import { TasadorIdNombreModel } from '@app/models/tasador-interno/tasador.idnombre.model';
import { TasadorIdValorModel } from '@app/models/tasador-interno/tasador.idvalor.model';
declare const ga: any;

@Component({
  selector: 'app-formulario-tasacion',
  templateUrl: './formulario-tasacion.component.html',
  styleUrls: ['./formulario-tasacion.component.scss'],
})
export class FormularioTasacionComponent implements OnInit {
  /**
   * Formulario de patente
   */
  @Input() patenteForm!: FormGroup;

  /**
   * Formulario de tasación
   */
  @Input() tasadorForm!: FormGroup;

  /**
   * Input con valores del select de Kilometraje
   */
  @Input() valoresSelectKilometraje!: TasadorIdValorModel<number, string>[];

  /**
   * Datos de versiones del vehículo a tasar
   */
  @Input() versiones!: TasadorIdNombreModel[];

  /**
   * Indicador submitted
   */
  @Input() isSubmitted!: boolean;

  /**
   * Indicador submitted tasación
   */
  @Input() isSubmittedTasacion!: boolean;

  /**
   * Indicador del paso en el que se encuentra el proceso de tasación
   */
  @Output() pasoTasacion = new EventEmitter<string>();

  /**
   * Indicador de búsqueda de pantente
   */
  @Output() buscaPatenteEmit = new EventEmitter();

  /**
   * Indicador de limpieza de formulario
   */
  @Output() limpiarFormEmit = new EventEmitter();

  /**
   * Indicador de realización de proceso de tasación
   */
  @Output() realizaTasacionEmit = new EventEmitter();

  /**
   * Indicador busqueda de patente sin resultado
   */
  @Input() patenteSinDatos!: boolean;

  /**
   * Indica si es flujo de venta
   */
  @Input() esVenta!: boolean;

  /**
   * Listado de marcas
   */
  @Input() marcas!: TasadorIdNombreModel[];

  /**
   * Indicador de realización de proceso de selección de marca
   */
  @Output() changeMarcaEmit = new EventEmitter();

  /**
   * Listado de modelos según marca seleccionada
   */
  @Input() modelos!: ModeloTasadorModel[];

  /**
   * Indicador de realización de proceso de selección de modelo
   */
  @Output() changeModeloEmit = new EventEmitter();

  /**
   * Listado de años según modelo seleccionada
   */
  @Input() anios!: TasadorIdNombreModel[];

  /**
   * Indicador de realización de proceso de selección de año
   */
  @Output() changeAnioEmit = new EventEmitter();

  /**
   * url pdf Términos y Condiciones
   */
  @Input() urlPdfTerminosCondiciones!: string;

  /**
   * Indica si el año del vehículo es valido para la tasación
   */
  @Input() anioValido!: boolean;

  /**
   * Indica si el modelo del vehículo tiene años validos para la tasación
   */
  @Input() aniosValidos!: boolean;

  
  /**
   * Indica que el correo ingresado es incorrecto
   */
   @Input() esErrorEmail: boolean = false;

  /**
   * Emmiter de evento del paso al agendamiento de inspección
   */
  @Output() pasoDatosPersonales = new EventEmitter();

  /**
   * Indica que no están aceptados los términos y condiciones
   */
  @Input() noCheckTerminosCondiciones!: boolean;

  /**
   * Indica si el año del vehículo tiene versiones validos para la tasación
   */
  @Input() versionesValidas!: boolean;

  /**
   * Indicador de realización de proceso de selección de la versión
   */
  @Output() changeVersionEmit = new EventEmitter();

  /**
   * Indicador de realización de proceso de selección del kilometraje
   */
  @Output() changeKilometrajeEmit = new EventEmitter();

  /**
   * Indica que el botón Buscar está deshabilitado
   */
  @Input() disableBuscar!: boolean;

  /**
   * Indica si ha sido clickeado el check de terminos y condiciones.
   */
  @Output() checkTerminos = new EventEmitter<boolean>();
  /*Subscripcion de datos */
  userSubcription!: Subscription;
  phone: string = '+56';
  constructor(
    private modalsService: ModalsService,
    private store: Store<AppState>,
    private location: Location
  ) {

  }

  ngOnInit(): void {
    this.location.replaceState('auto-quiero-vender');
    // this.location.onUrlChange((url, state) => {
    //   if(typeof ga == "function"){
    //     ga('set', 'page', ''+url);
    //     ga('send', 'pageview' );
    //   }
    // });
  }

  /**
   * Método encargado de buscar información de vehículo en base a la patente
   */
  buscaPatente(): void {
    this.buscaPatenteEmit.emit();
  }

  /**
   * Método encargado de realizar la tasación en base a los datos del vehículo
   */
  realizaTasacion(): void {



    this.realizaTasacionEmit.emit();
    this.modalsService.setModalAction(true);
    const { email, telefono } = this.tasadorForm.value;
    const { patente } = this.patenteForm.value;
    const emailActions = user.changeEmail({ content: email });
    const phoneActions = user.changeTelefono({ content: telefono });
    const patenteActions = user.changePatente({ content: patente });

    this.store.dispatch(emailActions);
    this.store.dispatch(phoneActions);
    this.store.dispatch(patenteActions);
  }

  /**
   * Retorna si el botón tasar debe estar deshabilitado o no
   */
  tasarDisabled(): boolean {
    return (
      this.tasadorForm.invalid ||
      (this.esVenta && !this.tasadorForm.controls.terminosCondiciones.value) ||
      (!this.isSubmitted &&
        this.patenteForm.controls.patente.value !== '' &&
        this.patenteForm.controls.patente.value !== null) ||
      (!this.esVenta && !this.isSubmitted) ||
      !this.anioValido
    );
  }

  /**
   * Método encargado de limpiar el formulario de tasación
   */
  limpiarFormTasador(): void {
    this.limpiarFormEmit.emit();
  }

  /**
   * Método que emite la marca seleccionada
   */
  onChangeMarca(): void {
    this.changeMarcaEmit.emit();
  }

  /**
   * Método que emite el modelo seleccionado
   */
  onChangeModelo(): void {
    this.changeModeloEmit.emit();
  }

  /**
   * Método que emite el año seleccionado
   */
  onChangeAnio(): void {
    this.changeAnioEmit.emit();
  }

  /**
   * Método que emite la versión seleccionada
   */
  onChangeVersion(): void {
    this.changeVersionEmit.emit();
  }

  /**
   * Método que emite el kilometraje seleccionado
   */
  onChangeKilometraje(): void {
    this.changeKilometrajeEmit.emit();
  }

  /**
   * Método que limpia mensaje de error cuando no existe información de la patente
   */
  limpiaMensajePatente(): void {
    this.patenteSinDatos = false;
  }

  /**
   * Emite el evento de enviar al formulario de agendamiento
   */
  agendarInspeccion(): void {
    if (!this.tasadorForm.controls.terminosCondiciones.value) {
      this.noCheckTerminosCondiciones = true;
    } else {
      this.pasoDatosPersonales.emit(true);
    }
  }

  /**
   * Indica si se aceptaron o no los términos y condiciones
   */
  aceptarTerminosCondiciones(): void {
    if (
      this.esVenta &&
      !this.versionesValidas &&
      this.tasadorForm.controls.terminosCondiciones.value
    ) {
      this.noCheckTerminosCondiciones = true;
    } else {
      this.noCheckTerminosCondiciones = false;
    }
    this.checkTerminos.emit(true);
  }
}

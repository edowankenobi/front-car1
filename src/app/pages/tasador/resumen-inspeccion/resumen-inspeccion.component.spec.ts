import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenInspeccionComponent } from './resumen-inspeccion.component';

describe('ResumenInspeccionComponent', () => {
  let component: ResumenInspeccionComponent;
  let fixture: ComponentFixture<ResumenInspeccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResumenInspeccionComponent ]
    })
    .overrideTemplate(ResumenInspeccionComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenInspeccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';
import { FormGroup } from '@angular/forms';
import { LoadingService } from '@app/services/loading/loading.service';

@Component({
  selector: 'app-resumen-inspeccion',
  templateUrl: './resumen-inspeccion.component.html',
  styleUrls: ['./resumen-inspeccion.component.scss']
})
export class ResumenInspeccionComponent implements OnInit {

  /**
   * Emmiter de evento para ir al paso agendamiento
   */
  @Output() pasoAgendamiento = new EventEmitter();

  /**
   * Indicador de confirmación de cita de inspección
   */
  @Output() confirmarCita = new EventEmitter();

  /**
   * Datos de la tasación
   */
  @Input() datosTasacion!: ValorTasacionModel;

  /**
   * Formulario de datos del agendamiento
   */
  @Input() agendamientoForm!: FormGroup;

  /**
   * Emmiter de evento para obtener las horas disponibles
   */
  @Output() horasDisponibles = new EventEmitter();

  constructor(private loadingService: LoadingService) {
  }

  ngOnInit(): void {
    registerLocaleData(es);
  }

  /**
   * Emite el evento de enviar al formulario de agendamiento
   */
  volver(): void {
    this.loadingService.show(true);
    const dia = new Date().toISOString().split('T');
    this.horasDisponibles.emit(dia[0]);
  }

  /**
   * Emite el evento de confirmar proceso de venta vehículo
   */
  confirmar(): void {
    this.confirmarCita.emit();
  }

}

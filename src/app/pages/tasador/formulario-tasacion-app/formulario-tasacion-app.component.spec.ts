import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormularioTasacionAppComponent } from './formulario-tasacion-app.component';

describe('FormularioTasacionAppComponent', () => {
  let component: FormularioTasacionAppComponent;
  let fixture: ComponentFixture<FormularioTasacionAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormularioTasacionAppComponent],
      imports: [ReactiveFormsModule],
    })
      .overrideTemplate(FormularioTasacionAppComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioTasacionAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('debe llamar emit buscaPatenteEmit', () => {
    expect(component).toBeTruthy();
    const getItemSpy = jest.spyOn(component.buscaPatenteEmit, 'emit');
    component.buscaPatente();
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('debe llamar emit realizaTasacionEmit', () => {
    expect(component).toBeTruthy();
    const getItemSpy = jest.spyOn(component.realizaTasacionEmit, 'emit');
    component.realizaTasacion();
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('debe llamar emit limpiarFormEmit', () => {
    expect(component).toBeTruthy();
    const getItemSpy = jest.spyOn(component.limpiarFormEmit, 'emit');
    component.limpiarFormTasador();
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('debe llamar emit changeMarcaEmit', () => {
    expect(component).toBeTruthy();
    const getItemSpy = jest.spyOn(component.changeMarcaEmit, 'emit');
    component.onChangeMarca();
    expect(getItemSpy).toHaveBeenCalled();
  });

  it('debe llamar emit changeModeloEmit', () => {
    expect(component).toBeTruthy();
    const getItemSpy = jest.spyOn(component.changeModeloEmit, 'emit');
    component.onChangeModelo();
    expect(getItemSpy).toHaveBeenCalled();
  });
});

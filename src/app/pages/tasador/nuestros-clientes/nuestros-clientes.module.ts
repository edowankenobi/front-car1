import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NuestrosClientesComponent } from './nuestros-clientes.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { AppSharedModule } from '@app/app.shared.module';

@NgModule({
  declarations: [
    NuestrosClientesComponent,
  ],
  imports: [
    CommonModule,
    CarouselModule,
    AppSharedModule
  ],
  exports: [NuestrosClientesComponent]
})
export class NuestrosClientesModule { }

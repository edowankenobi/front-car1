import { Component, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';

@Component({
  selector: 'app-nuestros-clientes',
  templateUrl: './nuestros-clientes.component.html',
  styleUrls: ['./nuestros-clientes.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 7000, pauseOnFocus: true, noPause: false, showIndicators: true, indicatorsByChunk: false, noWrapSlides: false } }
  ]
})
export class NuestrosClientesComponent implements OnInit {

  constructor() { }

  opinions: any[] = [
    {
      img: '/assets/images/avatar/avatar-felipe.jpg',
      alt: 'avatar Felipe',
      name: 'Felipe González Reyes',
      opinion: '“Compramos nuestro auto desde región y el servicio fue muy rápido y expedito”'
    },
    {
      img: '/assets/images/avatar/avatar-edison.jpg',
      alt: 'avatar Edison',
      name: 'Edison Araneda Reyes',
      opinion: '“Les vendí mi auto y todos los trámites salieron super bien”'
    },
    {
      img: '/assets/images/avatar/avatar-christian.jpg',
      alt: 'avatar Christian',
      name: 'Christian Oyarzún',
      opinion: '“Excelente servicio, me pagaron en menos de 1 hora y más que la competencia”'
    }
  ]

  ngOnInit(): void {
  }

}

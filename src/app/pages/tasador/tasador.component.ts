import { DatePipe } from '@angular/common';
import { Component, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/app.reducer';
import { HoraModel } from '@app/models/agenda/hora.model';
import { PeticionEnvioCita } from '@app/models/agenda/peticion-envio-cita.model';

import { MarcaTasadorModel } from '@app/models/tasador/marca-tasador.model';
import { ModeloTasadorModel } from '@app/models/tasador/modelo-tasador.model';
import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';

import { AgendaService } from '@app/services/agenda/agenda.service';
import { LoadingService } from '@app/services/loading/loading.service';
import { PdfService } from '@app/services/pdf/pdf.service';
import { TasadorService } from '@app/services/tasador-interno/tasador-interno.service';
import { userData } from '@app/store/user.actions';
import { ValidacionesCustom } from '@app/util/validaciones-custom';
import { environment } from '@env/environment';
import { Store } from '@ngrx/store';
import { NgRecaptcha3Service } from 'ng-recaptcha3';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable, Subscription } from 'rxjs';

import { GuardarDetalleRequestModel } from '../../models/tasador/guardar-detalle-request.model';
import { GuardarDetalleResponse } from '../../models/tasador/guardar-detalle-response.model';

import { CompraVehiculoFlujoService } from '../../services/compra-vehiculo-flujo/compra-vehiculo-flujo.service';
import { RespuestaEnvioCorreoComponent } from '../../ui/modals/respuesta-envio-correo/respuesta-envio-correo.component';
import { HubspotService } from '@app/services/hubspot/hubspot.services';
import { PeticionContacto } from '@app/models/hubspot/peticion-contacto.model';
import { PeticionNegocio } from '@app/models/hubspot/peticion-negocio.model';
import * as user from '@app/store/user.actions';

import { TasadorIdValorModel } from '@app/models/tasador-interno/tasador.idvalor.model';
import { TasadorPPUModel } from '@app/models/tasador-interno/tasador.ppu.model';
import { TasadorIdNombreModel } from '@app/models/tasador-interno/tasador.idnombre.model';
import { TasadorCotizacionModel, TasadorCotizacionMethods } from '@app/models/tasador-interno/tasador.cotizacion.model';
declare const ga: any;
@Component({
  selector: 'app-tasador',
  templateUrl: './tasador.component.html',
  styleUrls: ['./tasador.component.scss'],
})
export class TasadorComponent implements OnInit, OnDestroy {
  userSubcription!: Subscription;

  /**
   * Formulario de patente
   */
  patenteForm!: FormGroup;

  /**
   * Formulario de tasación
   */
  tasadorForm!: FormGroup;

  /**
   * Formulario de datos personales
   */
  datosPersonalesForm!: FormGroup;

  /**
   * Formulario de datos del agendamiento
   */
  agendamientoForm!: FormGroup;

  /**
   * Indicador submitted
   */
  isSubmitted = false;

  /**
   * Indicador sumbitted tasación
   */
  isSubmittedTasacion = false;

  /**
   * Datos del vehículo a tasar
   */
  datosVehiculo!: TasadorPPUModel | undefined;

  /**
   * Datos de la tasación
   */
  datosTasacion!: TasadorCotizacionModel;

  /**
   * Datos de las versiones
   */
  versiones!: TasadorIdNombreModel[];

  /**
   * Atributo que indica el paso en el que se encuentra la tasación
   */
  pasoTasacion = 'formulario';

  /**
   * Valores del select de Kilometraje
   */
  valoresSelectKilometraje: TasadorIdValorModel<number, string>[];

  /**
   * Bandera para mostrar mensaje patente no encontrada
   */
  patenteSinDatos!: boolean;

  /**
   * Objeto con la oferta realizada.
   */
  @Input() oferta?: ValorTasacionModel;

  /**
   * Indica si es flujo de venta
   */
  @Input() esVenta!: boolean;

  /**
   * referencia al modal
   */
  bsModalRef!: BsModalRef;

  /**
   * Arreglo con listado de marcas
   */
  marcas!: TasadorIdNombreModel[];

  /**
   * Arreglo con listado de modelos
   */
  modelos!: ModeloTasadorModel[];

  /**
   * Arreglo con listado de años
   */
  anios!: TasadorIdNombreModel[];

  /**
   * Arreglo con horas disponibles horario AM
   */
  horasDisponiblesAM!: HoraModel[];

  /**
   * Arreglo con horas disponibles horario PM
   */
  horasDisponiblesPM!: HoraModel[];

  /**
   * Indica el día actual o el seleccionado en el calendario
   */
  dayValue!: Date;

  /**
   * url pdf Términos y Condiciones
   */
  urlPdfTerminosCondiciones!: string;

  /**
   * Indica si el año del vehículo es valido para la tasación
   */
  anioValido: boolean;

  /**
   * Indica si el modelo del vehículo tiene años validos para la tasación
   */
  aniosValidos: boolean;

  /**
   * Indica si es flujo alternativo SIN tasación
   */
  esFlujoAlternativo: boolean;

  /**
   * Indica si el año del vehículo tiene versiones validos para la tasación
   */
  versionesValidas!: boolean;

  /**
   * Indica el nombre de la marca para flujo alternativo
   */
  marcaFlujoAlternativo: string;

  /**
   * Indica el nombre del modelo para flujo alternativo
   */
  modeloFlujoAlternativo: string;

  /**
   * Indica el año del vehículo para flujo alternativo
   */
  anioFlujoAlternativo: number | undefined;

  /**
   * Indica el nombre de la versión para flujo alternativo
   */
  versionFlujoAlternativo: string;

  /**
   * Indica el kilometraje del vehículo para flujo alternativo
   */
  kilometrajeFlujoAlternativo: number | undefined;

  /**
   * Indica que no están aceptados los términos y condiciones
   */
  noCheckTerminosCondiciones!: boolean;

  /**
   * Indica que el botón Buscar está deshabilitado
   */
  disableBuscar: boolean;

  /**
   * Flag que valida si el flujo actual es el de ficha vehiculo.
   */
  checkIfFichaVehiculo: boolean;

  /**
   * objeto que guarda datos de modelo de registro de datos
   * y creacion de folio.
   */
  detailedDataSaved!: GuardarDetalleRequestModel;

  /**
   * Flag que indica si fue seleccionado el check de terminos y condiciones.
   */
  checkTerminos!: boolean;

  /**
   * Fecha y hora en que se selecciono el check de terminos y condiciones.
   */
  terminosSelectedDateTime!: string;

  /**
   * Numero de folio que se genera a iniciar el flujo
   */
  nroFolio!: string;

  /**
   * nombre de marca seleccionada
   */
  marcaName!: string;

  /**
   * nombre de modelo seleccionada
   */
  modeloName!: string;

  /**
   * prefijo campo telefono
   */
  telefonoPrefijo: string;

  esErrorEmail: boolean = false;

  idDeal: string = '';
  
  data: any;

  constructor(
    private tasadorService: TasadorService,
    private loadingService: LoadingService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private agendaService: AgendaService,
    private pdfService: PdfService,
    private recaptcha3: NgRecaptcha3Service,
    private router: Router,
    private registroDetalleService: CompraVehiculoFlujoService,
    private datePipe: DatePipe,
    private store: Store<AppState>,
    private hubspotService : HubspotService
  ) {
    this.inicializaPatenteForm();
    this.inicializaTasadorForm();
    this.inicializaDatosPersonalesForm();
    this.inicializaAgendamientoForm();
    this.valoresSelectKilometraje = [];
    this.versiones = [];
    this.datosTasacion = TasadorCotizacionMethods.emptyTasadorCotizacionModel();
    this.dayValue = new Date();
    this.obtenerUrlPdf();
    this.anioValido = true;
    this.aniosValidos = true;
    this.esFlujoAlternativo = false;
    this.versionesValidas = true;
    this.marcaFlujoAlternativo = '';
    this.modeloFlujoAlternativo = '';
    this.anioFlujoAlternativo = undefined;
    this.versionFlujoAlternativo = '';
    this.kilometrajeFlujoAlternativo = undefined;
    this.disableBuscar = false;
    this.checkIfFichaVehiculo = false;
    this.esVenta = true;
    this.telefonoPrefijo = '+569';
  }

  get pasoActual(): number {
    switch (this.pasoTasacion) {
      case 'datosPersonales':
      case 'agendamiento':
        return 2;
      case 'exitoSolicitud':
      case 'resumenInspeccion':
        return 3;
      default:
        return 0;
    }
  }

  ngOnInit(): void {
    if (!!this.oferta && Object.entries(this.oferta).length !== 0) {
      this.completarDatosFormulario();
    }
    if (this.esVenta) {
      this.obtenerAnios();
      this.cargaSelectKilometraje();
      this.cargarFeriados();
      this.agregaValidator();
    } else {
      this.telefonoPrefijo = '';
    }
    if (this.router.url.includes('ficha-vehiculo-iframe')) {
      this.checkIfFichaVehiculo = true;
      this.cleanFormOnInit();
    }
    this.recaptcha3.init(environment.siteKeyCaptcha);
  }

  ngOnDestroy(): void {
    if (!this.checkIfFichaVehiculo) {
      this.recaptcha3.destroy();
    }
  }

  /**
   * Método encargado de agregar validacion de telefono en caso de ser Venta
   */
  agregaValidator(): void {
    if (this.esVenta) {
      this.tasadorForm.controls.telefono.setValue(this.telefonoPrefijo);
      this.tasadorForm.controls.telefono.setValidators([
        Validators.required,
        Validators.pattern('^([+])([0-9]+$)'),
        Validators.minLength(12),
      ]);
      this.tasadorForm.controls.telefono.updateValueAndValidity();
    }
  }

  /**
   * Método encargado de cambiar el paso seleccionado en el flujo de tasación
   * @param paso Paso seleccionado
   */
  pasoSeleccionado(paso: string): void {
    this.pasoTasacion = paso;
  }

  /**
   * Método encargado de volver al paso de formulario
   */
  volverATasar($event: string): void {
    this.loadingService.show();
    this.recaptcha3.getToken().then((token: string) => {
      this.detailedDataSaved.captcha = token;
      switch ($event) {
        case 'resultado':
          this.detailedDataSaved.pantallaCierre = $event;
          this.detailedDataSaved.clickBotonVolverATasar = true;
          break;
        case 'datosPersonales':
          this.detailedDataSaved.pantallaCierre = $event;
          this.detailedDataSaved.clickBotonVolverATasarDatosCliente = true;
          break;
        case 'exitoSolicitud':
          this.detailedDataSaved.pantallaCierre = $event;
          this.detailedDataSaved.exitoBotonConfirmar = true;
          this.detailedDataSaved.clickBotonVolverATasarFinal = true;
          break;
        default:
          break;
      }
      this.registroDetalleService
        .registerDetailedData(this.detailedDataSaved)
        .subscribe((response: GuardarDetalleResponse) => {
          if (response.codigo === '200') {
            this.loadingService.hide();
            this.limpiarForm();
            this.pasoTasacion = 'formulario';
          }
        });
    });
  }

  /**
   * Metodo que finaliza el proceso exitoso
   */
  finalizeSuccessfullProcess(): void {
    this.loadingService.show(true);

    this.detailedDataSaved.exitoBotonConfirmar = true;
    
    this.recaptcha3.getToken().then((token: string) => {
      this.detailedDataSaved.captcha = token;
      this.registroDetalleService
        .registerDetailedData(this.detailedDataSaved)
        .subscribe((response: GuardarDetalleResponse) => {
          if (response.codigo === '200') {
            this.loadingService.hide();
            this.router.navigate(['/']);
          }
        });
    });
  }

  /**
   * Metodo que crea el objeto a mandar para llamar servicio de registro
   * de datos.
   */
  createDetailedDataToSaveFromTasacion(): void {
    this.recaptcha3.getToken().then((token: string) => {
      const marca = this.datosTasacion.marca
        ? this.datosTasacion.marca
        : '';
      const modelo = this.datosTasacion.modelo
        ? this.datosTasacion.modelo
        : '';
      const version = this.datosTasacion.version
        ? this.datosTasacion.version
        : '';
      const fechaCotizacion = new Date();
      this.detailedDataSaved = new GuardarDetalleRequestModel();
      this.detailedDataSaved.folio = this.nroFolio;
      this.detailedDataSaved.fechaCotizacion = String(
        this.datePipe.transform(fechaCotizacion, 'dd-MM-yyyy HH:mm:ss')
      );
      this.detailedDataSaved.patenteTasacion =
        this.patenteForm.controls.patente.value || '';
      // this.detailedDataSaved.anioReglaExcepcion =
      //   this.datosVehiculo?.anioReglaExcepcion ?? '';
      this.detailedDataSaved.marca = marca;
      this.detailedDataSaved.modelo = modelo;
      this.detailedDataSaved.anio = this.tasadorForm.controls.anio.value;
      this.detailedDataSaved.kilometraje =
        this.tasadorForm.controls.kilometraje.value;
      this.detailedDataSaved.version = version;
      this.detailedDataSaved.emailTasacion =
        this.tasadorForm.controls.email.value;
      this.detailedDataSaved.telefonoTasacion =
        this.tasadorForm.controls.telefono.value;
      this.detailedDataSaved.aceptaTerminosYCondiciones = this.checkTerminos;
      this.detailedDataSaved.fechaHoraTerminosYCondiciones = String(
        this.datePipe.transform(
          this.terminosSelectedDateTime,
          'dd-MM-yyyy HH:mm:ss'
        )
      );
      this.detailedDataSaved.precioRetoma =
        this.datosTasacion.valorsugerido ?? 0;
      this.detailedDataSaved.precioMercado =
        this.datosTasacion.valorsugerido ?? 0;
      this.detailedDataSaved.precioOferta =
        this.datosTasacion?.valor ?? 0;
      //valores se dejan a su defecto ya que el nuevo tasador no lo obtiene
      this.detailedDataSaved.factor = '';
      this.detailedDataSaved.categoria = '';
      this.detailedDataSaved.fechaInicioRequestTasacion =
        this.datosTasacion.fecha || '';
      this.detailedDataSaved.fechaFinRequestTasacion =
        this.datosTasacion.fecha || '';
      this.detailedDataSaved.clickBotonTasarAuto = true;
      this.detailedDataSaved.captcha = token;
      this.detailedDataSaved.pantallaCierre = '';
      this.detailedDataSaved.generaFolio = false;
      this.detailedDataSaved.estado = 'activo';
    });
  }

  /**
   * Método que carga los valores del select de kilometraje
   */
  cargaSelectKilometraje(): void {
    this.loadingService.show(true);
    this.tasadorService.kilometrajeSeleccionable().subscribe((response) => {
      this.loadingService.hide();
      this.valoresSelectKilometraje = response;
    });
  }

  /**
   * Método encargado de buscar los datos de patentes
   */
  buscarPatente(): void {
    this.patenteSinDatos = false;
    this.anioValido = true;
    this.aniosValidos = true;
    this.versionesValidas = true;
    if (this.patenteForm.valid) {
      this.marcas = [];
      this.modelos = [];
      this.anios = [];
      this.loadingService.show(true);
      this.tasadorService
        .datosPorPatente(this.patenteForm.controls.patente.value)
        .subscribe((response) => {
          this.tasadorForm.controls.marca.disable();
          this.tasadorForm.controls.modelo.disable();
          this.tasadorForm.controls.anio.disable();
          this.isSubmitted = true;
          this.disableBuscar = true;
          this.loadingService.hide();
          if (response != null && response.codigo === '200') {
            if (this.checkIfFichaVehiculo) {
              this.cargaSelectKilometraje();
            }
            this.patenteForm.controls.patente.disable();
            this.datosVehiculo = response.elemento;
            this.marcas = [
              {
                id: this.datosVehiculo.marcaId,
                nombre: this.datosVehiculo.marca,
              },
            ];
            this.modelos = [
              {
                id: this.datosVehiculo.modeloId,
                nombre: this.datosVehiculo.modelo,
              },
            ];
            this.anios = [{
              id: this.datosVehiculo.anio,
              nombre: this.datosVehiculo.anio.toString()
            }];
            this.versiones = this.datosVehiculo.versiones;
            this.tasadorForm.controls.version.enable();
            this.tasadorForm.controls.kilometraje.enable();
            this.tasadorForm.controls.marca.setValue(
              this.datosVehiculo.marcaId
            );
            this.tasadorForm.controls.modelo.setValue(
              this.datosVehiculo.modeloId
            );
            if(this.versiones.length == 1) {
              this.tasadorForm.controls.version.disable();
              this.tasadorForm.controls.version.setValue(
                this.datosVehiculo.versiones[0].id.toString()
              );
            } else {
              this.tasadorForm.controls.version.setValue('');
            }
            this.tasadorForm.controls.anio.setValue(this.datosVehiculo.anio);
            this.tasadorForm.controls.kilometraje.setValue('');
            this.anioValido = response.elemento.anioId > 0;
            this.versionesValidas = this.datosVehiculo.versiones.length > 0;
          } else {
            this.patenteSinDatos = true;
            this.isSubmitted = false;
            this.tasadorForm.reset();
            this.tasadorForm.controls.version.disable();
            this.tasadorForm.controls.kilometraje.disable();
            console.log("no se encontró patente");
            this.loadingService.hide();
            this.disableBuscar = false;
          }
        });
    }
  }

  /**
   * Método encargado de limpiar el formulario de tasación
   */
  limpiarForm(): void {
    this.patenteForm.controls.patente.enable();
    this.patenteForm.reset();
    this.tasadorForm.reset();
    this.isSubmitted = false;
    this.isSubmittedTasacion = false;
    this.tasadorForm.controls.version.disable();
    this.tasadorForm.controls.kilometraje.disable();
    this.patenteSinDatos = false;
    if (this.esVenta) {
      this.tasadorForm.controls.marca.disable();
      this.tasadorForm.controls.modelo.disable();
      this.obtenerAnios();
    }
    this.anioValido = true;
    this.aniosValidos = true;
    this.datosTasacion = TasadorCotizacionMethods.emptyTasadorCotizacionModel();
    this.datosPersonalesForm.reset();
    this.esFlujoAlternativo = false;
    this.versionesValidas = true;
    this.marcaFlujoAlternativo = '';
    this.modeloFlujoAlternativo = '';
    this.anioFlujoAlternativo = undefined;
    this.versionFlujoAlternativo = '';
    this.kilometrajeFlujoAlternativo = undefined;
    this.datosVehiculo = undefined;
    this.disableBuscar = false;
    this.tasadorForm.controls.telefono.setValue(this.telefonoPrefijo);
  }

  /**
   * Método encargado de realizar la tasación en base a los datos del vehículo.
   */
  realizaTasacion(): void {
    this.isSubmittedTasacion = true;
    this.obtainIdFolio();
    if (this.tasadorForm.valid) {
      this.loadingService.show(true);
      this.recaptcha3.getToken().then(
        (token: string) => {
          if(!this.esVenta) {
            this.valorTasacion(token);
            return;
          }
          this.registraContactoHubspot().subscribe(
            responseContacto => { 
              if(responseContacto == 'INVALID_EMAIL')
              {
                this.esErrorEmail = true;
              }
              else
              {
                this.esErrorEmail = false;
                this.valorTasacion(token, responseContacto);
              }
            },
            error => { 
              console.log(error); 
              this.loadingService.hide();
            });
        },
        (error: any) => {
          // handle error here
          this.loadingService.hide();
          console.log(error);
        }
      );
    }
  }

  valorTasacion(token: string, responseContacto: any = null): void {
    this.tasadorForm.controls.captcha.setValue(token);
    this.tasadorService
      .obtenerValorTasacion(
        this.tasadorForm.controls.version.value,
        this.tasadorForm.controls.kilometraje.value,
        this.tasadorForm.controls.captcha.value,
        this.tasadorForm.controls.telefono.value != '' ?
          this.tasadorForm.controls.telefono.value : undefined,
        this.tasadorForm.controls.email.value != '' ?
          this.tasadorForm.controls.email.value : undefined
      )
      .subscribe((response) => {
        if (response != null && response.codigo === '200') {
          this.datosTasacion = response.elemento;
          if (this.datosVehiculo) {
            this.datosTasacion.marca = this.datosVehiculo.marca;
            this.datosTasacion.modelo = this.datosVehiculo.modelo;
            this.datosTasacion.ano = this.datosVehiculo.anio;
          } else {
            this.datosTasacion.marca = this.marcaFlujoAlternativo;
            this.datosTasacion.modelo = this.modeloFlujoAlternativo;
            this.datosTasacion.ano = this.anioFlujoAlternativo;
          }
          if (!this.esVenta) {
            this.siguientePasoTasacion();
          }
          else {
            this.registraNegocioHubspot(responseContacto).subscribe(negocio => {
              this.idDeal = negocio;
              this.siguientePasoTasacion();
            });
          }
        } else {
          this.loadingService.hide();
          console.log(
            '[TasadorComponent][realizaTasacion] Error al realizar la tasación',
            response
          );
          const initialState = {
            isSuccess: false,
            emailCliente: '',
          };
          this.bsModalRef = this.modalService.show(
            RespuestaEnvioCorreoComponent,
            {
              initialState,
              backdrop: 'static',
              keyboard: false,
              class: 'modal-lg',
            }
          );
          throw new Error(
            '[TasadorComponent][realizaTasacion] Error al realizar la tasación'
          );
        }
        if (this.esVenta) {
          window.scrollTo(0, 0);
        }
      }, error => {
        this.loadingService.hide();
        console.log(error);
      });
  }
  siguientePasoTasacion(): void {
    //this.datosTasacion.version = this.versionFlujoAlternativo;
    this.datosTasacion.kilometraje =
    this.kilometrajeFlujoAlternativo;
    this.handleDataIfFichaVehiculo();
    //console.log('[realizaTasacion][this.datosTasacion]', response);
    this.createDetailedDataToSaveFromTasacion();
    this.registerOnFirstAppraisal();
  }

  /**
   * Metodo que maneja los datos a setear si es flujo de ficha de vehiculo
   */
  handleDataIfFichaVehiculo(): void {
    if (this.checkIfFichaVehiculo) {
      this.datosTasacion.versionid = this.tasadorForm.controls.version.value;
      this.datosTasacion.kilometraje =
        this.tasadorForm.controls.kilometraje.value;
      this.onChangeVersion();
      this.datosTasacion.version = this.versionFlujoAlternativo;
    }
  }

  /**
   * Metodo que hace registro de datos en la primera tasacion.
   */
  registerOnFirstAppraisal(): void {
    this.recaptcha3.getToken().then((token: string) => {
      this.detailedDataSaved.captcha = token;
      this.registroDetalleService
        .registerDetailedData(this.detailedDataSaved)
        .subscribe((response: GuardarDetalleResponse) => {
          if (response.codigo === '200') {
            this.pasoTasacion = 'datosPersonales';
            this.loadingService.hide();
            // this.router.events.subscribe(evt => {
            //   if(evt instanceof NavigationEnd && !!evt.url){
            //     if(typeof ga == "function"){
            //       ga('set', 'page', ''+evt.url);
            //       ga('send', 'auto-recibe-oferta');
            //     }
            //   }
            // });
          } else {
            this.pasoTasacion = 'formulario';
            this.loadingService.hide();
          }
        },
        error => {
          console.log("error registerDetailedData", error);
        });
    });
  }

  /**
   * Método encargado de enviar la oferta del tasador al servicio de oferta
   */
  aceptaOferta(): void {
    this.datosTasacion.patente = this.patenteForm.controls.patente.value;
    console.log(
      '[TasadorComponent][aceptaOferta] Se acepta la oferta: ',
      this.datosTasacion
    );
    this.recaptcha3.getToken().then((token: string) => {
      this.detailedDataSaved.captcha = token;
      this.detailedDataSaved.clickBotonAgendar = true;
      this.detailedDataSaved.generaFolio = false;
      this.detailedDataSaved.patenteTasacion = this.datosTasacion.patente || '';
      this.registroDetalleService
        .registerDetailedData(this.detailedDataSaved)
        .subscribe((response: GuardarDetalleResponse) => {
          if (response.codigo === '200') {
            this.loadingService.hide();
            this.tasadorService.enviarOferta(this.datosTasacion);
          } else {
            this.loadingService.hide();
            this.pasoTasacion = 'formulario';
          }
        });
    });
  }

  /**
   * Método encargado de llenar el formulario de tasación con la oferta previa realizada
   */
  completarDatosFormulario(): void {
    if (!!this.oferta?.patente) {
      this.cargaSelectKilometraje();
      this.marcas = [];
      this.modelos = [];
      this.anios = [];
      this.loadingService.show(true);
      console.log('[buscarPatente]', this.oferta.patente);
      this.tasadorService
        .datosPorPatente(this.oferta.patente)
        .subscribe((response) => {
          this.loadingService.hide();
          this.isSubmitted = true;
          if (response != null && response.codigo === '200') {
            this.datosVehiculo = response.elemento;
            this.marcas = [
              {
                id: this.datosVehiculo.marcaId,
                nombre: this.datosVehiculo.marca,
              },
            ];
            this.modelos = [
              {
                id: this.datosVehiculo.modeloId,
                nombre: this.datosVehiculo.modelo,
              },
            ];
            this.anios = [{
              id: this.datosVehiculo.anio,
              nombre: this.datosVehiculo.anio.toString()
            }];
            this.versiones = this.datosVehiculo.versiones;
            this.patenteForm.controls.patente.setValue(this.oferta?.patente);
            this.tasadorForm.controls.marca.setValue(this.oferta?.brandId);
            this.tasadorForm.controls.modelo.setValue(
              this.datosVehiculo?.modeloId
            );
            this.tasadorForm.controls.version.setValue(this.oferta?.versionId);
            this.tasadorForm.controls.anio.setValue(this.oferta?.year);
            this.tasadorForm.controls.kilometraje.setValue(
              this.oferta?.requestedKm
            );
            this.tasadorForm.controls.version.enable();
            this.tasadorForm.controls.kilometraje.enable();
          } else {
            this.patenteSinDatos = true;
            this.isSubmitted = false;
            console.log("no se encontró patente");
            this.loadingService.hide();
            this.disableBuscar = false;
          }
        });
    }
  }

  /**
   * Método encargado de envíar al formulario de datos personales
   * @param esFlujoAlternativo Indica si es flujo alternativo SIN tasación
   */
  pasoDatosPersonales(esFlujoAlternativo?: boolean): void {
    window.scrollTo(0, 0);
    if (!!this.patenteForm.controls.patente.value) {
      this.datosPersonalesForm.controls.patente.setValue(
        this.patenteForm.controls.patente.value
      );
    }
    if (this.tasadorForm && !!this.tasadorForm.controls.email.value) {
      this.datosPersonalesForm.controls.email.setValue(
        this.tasadorForm.controls.email.value
      );
    }
    if (this.tasadorForm && !!this.tasadorForm.controls.telefono.value) {
      this.datosPersonalesForm.controls.telefono.setValue(
        this.tasadorForm.controls.telefono.value
      );
    }
    if (esFlujoAlternativo) {
      this.esFlujoAlternativo = esFlujoAlternativo;
      this.obtainIdFolio();
      this.saveAlternativeCase();
    }
    this.verifyStep();
    this.loadingService.show(true);

    this.recaptcha3.getToken().then((token: string) => {
      this.detailedDataSaved.captcha = token;
      this.detailedDataSaved.generaFolio = false;
      this.registroDetalleService
        .registerDetailedData(this.detailedDataSaved)
        .subscribe((response: GuardarDetalleResponse) => {
          if (response.codigo === '200') {
            this.loadingService.hide();
            this.pasoTasacion = 'datosPersonales';
            // this.router.navigate(['/auto-recibe-oferta'])
            // this.router.events.subscribe(evt => {
            //   if(evt instanceof NavigationEnd && !!evt.url){
            //     if(typeof ga == "function"){
            //       ga('set', 'page', ''+evt.url);
            //       ga('send', 'auto-recibe-oferta');
            //     }
            //   }
            // });
            // this.router.navigate(['auto-recibe-oferta'])
          } else {
            this.loadingService.hide();
            this.pasoTasacion = 'formulario';
          }
        });
    });
    //this.router.navigate(['auto-recibe-oferta'])
  }

  /**
   * Metodo que verifica si es flujo alternativo para
   * Realizar proceso de guardado.
   */
  saveAlternativeCase(): void {
    this.obtainIdFolio();
    this.loadingService.show(true);
    this.createDetailedDataToSaveFromTasacion();

    this.recaptcha3.getToken().then((token: string) => {
      this.detailedDataSaved.clickBotonAgendar = true;
      this.getBrandAndModelName();
      this.detailedDataSaved.marca = this.marcaName;
      this.detailedDataSaved.modelo = this.modeloName;
      this.detailedDataSaved.captcha = token;
      this.registroDetalleService
        .registerDetailedData(this.detailedDataSaved)
        .subscribe((response: GuardarDetalleResponse) => {
          if (response.codigo === '200') {
            this.loadingService.hide();
            this.pasoTasacion = 'datosPersonales';
            // this.router.events.subscribe(evt => {
            //   if(evt instanceof NavigationEnd && !!evt.url){
            //     if(typeof ga == "function"){
            //       ga('set', 'page', ''+evt.url);
            //       ga('send', 'auto-recibe-oferta');
            //     }
            //   }
            // });
          } else {
            this.loadingService.hide();
            this.pasoTasacion = 'formulario';
            console.log(
              '[TasadorComponent][registerDetailedData] Error al avanzar en flujo alternativo',
              response
            );
            const initialState = {
              isSuccess: false,
              emailCliente: '',
            };
            this.bsModalRef = this.modalService.show(
              RespuestaEnvioCorreoComponent,
              {
                initialState,
                backdrop: 'static',
                keyboard: false,
                class: 'modal-lg',
              }
            );
            throw new Error(
              '[TasadorComponent][realizaTasacion] Error al avanzar en flujo alternativo'
            );
          }
        });
    });
  }

  /**
   * Metodo que guarda el nombre de marca y modelos cuando es
   * flujo de excepcion.
   */
  getBrandAndModelName(): void {
    this.marcas.forEach((resp: MarcaTasadorModel) => {
      if (resp.id === Number(this.tasadorForm.controls.marca.value)) {
        this.marcaName = resp.nombre;
      }
    });

    this.modelos.forEach((resp: MarcaTasadorModel) => {
      if (resp.id === Number(this.tasadorForm.controls.modelo.value)) {
        this.modeloName = resp.nombre;
      }
    });
  }

  /**
   * Metodo que verifica el paso en que se encuentra el flujo.
   */
  verifyStep(): void {
    if (this.pasoTasacion === 'agendamiento') {
      this.detailedDataSaved.clickBotonVolverAgenda = true;
    } else {
      this.detailedDataSaved.clickBotonAgendar = true;
    }
  }

  /**
   * Método encargado de enviar al paso de agendamiento
   */
  pasoAgendamiento(): void {
    this.loadingService.show(true);
    if (this.esFlujoAlternativo) {
      if (this.datosVehiculo) {
        this.datosTasacion.marca = this.datosVehiculo.marca;
        this.datosTasacion.modelo = this.datosVehiculo.modelo;
        this.datosTasacion.ano = this.datosVehiculo.anio;
      } else {
        this.datosTasacion.marca = this.marcaFlujoAlternativo;
        this.datosTasacion.modelo = this.modeloFlujoAlternativo;
        this.datosTasacion.ano = this.anioFlujoAlternativo;
      }
      this.datosTasacion.version = this.versionFlujoAlternativo;
      this.datosTasacion.kilometraje = this.kilometrajeFlujoAlternativo;
    }
    this.agendamientoForm.reset();
    window.scrollTo(0, 0);

    this.recaptcha3.getToken().then((token: string) => {
      this.detailedDataSaved.clickBotonSiguiente = true;
      this.detailedDataSaved.nombre =
        this.datosPersonalesForm.controls.nombre.value;
      this.detailedDataSaved.apellido =
        this.datosPersonalesForm.controls.apellidos.value;
      this.detailedDataSaved.emailDatosCliente =
        this.datosPersonalesForm.controls.email.value;
      this.detailedDataSaved.telefono =
        this.datosPersonalesForm.controls.telefono.value;
      this.detailedDataSaved.patenteDatosCliente =
        this.datosPersonalesForm.controls.patente.value;
      this.detailedDataSaved.pantallaCierre = '';
      this.detailedDataSaved.estado = 'activo';
      this.detailedDataSaved.captcha = token;
      this.registroDetalleService
        .registerDetailedData(this.detailedDataSaved)
        .subscribe((response: GuardarDetalleResponse) => {
          if (response.codigo === '200') {
            this.pasoTasacion = 'agendamiento';
            // this.router.events.subscribe(evt => {
            //   if(evt instanceof NavigationEnd && !!evt.url){
            //     if(typeof ga == "function"){
            //       ga('set', 'page', ''+evt.url);
            //       ga('send', 'auto-agenda-inspeccion');
            //     }
            //   }
            // });
            this.loadingService.hide();
          } else {
            this.loadingService.hide();
            this.pasoTasacion = 'formulario';
          }
        });
    });
  }

  /**
   * Método encargado de enviar al paso de resumen de agendamiento
   */
  pasoResumen(): void {
    window.scrollTo(0, 0);
    this.pasoTasacion = 'resumenInspeccion';
  }

  /**
   * Método encargado traer las marcas desde el servicio y poblar el selector del formulario
   */
  cargarMarcas(): void {
    this.marcas = [];
    this.loadingService.show(true);
    this.tasadorService
      .obtenerMarcas(this.tasadorForm.controls.anio.value)
      .subscribe((response) => {
        this.loadingService.hide();
        if (response != null && response.codigo === '200') {
          this.marcas = response.elemento;
        } else {
          console.log(
            '[TasadorComponent][cargarMarcas] Error al realizar la cargar de marcas',
            response
          );
          const initialState = {
            isSuccess: false,
            emailCliente: '',
          };
          this.bsModalRef = this.modalService.show(
            RespuestaEnvioCorreoComponent,
            {
              initialState,
              backdrop: 'static',
              keyboard: false,
              class: 'modal-lg',
            }
          );
          throw new Error(
            '[TasadorComponent][cargarMarcas] Error al realizar la cargar de marcas'
          );
        }
      });
  }

  /**
   * Método encargado traer los modelos correspondientes a la marca seleccionada
   */
  onChangeMarca(): void {
    const nombreMarca = this.marcas.find(
      (marca) => marca.id === Number(this.tasadorForm.controls.marca.value)
    );
    this.marcaFlujoAlternativo = nombreMarca ? nombreMarca?.nombre : '';
    this.versiones = [];
    this.tasadorForm.controls.modelo.disable();
    this.tasadorForm.controls.version.disable();
    this.tasadorForm.controls.kilometraje.disable();
    this.tasadorForm.controls.version.reset();
    this.tasadorForm.controls.kilometraje.reset();
    this.anioValido = true;
    this.aniosValidos = true;
    this.versionesValidas = true;
    this.loadingService.show(true);
    this.tasadorService
      .obtenerModelos(
        this.tasadorForm.controls.marca.value
      )
      .subscribe((response) => {
        this.loadingService.hide();
        if (response != null && response.codigo === '200') {
          this.modelos = response.elemento;
          this.tasadorForm.controls.modelo.enable();
          this.tasadorForm.controls.modelo.setValue('');
        } else {
          console.log(
            '[TasadorComponent][cargarmodelos] Error al realizar la cargar de modelos',
            response
          );
          const initialState = {
            isSuccess: false,
            emailCliente: '',
          };
          this.bsModalRef = this.modalService.show(
            RespuestaEnvioCorreoComponent,
            {
              initialState,
              backdrop: 'static',
              keyboard: false,
              class: 'modal-lg',
            }
          );
          throw new Error(
            '[TasadorComponent][cargarmodelos] Error al realizar la cargar de modelos'
          );
        }
      });
  }

  /**
   * Método encargado traer los años correspondientes al modelo/marca seleccionado
   */
  onChangeModelo(): void {
    const nombreModelo = this.modelos.find(
      (modelo) => modelo.id === Number(this.tasadorForm.controls.modelo.value)
    );
    this.modeloFlujoAlternativo = nombreModelo ? nombreModelo?.nombre : '';
    this.tasadorForm.controls.version.disable();
    this.tasadorForm.controls.kilometraje.disable();
    this.tasadorForm.controls.version.reset();
    this.tasadorForm.controls.kilometraje.reset();
    this.anioValido = true;
    this.aniosValidos = true;
    this.versionesValidas = true;
    this.loadingService.show(true);
    this.tasadorService
      .obtenerVersiones(
        this.tasadorForm.controls.modelo.value
      )
      .subscribe((response) => {
        this.loadingService.hide();
        if (response != null && response.codigo === '200') {
          this.versiones = response.elemento;
          this.tasadorForm.controls.version.enable();
          this.tasadorForm.controls.kilometraje.enable();
          this.tasadorForm.controls.version.setValue('');
          this.tasadorForm.controls.kilometraje.setValue('');
          this.versionesValidas = this.versiones.length > 0;
        } else {
          console.log(
            '[TasadorComponent][cargarmodelos] Error al realizar la cargar de versiones',
            response
          );
          const initialState = {
            isSuccess: false,
            emailCliente: '',
          };
          this.bsModalRef = this.modalService.show(
            RespuestaEnvioCorreoComponent,
            {
              initialState,
              backdrop: 'static',
              keyboard: false,
              class: 'modal-lg',
            }
          );
          throw new Error(
            '[TasadorComponent][cargarmodelos] Error al realizar la cargar de versiones'
          );
        }
      });
  }

  obtenerAnios(): void {
    this.tasadorService.obtenerAnios().subscribe((response) => {
      this.loadingService.hide();
      if (response != null && response.codigo === '200') {
        this.anios = response.elemento;
        this.tasadorForm.controls.anio.enable();
        this.tasadorForm.controls.anio.setValue('');
        this.tasadorForm.controls.version.reset();
        this.tasadorForm.controls.marca.reset();
        this.tasadorForm.controls.modelo.reset();
        this.tasadorForm.controls.kilometraje.reset();
        this.aniosValidos = this.anios.length > 0;
      } else {
        console.log(
          '[TasadorComponent][cargaraños] Error al realizar la cargar de años',
          response
        );
        const initialState = {
          isSuccess: false,
          emailCliente: '',
        };
        this.bsModalRef = this.modalService.show(
          RespuestaEnvioCorreoComponent,
          {
            initialState,
            backdrop: 'static',
            keyboard: false,
            class: 'modal-lg',
          }
        );
        throw new Error(
          '[TasadorComponent][cargaraños] Error al realizar la cargar de años'
        );
      }
    });
  }

  /**
   * Metodo que limpia formulario al cargar la tasacion en ficha vehiculo
   */
  cleanFormOnInit(): void {
    setTimeout(() => {
      this.tasadorForm.controls.anio.reset();
      this.tasadorForm.controls.version.reset();
      this.tasadorForm.controls.marca.reset();
      this.tasadorForm.controls.modelo.reset();
      this.tasadorForm.controls.kilometraje.reset();
    }, 0);
  }

  /**
   * Método encargado traer las versiones correspondientes al año seleccionado
   */
  onChangeAnio(): void {
    this.anioFlujoAlternativo = Number(this.tasadorForm.controls.anio.value);
    this.tasadorForm.controls.version.disable();
    this.tasadorForm.controls.kilometraje.disable();
    this.tasadorForm.controls.version.reset();
    this.tasadorForm.controls.kilometraje.reset();
    this.anioValido = true;
    this.aniosValidos = true;
    this.versionesValidas = true;
    this.noCheckTerminosCondiciones = false;
    this.cargarMarcas();
    this.tasadorForm.controls.marca.enable();
    this.tasadorForm.controls.marca.setValue('');
    this.tasadorForm.controls.modelo.reset();
    this.tasadorForm.controls.modelo.disable();
  }

  /**
   * Método encargado de setear el nombre de la versión seleccionada
   */
  onChangeVersion(): void {
    const nombreVersion = this.versiones.find(
      (version) =>
        version.id === Number(this.tasadorForm.controls.version.value)
    );
    this.versionFlujoAlternativo = nombreVersion ? nombreVersion?.nombre : '';
  }

  /**
   * Método encargado de setear el kilometraje seleccionado
   */
  onChangeKilometraje(): void {
    this.kilometrajeFlujoAlternativo = Number(
      this.tasadorForm.controls.kilometraje.value
    );
  }

  /**
   * Método que inicializa el formulario de patente
   */
  inicializaPatenteForm(): void {
    this.patenteForm = this.formBuilder.group({
      patente: [
        '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z]{4}[0-9]{2}$'),
          ValidacionesCustom.validaPatenteAntigua,
        ],
      ],
    });
  }

  /**
   * Método que inicializa el formulario de tasación
   */
  inicializaTasadorForm(): void {
    this.tasadorForm = this.formBuilder.group({
      marca: [{ value: null, disabled: true }, Validators.required],
      modelo: [{ value: null, disabled: true }, Validators.required],
      version: [{ value: null, disabled: true }, Validators.required],
      anio: [{ value: null, disabled: false }, Validators.required],
      kilometraje: [{ value: null, disabled: true }, Validators.required],
      email: [
        '',
        [
          Validators.email,
          Validators.pattern(
            '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'
          ),
          Validators.minLength(2),
        ],
      ],
      telefono: [
        '',
        [Validators.pattern('^([+])([0-9]+$)'), Validators.minLength(12)],
      ],
      terminosCondiciones: false,
      captcha: '',
    });
    if (!this.esVenta) {
      this.tasadorForm.controls.anio.disable();
    }
  }

  /**
   * Método que inicializa el formulario de datos personales
   */
  inicializaDatosPersonalesForm(): void {
    this.datosPersonalesForm = this.formBuilder.group({
      patente: [
        '',
        [
          Validators.pattern('^[A-Za-z]{4}[0-9]{2}$'),
          ValidacionesCustom.validaPatenteAntigua,
        ],
      ],
      nombre: [
        '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$'),
          Validators.minLength(2),
        ],
      ],
      apellidos: [
        '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$'),
          Validators.minLength(2),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern(
            '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'
          ),
          Validators.minLength(2),
        ],
      ],
      telefono: [
        '',
        [
          Validators.required,
          Validators.pattern('^([+])([0-9]+$)'),
          Validators.minLength(12),
        ],
      ],
    });
  }

  /**
   * Método que inicializa el formulario de datos agendamiento
   */
  inicializaAgendamientoForm(): void {
    this.agendamientoForm = this.formBuilder.group({
      idProveedor: ['', Validators.required],
      nombreProveedor: ['', Validators.required],
      inicio: ['', Validators.required],
      fin: ['', Validators.required],
      inicioBloque: ['', Validators.required],
      fechaInspeccion: ['', Validators.required],
    });
  }

  /**
   * Método que obtiene las horas disponibles para la fecha actual
   * @param fecha fecha a consultar
   *
   */
  obtenerHorasDisponibles(fecha: string): void {
    this.agendaService.obtenerHorasDisponibles(fecha).subscribe((response) => {
      this.loadingService.hide();

      if (response != null && response.codigo === '200') {
        this.procesarHorarios(response.horas.horasDisponibles);
        this.pasoAgendamiento();
      } else {
        console.log(
          '[TasadorComponent][cargarfechasdisponibles] Error al realizar la carga de fechas disponibles',
          response
        );
        const initialState = {
          isSuccess: false,
          emailCliente: '',
        };
        this.bsModalRef = this.modalService.show(
          RespuestaEnvioCorreoComponent,
          {
            initialState,
            backdrop: 'static',
            keyboard: false,
            class: 'modal-lg',
          }
        );
        throw new Error(
          '[TasadorComponent][cargarfechasdisponibles] Error al realizar la carga de fechas disponibles'
        );
      }
    });
  }

  /**
   * Método que obtiene las horas disponibles para la fecha actual
   * @param horasDisponibles arreglo de horas disponibles
   */
  procesarHorarios(horasDisponibles: HoraModel[]): void {
    this.horasDisponiblesAM = [];
    this.horasDisponiblesPM = [];
    if (horasDisponibles.length > 0) {
      horasDisponibles.forEach((hora) => {
        const bloque = hora.inicioBloque.split(':')[0];
        if (Number(bloque) < 12) {
          this.horasDisponiblesAM.push(hora);
        } else {
          this.horasDisponiblesPM.push(hora);
        }
      });
    }
  }

  /**
   * Método para agendar y confirmar cita de inspección
   */
  confirmarCita(): void {
    this.recaptcha3.getToken().then(
      (token: string) => {
        const anio = this.datosTasacion.ano
          ? this.datosTasacion.ano?.toString()
          : '';
        const kilometraje = this.datosTasacion.kilometraje
          ? this.datosTasacion.kilometraje.toString()
          : '';
        const marca = this.datosTasacion.marca
          ? this.datosTasacion.marca
          : '';
        const modelo = this.datosTasacion.modelo
          ? this.datosTasacion.modelo
          : '';
        const valor = this.datosTasacion.valor
          ? this.datosTasacion.valor.toString()
          : '';
        const version = this.datosTasacion.version
          ? this.datosTasacion.version
          : '';
        const requestFormulario = new PeticionEnvioCita(
          this.agendamientoForm.controls.inicio.value,
          this.agendamientoForm.controls.fin.value,
          environment.idServicio,
          this.agendamientoForm.controls.idProveedor.value,
          this.datosPersonalesForm.controls.nombre.value,
          this.datosPersonalesForm.controls.apellidos.value,
          this.datosPersonalesForm.controls.email.value,
          this.datosPersonalesForm.controls.telefono.value,
          anio,
          kilometraje,
          marca,
          modelo,
          this.datosPersonalesForm.controls.patente.value,
          this.agendamientoForm.controls.nombreProveedor.value,
          valor,
          version,
          token,
          this.detailedDataSaved.folio,
          this.detailedDataSaved.fechaCotizacion
        );

        const fecha = user.fecha({
          content: this.agendamientoForm.controls.inicio.value,
        });
        this.store.dispatch(fecha);

        //console.log(this.agendamientoForm.controls.inicio.value);
        this.loadingService.show(true);
        this.agendaService.enviarFormularioCita(requestFormulario).subscribe(
          (response) => {
            window.scrollTo(0, 0);
            if (response != null && response.codigo === '200') {

              this.recaptcha3.getToken().then((token2: string) => {
                const horaAgenda = new Date(
                  this.agendamientoForm.controls.inicio.value
                );
                const horaAgendaFormatted = horaAgenda
                  .toISOString()
                  .split('T')[1]
                  .split('.')[0];
                this.detailedDataSaved.clickBotonConfirmar = true;
                this.detailedDataSaved.inspector =
                  this.agendamientoForm.controls.nombreProveedor.value;
                this.detailedDataSaved.fechaAgenda = String(
                  this.datePipe.transform(
                    this.agendamientoForm.controls.inicio.value,
                    'dd-MM-yyyy'
                  )
                );
                this.detailedDataSaved.horaAgenda = horaAgendaFormatted;
                this.detailedDataSaved.estado = 'activo';
                this.detailedDataSaved.pantallaCierre = '';
                this.detailedDataSaved.exitoBotonConfirmar = true;
                this.detailedDataSaved.exitoEnvioCorreo = true;
                this.detailedDataSaved.captcha = token2;
                this.registroDetalleService
                  .registerDetailedData(this.detailedDataSaved)
                  .subscribe((resp: GuardarDetalleResponse) => {
                    if (resp.codigo === '200') {
                      this.pasoTasacion = 'exitoSolicitud';
                      this.loadingService.hide();
                      // this.router.events.subscribe(evt => {
                      //   if(evt instanceof NavigationEnd && !!evt.url){
                      //     if(typeof ga == "function"){
                      //       ga('set', 'page', ''+evt.url);
                      //       ga('send', 'auto-confirmacion-inspeccion');

                      //     }
                      //   }
                      // });


                    } else {
                      this.pasoTasacion = 'errorSolicitud';
                      this.loadingService.hide();
                    }
                  });
              });
            } else {
              console.log(
                '[TasadorComponent][confirmarCita] Error al realizar la confirmación de la cita de inspección',
                response
              );
              this.recaptcha3.getToken().then((tokenError: string) => {
                this.detailedDataSaved.clickBotonConfirmar = true;
                this.detailedDataSaved.exitoBotonConfirmar = false;
                this.detailedDataSaved.exitoEnvioCorreo = false;
                this.detailedDataSaved.captcha = tokenError;
                this.registroDetalleService
                  .registerDetailedData(this.detailedDataSaved)
                  .subscribe((resp: GuardarDetalleResponse) => {
                    if (resp.codigo === '200') {
                      this.pasoTasacion = 'errorSolicitud';
                      this.loadingService.hide();
                      throw new Error(
                        '[TasadorComponent][confirmarCita] Error al realizar la confirmación de la cita de inspección'
                      );
                    }
                  });
              });
            }
          },
          (err) => {
            this.loadingService.hide();
            console.log(
              '[TasadorComponent][confirmarCita] Error en el servicio',
              err
            );
            this.pasoTasacion = 'errorSolicitud';
            throw new Error(
              '[TasadorComponent][confirmarCita] Error en el servicio' + err
            );
          }
        );
      },
      (error: any) => {
        // handle error here
        console.log(error);
      }
    );
  }

  /**
   * Método encargado de obtener los días feriados
   */
  cargarFeriados(): void {
    this.loadingService.show(true);
    this.agendaService.obtenerDiasFeriados().subscribe((response) => {
      this.loadingService.hide();
      if (response != null && response.codigo === '200') {
        this.sumaDiaFeriado(response.feriados);
      } else {
        console.log(
          '[TasadorComponent][cargarFeriados] Error al realizar la cargar de días feriados',
          response
        );
        const initialState = {
          isSuccess: false,
          emailCliente: '',
        };
        this.bsModalRef = this.modalService.show(
          RespuestaEnvioCorreoComponent,
          {
            initialState,
            backdrop: 'static',
            keyboard: false,
            class: 'modal-lg',
          }
        );
        throw new Error(
          '[TasadorComponent][cargarFeriados] Error al realizar la cargar de días feriados'
        );
      }
    });
  }

  /**
   * Método encargado de setear el formato correcto para generar el día feriado
   */
  sumaDiaFeriado(feriados: string[]): void {
    const disabledDates: Date[] = [];
    feriados.forEach((dia) => {
      const diaDate = new Date(dia);
      const diaMasDate = diaDate.setDate(diaDate.getDate() + 1);
      const diaMas = new Date(diaMasDate);
      disabledDates.push(diaMas);
    });
    this.agendaService.almacenaFeriados(disabledDates);
  }

  /**
   * Método encargado de retornar la URL del pdf de términos y condiciones
   */
  obtenerUrlPdf(): void {
    this.pdfService.obtenerPdfTerminosCondiciones().subscribe((response) => {
      this.urlPdfTerminosCondiciones = response.url;
    });
  }

  /**
   * Metodo que obtiene booleano que indica si se selecciono el checbox de temrinos y condiciones
   * y genera fecha actual en que fue seleccionado.
   * @param $event boolean
   */
  checkTerminosClicked($event: boolean): void {
    this.checkTerminos = $event;
    const date = new Date();
    this.terminosSelectedDateTime = String(date);
  }

  /**
   * Metodo que hace llamado
   */
  obtainIdFolio(): void {
    this.loadingService.show(true);
    this.recaptcha3.getToken().then((token: string) => {
      this.detailedDataSaved = new GuardarDetalleRequestModel();
      this.detailedDataSaved.captcha = token;
      this.detailedDataSaved.generaFolio = true;
      this.registroDetalleService
        .registerDetailedData(this.detailedDataSaved)
        .subscribe((response: GuardarDetalleResponse) => {
          if (response.codigo === '200') {
            this.loadingService.hide();
            this.detailedDataSaved.folio = response.folio;
            this.nroFolio = response.folio;
            //console.log("Folio", response.folio);
          } else {
            this.loadingService.hide();
            throw new Error(
              '[TasadorComponent][obtainIdFolio] Error al generar id de folio'
            );
          }
        });
    });
  }

  registraContactoHubspot() : Observable<any> {
    const requestContacto = new PeticionContacto(
      '',
      '',
      '',
      this.tasadorForm.controls.email.value,
      this.tasadorForm.controls.telefono.value
    );

    return this.hubspotService.enviarContactoHubspotSellIn(requestContacto);
  }

  registraNegocioHubspot(idContacto:string) : Observable<any> {
    const requestNegocio = new PeticionNegocio(
      '',
      '',
      '',
      this.patenteForm.controls.patente.value,
      this.tasadorForm.controls.anio.value.toString(),
      this.datosTasacion.marca,
      this.datosTasacion.modelo,
      '',
      '',
      this.tasadorForm.controls.kilometraje.value,
      '',this.datosTasacion.valor?.toString(),
      '','','','','','','',this.nroFolio,this.datosTasacion.valor?.toString(), this.versionFlujoAlternativo,'',''
    );

    return this.hubspotService.enviarNegocioHubspotSellIn(requestNegocio, idContacto);
  }
}

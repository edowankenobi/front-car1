import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LandingSellInComponent } from './landing-sell-in.component';

@NgModule({
  declarations: [LandingSellInComponent],
  imports: [CommonModule],
  exports: [LandingSellInComponent],
})
export class LandingSellInModule {}

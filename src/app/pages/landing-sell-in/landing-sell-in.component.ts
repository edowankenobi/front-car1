import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-sell-in',
  templateUrl: './landing-sell-in.component.html',
  styleUrls: ['./landing-sell-in.component.scss'],
})
export class LandingSellInComponent implements OnInit, OnDestroy {
  mobile = false;
  constructor(private metaService: Meta, private router: Router) {
    if (window.screen.width <= 780) {
      this.mobile = true;
    }
  }

  ngOnInit(): void {
    this.setMetaData();
  }

  ngOnDestroy(): void {
    this.metaService.removeTag('name=\'title\'');
    this.metaService.removeTag('name=\'description\'');
  }

  /**
   * Metodo que setea meta data especifica al flujo.
   */
  setMetaData(): void {
    this.metaService.addTags([
      {
        name: 'title',
        content: 'En Car1 compramos tu auto usado. Vender tu auto es fácil',
      },
      {
        name: 'description',
        content:
          'En Car1 compramos tu auto usado. Cotizar online para vender tu auto es fácil. El precio en dos minutos. Confianza y rapidez para vender tu auto usado.',
      },
    ]);
  }

  /**
   * Metodo que redirige a pantalla de venta.
   */
  goToVenta(): void {
    this.router.navigateByUrl('/venta-vehiculo');
  }

  /**
   * Metodo que redirige al home.
   */
  redirectToHome(): void {
    this.router.navigateByUrl('');
  }
}

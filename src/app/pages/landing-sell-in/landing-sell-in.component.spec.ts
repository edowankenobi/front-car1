import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { LandingSellInComponent } from './landing-sell-in.component';

describe('LandingSellInComponent', () => {
  let component: LandingSellInComponent;
  let fixture: ComponentFixture<LandingSellInComponent>;
  const routerMock = {
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingSellInComponent ],
      providers: [
        { provide: Router, useValue: routerMock},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingSellInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

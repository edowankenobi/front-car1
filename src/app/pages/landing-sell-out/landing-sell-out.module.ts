import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LandingSellOutComponent } from './landing-sell-out.component';

@NgModule({
  declarations: [LandingSellOutComponent],
  imports: [CommonModule],
  exports: [LandingSellOutComponent],
})
export class LandingSellOutModule {}

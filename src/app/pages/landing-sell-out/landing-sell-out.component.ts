import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-sell-out',
  templateUrl: './landing-sell-out.component.html',
  styleUrls: ['./landing-sell-out.component.scss'],
})
export class LandingSellOutComponent implements OnInit, OnDestroy {
  mobile = false;
  constructor(private metaService: Meta, private router: Router) {
    if (window.screen.width <= 780) {
      this.mobile = true;
    }
  }

  ngOnInit(): void {
    this.setMetaData();
  }

  ngOnDestroy(): void {
    this.metaService.removeTag('name=\'title\'');
    this.metaService.removeTag('name=\'description\'');
  }

  /**
   * Metodo que setea meta data especifica al flujo.
   */
  setMetaData(): void {
    this.metaService.addTags([
      {
        name: 'title',
        content: 'Transparencia para comprar camionetas, SUV y autos usados',
      },
      {
        name: 'description',
        content:
          'El mejor lugar para comprar camionetas usadas, SUV usados, vehículos y autos usados garantizados al mejor precio. En Car1 somos transparentes y fiables',
      },
    ]);
  }

  /**
   * Metodo que redirige a pantalla de venta.
   */
  goToVenta(): void {
    this.router.navigateByUrl('/stock-disponible');
  }

  /**
   * Metodo que redirige al home.
   */
  redirectToHome(): void {
    this.router.navigateByUrl('');
  }
}

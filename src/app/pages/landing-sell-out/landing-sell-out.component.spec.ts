import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { LandingSellOutComponent } from './landing-sell-out.component';

describe('LandingSellOutComponent', () => {
  let component: LandingSellOutComponent;
  let fixture: ComponentFixture<LandingSellOutComponent>;
  const routerMock = {
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingSellOutComponent ],
      providers: [
        { provide: Router, useValue: routerMock},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingSellOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

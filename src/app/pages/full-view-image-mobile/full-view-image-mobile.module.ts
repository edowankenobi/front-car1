import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullViewImageMobileComponent } from './full-view-image-mobile.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { AppSharedModule } from '@app/app.shared.module';

@NgModule({
  declarations: [
    FullViewImageMobileComponent
  ],
  imports: [
    CommonModule,
    CarouselModule,
    AppSharedModule
  ]
})
export class FullViewImageMobileModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FullViewImageMobileComponent } from './full-view-image-mobile.component';

describe('FullViewImageMobileComponent', () => {
  let component: FullViewImageMobileComponent;
  let fixture: ComponentFixture<FullViewImageMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FullViewImageMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FullViewImageMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, HostListener, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { environment } from '@env/environment';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MethodsUtil } from '@app/util/methods.util';

@Component({
  selector: 'app-full-view-image-mobile',
  templateUrl: './full-view-image-mobile.component.html',
  styleUrls: ['./full-view-image-mobile.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 5000, pauseOnFocus: true, noPause: false, showIndicators: true, indicatorsByChunk: false, noWrapSlides: false } }
 ]
})
export class FullViewImageMobileComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { 
    document.body.style.backgroundColor = "#000"; //correccion de error de estilos
  }

  slides: string[] = [];
  activeSlide: number = 0;
  imgHeight: string = 'auto';
  displayCloseBtn: string = '';
  validacion: string = '';
  patente: string = '';

  ngOnInit(): void {
    this.obtainParams();
    this.setHeightValue(screen.availWidth, screen.availHeight);
  }

  generateSlides(patente: string, nfotos: number): void {
    if (nfotos <= 0) return;
    for (let i = 0; i < nfotos; i++) {
      const img = `${environment.fotos.baseURL}${patente}/${environment.fotos.nombre.replace('{number}', i.toString())}`;
      this.slides.push(img);
    }
    this.setActiveSlide();
  }
  obtainParams(): void {
    this.route.params.subscribe((params: Params) => {
      if(params.patente.length != 6 || isNaN(params.validacion) || params.validacion.length < 7) {
        this.navigateStock();
        return;
      }
      if(!MethodsUtil.isMobile()) {
        this.navigateFicha(params.patente);
        return;
      }
      this.validacion = params.validacion;
      this.patente = params.patente;

      const validLocal = MethodsUtil.generateTimePath();
      if(!MethodsUtil.validateTimePath(this.validationNumber, parseInt(validLocal.toString()))) {
        this.navigateFicha(params.patente);
        return;
      }
      this.generateSlides(params.patente, this.nFotos);
    });
  }
  get nFotos(): number {
    return parseInt(this.validacion.substring(0, 2));
  }
  get validationNumber(): number {
    return parseInt(this.validacion.substring(4));
  }
  setActiveSlide(): void {
    setTimeout(() => {
      this.activeSlide = parseInt(this.validacion.substring(2, 4));
    }, 100);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    this.setHeightValue(event.target.innerWidth, event.target.innerHeight);
    this.displayCloseBtnChange(event.target.innerWidth, event.target.innerHeight);
  }
  setHeightValue(width: number, height: number): void {
    this.imgHeight = width > height
      ? height + "px"
      : "auto";
  }
  closeFullView(): void {
    this.navigateFicha(this.patente);
  }
  navigateFicha(patente: string): void {
    this.navigate('/ficha-vehiculo-iframe/' + patente);
  }
  navigateStock(): void {
    this.navigate('/stock-disponible');
  }
  navigate(path: string): void {
    document.body.style.backgroundColor = "#fff"; //correccion de error de estilos
    this.router.navigate([path]);
  }
  displayCloseBtnChange(width: number, height: number): void {
    this.displayCloseBtn = width > height
      ? 'display:none !important'
      : '';
  }
}

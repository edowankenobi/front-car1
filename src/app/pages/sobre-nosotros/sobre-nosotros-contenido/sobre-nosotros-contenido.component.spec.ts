import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SobreNosotrosContenidoComponent } from './sobre-nosotros-contenido.component';

describe('SobreNosotrosContenidoComponent', () => {
  let component: SobreNosotrosContenidoComponent;
  let fixture: ComponentFixture<SobreNosotrosContenidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SobreNosotrosContenidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SobreNosotrosContenidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

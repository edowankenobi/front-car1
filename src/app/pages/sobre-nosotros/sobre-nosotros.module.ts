import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoCompraModule } from '@app/ui/info-compra/info-compra.module';
import { SobreNosotrosComponent } from './sobre-nosotros.component';
import { SobreNosotrosBannerComponent } from './sobre-nosotros-banner/sobre-nosotros-banner.component';
import { SobreNosotrosContenidoComponent } from './sobre-nosotros-contenido/sobre-nosotros-contenido.component';



@NgModule({
  declarations: [SobreNosotrosComponent, SobreNosotrosBannerComponent, SobreNosotrosContenidoComponent],
  imports: [
    CommonModule,
    InfoCompraModule
  ]
})
export class SobreNosotrosModule { }

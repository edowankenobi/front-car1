import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactoModule } from '@app/pages/contacto/contacto.module';
import { ContactoGeneralComponent } from '@app/pages/contacto-general/contacto-general.component';
import { BannerContactoComponent } from './banner-contacto/banner-contacto.component';

@NgModule({
  declarations: [ContactoGeneralComponent, BannerContactoComponent],
  imports: [CommonModule, ContactoModule]
})
export class ContactoGeneralModule {}

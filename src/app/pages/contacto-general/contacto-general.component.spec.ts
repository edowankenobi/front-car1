import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactoGeneralComponent } from './contacto-general.component';

describe('ContactoGeneralComponent', () => {
  let component: ContactoGeneralComponent;
  let fixture: ComponentFixture<ContactoGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactoGeneralComponent ]
    })
    .overrideTemplate(ContactoGeneralComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactoGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

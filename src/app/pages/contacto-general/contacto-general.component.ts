import { Component, OnInit } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-contacto-general',
  templateUrl: './contacto-general.component.html',
  styleUrls: ['./contacto-general.component.scss']
})
export class ContactoGeneralComponent implements OnInit {
  constructor() {
    this.fichaVehiculo = {};
  }

  fichaVehiculo: FichaVehiculoModel = {};

  ngOnInit(): void {}
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BsModalRef,
  BsModalService,
  ModalModule,
  ModalOptions,
} from 'ngx-bootstrap/modal';
import { StockDisponibleComponent } from './stock-disponible.component';

describe('StockDisponibleComponent', () => {
  let component: StockDisponibleComponent;
  let fixture: ComponentFixture<StockDisponibleComponent>;
  const mockModal = {
    show<T = Object>(
      content:
        | string
        | TemplateRef<any>
        | {
            new (...args: any[]): T;
          },
      config?: ModalOptions<T>
    ): BsModalRef<T> {
      return {} as BsModalRef<T>;
    },
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StockDisponibleComponent],
      imports: [HttpClientTestingModule, ModalModule.forRoot()],
      providers: [BsModalRef, { provide: BsModalService, useValue: mockModal }],
    })
      .overrideTemplate(StockDisponibleComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockDisponibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

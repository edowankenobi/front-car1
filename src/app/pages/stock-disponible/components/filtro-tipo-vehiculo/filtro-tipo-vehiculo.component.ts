import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TipoVehiculo } from '@app/models/tipo-vehiculo/tipo-vehiculo.model';
import { FiltrosService } from '../../../../services/filtros/filtros.service';

@Component({
  selector: 'app-filtro-tipo-vehiculo',
  templateUrl: './filtro-tipo-vehiculo.component.html',
  styleUrls: ['./filtro-tipo-vehiculo.component.scss']
})
export class FiltroTipoVehiculoComponent implements OnInit {

  @Input() tipoVehiculos: TipoVehiculo[] = [];
  @Output() item = new EventEmitter<boolean>();

  constructor(private filtrosService: FiltrosService) { }

  ngOnInit(): void {
  }

  tipoSeleccionado(vehiculo: TipoVehiculo): void {
    this.filtrosService.enviarFiltro({ origen: 'tipo', nombre: vehiculo.Nombre });
    this.item.emit(true);
  }

}

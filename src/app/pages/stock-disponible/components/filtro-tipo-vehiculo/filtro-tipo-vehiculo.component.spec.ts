import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroTipoVehiculoComponent } from './filtro-tipo-vehiculo.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FiltroTipoVehiculoComponent', () => {
  let component: FiltroTipoVehiculoComponent;
  let fixture: ComponentFixture<FiltroTipoVehiculoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FiltroTipoVehiculoComponent],
      imports: [HttpClientTestingModule]
    })
      .overrideTemplate(FiltroTipoVehiculoComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroTipoVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardVehiculoComponent } from '@app/pages/stock-disponible/components/card-vehiculo/card-vehiculo.component';
import { CardVehiculoRoutingModule } from '@app/pages/stock-disponible/components/card-vehiculo/card-vehiculo-routing.module';

@NgModule({
  declarations: [CardVehiculoComponent],
  imports: [CommonModule, CardVehiculoRoutingModule],
  exports: [CardVehiculoComponent]
})
export class CardVehiculoModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { CardVehiculoComponent } from './card-vehiculo.component';

describe('CardVehiculoComponent', () => {
  let component: CardVehiculoComponent;
  let fixture: ComponentFixture<CardVehiculoComponent>;
  const routerMock = {
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardVehiculoComponent ],
      providers: [
        { provide: Router, useValue: routerMock},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

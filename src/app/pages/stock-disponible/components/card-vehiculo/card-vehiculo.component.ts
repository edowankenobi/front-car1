import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { LoadingService } from '@app/services/loading/loading.service';

@Component({
  selector: 'app-card-vehiculo',
  templateUrl: './card-vehiculo.component.html',
  styleUrls: ['./card-vehiculo.component.scss'],
})
export class CardVehiculoComponent implements OnInit {
  constructor(private loadingService: LoadingService, private router: Router) {}

  @Input() vehiculo: FichaVehiculoModel = {};

  ngOnInit(): void {
    registerLocaleData(es);
  }

  getUrlMiniatura(): string | undefined {
    if (this.vehiculo.Foto) {
      return this.vehiculo.Foto[0].url;
    }
    return '';
  }

  hideSpinner(v: any): void {
    setTimeout(() => {
      this.loadingService.hide();
    }, 2000);
  }

  redirectToFicha(): void {

    let completeUrl = '';
    let url = '';
    const patente = this.vehiculo.Patente;
    url = 'ficha-vehiculo-iframe';
    completeUrl = [url.slice(0), '/', patente, completeUrl.slice(0)].join('');
    if (!!this.vehiculo) {
      // si se requiere que la ficha se abra en la misma pantalla , dejar esta linea
      // y eliminar la que dicen 'windows.open'
      //this.router.navigate(['ficha-vehiculo-iframe', this.vehiculo.Patente]);
      window.open(completeUrl, '_blank');
    }


    

    
    


  }
}

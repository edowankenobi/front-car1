import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FiltroTransmisionCombustibleComponent } from './filtro-transmision-combustible.component';

describe('FiltroTransmisionCombustibleComponent', () => {
  let component: FiltroTransmisionCombustibleComponent;
  let fixture: ComponentFixture<FiltroTransmisionCombustibleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FiltroTransmisionCombustibleComponent],
      imports: [HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroTransmisionCombustibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe evaluar si el btn se encuentra en el arreglo de filtros', () => {
    const event = 'string';
    const filtrosSeleccionadosMock = {
      nombre: 'string',
    };
    component.filtrosSeleccionados.push(filtrosSeleccionadosMock);
    expect(component.isMarcada(event)).toBe(true);
  });
});

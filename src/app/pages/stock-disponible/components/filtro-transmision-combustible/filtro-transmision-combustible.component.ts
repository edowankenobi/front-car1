import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FiltrosService } from '@app/services/filtros/filtros.service';
import { TipoModel } from '@app/models/tipo.model';

@Component({
  selector: 'app-filtro-transmision-combustible',
  templateUrl: './filtro-transmision-combustible.component.html',
  styleUrls: ['./filtro-transmision-combustible.component.scss']
})
export class FiltroTransmisionCombustibleComponent implements OnInit {
  @Output() item = new EventEmitter<boolean>();

  /**
   * Arreglo de tipos de Transmisión a disponer en el filtro.
   */
  @Input() tiposTransmision: TipoModel[] = [];

  /**
   * Arreglo de tipos de Combustible a disponer en el filtro.
   */
  @Input() tiposCombustible: TipoModel[] = [];

  /**
   * Filtros seleccionados
   */
  filtrosSeleccionados: any[];

  constructor(private filtrosService: FiltrosService) {
    this.filtrosSeleccionados = [];
    this.filtrosService.filtrosSeleccionados$.subscribe(res => {
      this.filtrosSeleccionados = res;
    });
  }

  ngOnInit(): void {}

  cerrarDropdown(): void {
    this.item.emit(true);
  }

  filtroSeleccionado(origen: string, filtro: string): void {
    this.filtrosService.enviarFiltro({ origen, nombre: filtro });
  }

  /**
   * Método que evalúa si el botón se encuentra en el arreglo de filtros o no.
   * @param $event nombre de filtro a evaluar
   */
  isMarcada($event: string): boolean {
    let marcada = false;
    if (this.filtrosSeleccionados.length > 0) {
      for (const filtro of this.filtrosSeleccionados) {
        if (filtro.nombre === $event) {
          marcada = true;
        }
      }
    }
    return marcada;
  }
}

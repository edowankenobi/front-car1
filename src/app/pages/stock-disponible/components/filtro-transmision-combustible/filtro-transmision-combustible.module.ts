import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroTransmisionCombustibleComponent } from '@app/pages/stock-disponible/components/filtro-transmision-combustible/filtro-transmision-combustible.component';

@NgModule({
  declarations: [FiltroTransmisionCombustibleComponent],
  imports: [CommonModule],
  exports: [FiltroTransmisionCombustibleComponent]
})
export class FiltroTransmisionCombustibleModule {}

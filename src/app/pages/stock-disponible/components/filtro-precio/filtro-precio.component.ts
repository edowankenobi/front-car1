import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { RangeOptions } from '@app/models/filtros/range/range-options.model';

@Component({
  selector: 'app-filtro-precio',
  templateUrl: './filtro-precio.component.html',
  styleUrls: ['./filtro-precio.component.scss']
})
export class FiltroPrecioComponent implements OnInit {

  @Output() item = new EventEmitter<boolean>();

  opciones: RangeOptions;

  constructor() {

    this.opciones = {
      origen: 'precio',
      titulo: 'Rango de precio',
      minValue: 0,
      maxValue: 20000000,
      floor: 0,
      ceil: 100000000,
      step: 500000
    };
   }

  ngOnInit(): void {
  }

  cerrarDropdown(): void{
    this.item.emit(true);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroAnioKilometrajeComponent } from './filtro-anio-kilometraje.component';

describe('FiltroAnioKilometrajeComponent', () => {
  let component: FiltroAnioKilometrajeComponent;
  let fixture: ComponentFixture<FiltroAnioKilometrajeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiltroAnioKilometrajeComponent ]
    })
      .overrideTemplate(FiltroAnioKilometrajeComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroAnioKilometrajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

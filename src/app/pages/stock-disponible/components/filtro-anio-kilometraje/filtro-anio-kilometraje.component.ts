import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { RangeOptions } from '@app/models/filtros/range/range-options.model';

@Component({
  selector: 'app-filtro-anio-kilometraje',
  templateUrl: './filtro-anio-kilometraje.component.html',
  styleUrls: ['./filtro-anio-kilometraje.component.scss']
})
export class FiltroAnioKilometrajeComponent implements OnInit {

  @Output() item = new EventEmitter<boolean>();

  opcionesAnio: RangeOptions;
  opcionesKms: RangeOptions;

  minyearval: number = 2012;
  get maxyearval(): number {
    const date = new Date();
    let year = date.getFullYear()
    
    //septiembre, getMonth() devuelve el mes -1
    if(date.getMonth() >= 8) {
      year++;
    }
    return year;
  }
  constructor() {
    this.opcionesAnio = {
      origen: 'anio',
      titulo: 'Rango de años',
      minValue: this.minyearval,
      maxValue: this.maxyearval,
      floor: this.minyearval,
      ceil: this.maxyearval,
      step: 1
    };

    this.opcionesKms = {
      origen: 'kilometros',
      titulo: 'Rango de kilómetros',
      minValue: 0,
      maxValue: 80000,
      floor: 0,
      ceil: 150000,
      step: 1000
    };
  }

  ngOnInit(): void {
  }

  cerrarDropdown(): void{
    this.item.emit(true);
  }
}

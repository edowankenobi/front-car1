import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FiltrosService } from '@app/services/filtros/filtros.service';
import { MarcaModel } from '@app/models/marcas/marca.model';

@Component({
  selector: 'app-filtro-marca',
  templateUrl: './filtro-marca.component.html',
  styleUrls: ['./filtro-marca.component.scss']
})
export class FiltroMarcaComponent implements OnInit {
  @Output() item = new EventEmitter<boolean>();

  /**
   * Arreglo de marcas a disponer en el filtro.
   */
  @Input() marcas: MarcaModel[] = [];

  constructor(private filtrosService: FiltrosService) {}

  ngOnInit(): void {}

  marcaSeleccionada(marca: string): void {
    this.filtrosService.enviarFiltro({ origen: 'marca', nombre: marca });
    this.item.emit(true);
  }
}

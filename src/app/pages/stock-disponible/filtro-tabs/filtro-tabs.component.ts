import { Component, OnInit, Input } from '@angular/core';
import { FiltroModel } from '@app/models/filtros/filtro.model';
import { TipoVehiculo } from '@app/models/tipo-vehiculo/tipo-vehiculo.model';
import { TipoModel } from '@app/models/tipo.model';
import { MarcaModel } from '@app/models/marcas/marca.model';
import { environment } from '@env/environment';

@Component({
  selector: 'app-filtro-tabs',
  templateUrl: './filtro-tabs.component.html',
  styleUrls: ['./filtro-tabs.component.scss']
})
export class FiltroTabsComponent implements OnInit {
  verTipoVehiculo: boolean;
  verMarca: boolean;
  verPrecio: boolean;
  verAnioKilometraje: boolean;
  verTransmisionCombustible: boolean;
  /**
   * Filtros a mostrar
   */
  @Input() filtros: FiltroModel[] = [];
  @Input() tipoVehiculos: TipoVehiculo[] = [];

  /**
   * Arreglo de tipos de Transmisión a disponer en el filtro.
   */
  @Input() tiposTransmision: TipoModel[] = [];

  /**
   * Arreglo de tipos de Combustible a disponer en el filtro.
   */
  @Input() tiposCombustible: TipoModel[] = [];

  /**
   * Arreglo de marcas a disponer en el filtro.
   */
  @Input() marcas: MarcaModel[] = [];

  constructor() {
    this.verTipoVehiculo = false;
    this.verMarca = false;
    this.verPrecio = false;
    this.verAnioKilometraje = false;
    this.verTransmisionCombustible = false;
  }

  ngOnInit(): void {}

  verOcultar(filtro: string): void {
    switch (filtro) {
      case 'tipoVehiculo': {
        this.verTipoVehiculo = !this.verTipoVehiculo;
        this.verMarca = false;
        this.verPrecio = false;
        this.verAnioKilometraje = false;
        this.verTransmisionCombustible = false;
        break;
      }
      case 'marca': {
        this.verMarca = !this.verMarca;
        this.verTipoVehiculo = false;
        this.verPrecio = false;
        this.verAnioKilometraje = false;
        this.verTransmisionCombustible = false;
        break;
      }
      case 'precio': {
        this.verPrecio = !this.verPrecio;
        this.verTipoVehiculo = false;
        this.verMarca = false;
        this.verAnioKilometraje = false;
        this.verTransmisionCombustible = false;
        break;
      }
      case 'anio-kilometraje': {
        this.verAnioKilometraje = !this.verAnioKilometraje;
        this.verTipoVehiculo = false;
        this.verMarca = false;
        this.verPrecio = false;
        this.verTransmisionCombustible = false;
        break;
      }
      case 'transmision-combustible': {
        this.verTransmisionCombustible = !this.verTransmisionCombustible;
        this.verTipoVehiculo = false;
        this.verMarca = false;
        this.verPrecio = false;
        this.verAnioKilometraje = false;
        break;
      }
    }
  }

  opcionFiltroSeleccionada(filtroSeleccionado: boolean): void {
    this.verTipoVehiculo = !filtroSeleccionado;
    this.verMarca = !filtroSeleccionado;
    this.verPrecio = !filtroSeleccionado;
    this.verAnioKilometraje = !filtroSeleccionado;
    this.verTransmisionCombustible = !filtroSeleccionado;
  }

  agendaHubspot(): void {
    window.open(environment.hubspot.agenda, '_blank');
  }
}

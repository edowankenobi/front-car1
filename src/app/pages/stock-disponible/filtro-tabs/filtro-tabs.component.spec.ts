import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroTabsComponent } from './filtro-tabs.component';

describe('FiltroTabsComponent', () => {
  let component: FiltroTabsComponent;
  let fixture: ComponentFixture<FiltroTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiltroTabsComponent ]
    })
    .overrideTemplate(FiltroTabsComponent, '')
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('verOcultar tipoVehiculo', () => {
    component.verOcultar('tipoVehiculo');
    expect(component.verTipoVehiculo).toEqual(true)
  });

  it('verOcultar marca', () => {
    component.verOcultar('marca');
    expect(component.verMarca).toEqual(true)
  });

  it('verOcultar precio', () => {
    component.verOcultar('precio');
    expect(component.verPrecio).toEqual(true)
  });

  it('verOcultar anio-kilometraje', () => {
    component.verOcultar('anio-kilometraje');
    expect(component.verAnioKilometraje).toEqual(true)
  });

  it('verOcultar transmision-combustible', () => {
    component.verOcultar('transmision-combustible');
    expect(component.verTransmisionCombustible).toEqual(true)
  });

  it('opcionFiltroSeleccionada', () => {
    component.opcionFiltroSeleccionada(false);
    expect(component.verTipoVehiculo).toEqual(true)
    expect(component.verMarca).toEqual(true)
    expect(component.verPrecio).toEqual(true)
    expect(component.verAnioKilometraje).toEqual(true)
    expect(component.verTransmisionCombustible).toEqual(true)
  });
});

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CatalogoModule } from '@app/pages/stock-disponible/catalogo/catalogo.module';
import { StockDisponibleComponent } from '@app/pages/stock-disponible/stock-disponible.component';
import { ModalsModule } from '../../ui/modals/modals.module';
import { RangeModule } from '../../ui/range/range.module';
import { FiltroAnioKilometrajeComponent } from './components/filtro-anio-kilometraje/filtro-anio-kilometraje.component';
import { FiltroMarcaComponent } from './components/filtro-marca/filtro-marca.component';
import { FiltroPrecioComponent } from './components/filtro-precio/filtro-precio.component';
import { FiltroTipoVehiculoComponent } from './components/filtro-tipo-vehiculo/filtro-tipo-vehiculo.component';
// tslint:disable-next-line:max-line-length
import { FiltroTransmisionCombustibleComponent } from './components/filtro-transmision-combustible/filtro-transmision-combustible.component';
import { FiltroSeleccionadoComponent } from './filtro-seleccionado/filtro-seleccionado.component';
import { FiltroTabsComponent } from './filtro-tabs/filtro-tabs.component';

@NgModule({
  declarations: [
    StockDisponibleComponent,
    FiltroTabsComponent,
    FiltroSeleccionadoComponent,
    FiltroMarcaComponent,
    FiltroTipoVehiculoComponent,
    FiltroPrecioComponent,
    FiltroAnioKilometrajeComponent,
    FiltroTransmisionCombustibleComponent,
  ],
  imports: [
    CommonModule,
    RangeModule,
    CatalogoModule,
    FormsModule,
    ModalsModule,
  ],
})
export class StockDisponibleModule {}

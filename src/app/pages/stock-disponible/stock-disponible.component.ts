import { Component, OnInit } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { FiltroModel } from '@app/models/filtros/filtro.model';
import { MarcaModel } from '@app/models/marcas/marca.model';
import { TipoVehiculo } from '@app/models/tipo-vehiculo/tipo-vehiculo.model';
import { TipoModel } from '@app/models/tipo.model';
import { CaracteristicasService } from '@app/services/caracteristicas/caracteristicas.service';
import { CatalogoService } from '@app/services/catalogo/catalogo.service';
import { FiltrosService } from '@app/services/filtros/filtros.service';
import { LoadingService } from '@app/services/loading/loading.service';
import { MarcasService } from '@app/services/marcas/marcas.service';
import { TipoVehiculoService } from '@app/services/tipo-vehiculo/tipo-vehiculo.service';
import { ModalQuieroVenderComponent } from '@app/ui/modals/modal-quiero-vender/modal-quiero-vender.component';
import { FiltraCatalogoUtil } from '@app/util/filtra-catalogo.util';
import { filtrosStrapiUtil } from '@app/util/filtros-strapi.util';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-stock-disponible',
  templateUrl: './stock-disponible.component.html',
  styleUrls: ['./stock-disponible.component.scss'],
})
export class StockDisponibleComponent implements OnInit {
  /**
   * Filtros a cargar en los tabs
   */
  filtros: FiltroModel[] = [];

  /**
   * Arreglo de tipo de vehículos a mostrar en filtro
   */
  tipoVehiculos: TipoVehiculo[] = [];

  /**
   * Arreglo de vehículos con el que se inicializa la vista
   */
  vehiculos: FichaVehiculoModel[] = [];

  /**
   * Arreglo de todos los vehículos disponibles en car1
   */
  totalVehiculos: FichaVehiculoModel[] = [];

  /**
   * Arreglo de tipos de Transmisión a disponer en el filtro.
   */
  tiposTransmision: TipoModel[] = [];

  /**
   * Arreglo de tipos de Combustible a disponer en el filtro.
   */
  tiposCombustible: TipoModel[] = [];

  /**
   * Arreglo de marcas a disponer en el filtro.
   */
  marcas: MarcaModel[] = [];

  /**
   * Contador de resultados a desplegar
   */
  contadorResultados = 0;

  /**
   * Filtro seleccionado en la barra de filtros
   */
  filtroSeleccionado: Subscription;

  /**
   * referencia al modal
   */
  bsModalRef!: BsModalRef;

  /**
   * Filtros cargados como parámetro de carga
   */
  filtrosParam: any[] = [];

  /**
   * Lista de filtros seleccionados
   */
  filtrosSeleccionados: any[];

  constructor(
    private filtrosService: FiltrosService,
    private tipoVehiculoService: TipoVehiculoService,
    private catalogoService: CatalogoService,
    private caracteristicasService: CaracteristicasService,
    private marcasService: MarcasService,
    private loadingService: LoadingService,
    private modalService: BsModalService
  ) {
    this.filtrosSeleccionados = [];
    this.filtroSeleccionado = this.filtrosService.filtroSeleccionado$.subscribe(
      (res) => {
        this.filtrosParam.push(res);
       
        
        this.catalogoService.catalogoVehiculos().subscribe((response) => {
          
          this.totalVehiculos = response;
          this.vehiculos = FiltraCatalogoUtil.filtrarVehiculos(
            this.filtrosParam,
            this.totalVehiculos
          );
          this.filtrosParam.forEach(element => {

          if(element.origen === 'marca' || element.origen === 'tipo') {
            this.filtrosService.enviarFiltro({ origen: element.origen, nombre: element.nombre });
          }

          });
          this.contadorResultados = this.vehiculos.length;
        });
      }
    );
    this.filtrosService.filtrosSeleccionados$.subscribe((res) => {
      this.loadingService.show();
      this.vehiculos = FiltraCatalogoUtil.filtrarVehiculos(
        res,
        this.totalVehiculos
      );
      this.contadorResultados = this.vehiculos.length;
      setTimeout(() => {
        this.loadingService.hide();
      }, 2000);
    });

    this.filtrosService.enviarFiltro({
      origen: filtrosStrapiUtil.orden.origen,
      nombre: 'published_at:ASC',
    });
    this.filtroSeleccionado.unsubscribe();
    this.inicializarFiltros();
    this.inicializaTipoVehiculo();
  }

  ngOnInit(): void {
    //se elimina la carga del modal
    //this.initModalVender();
  }

  /**
   * Método encargado de obtener los filtros indicados en strapi
   */
  inicializarFiltros(): void {
    this.loadingService.show();
    this.filtrosService.filtros().subscribe((response) => {
      this.filtros = response;
      setTimeout(() => {
        this.loadingService.hide();
      }, 2000);
    });
    this.loadingService.show();
    this.caracteristicasService
      .obtenerTipoTransmision()
      .subscribe((response) => {
        this.tiposTransmision = response;
        setTimeout(() => {
          this.loadingService.hide();
        }, 2000);
      });
    this.loadingService.show();
    this.caracteristicasService
      .obtenerTipoCombustible()
      .subscribe((response) => {
        this.tiposCombustible = response;
        setTimeout(() => {
          this.loadingService.hide();
        }, 2000);
      });
    this.loadingService.show();
    this.marcasService.marcas().subscribe((response) => {
      this.marcas = response;
      setTimeout(() => {
        this.loadingService.hide();
      }, 2000);
    });
  }

  /**
   * Método encargado de inicializar los tipos de vehículos
   */
  inicializaTipoVehiculo(): void {
    this.tipoVehiculoService.tipoVehiculos().subscribe((response) => {
      this.tipoVehiculos = response;
    });
  }

  /**
   * Inicializa modal de quiero vender
   */
  initModalVender(): void {
    this.loadingService.show();
    const initialState = {
      modalHome: false,
    };
    this.bsModalRef = this.modalService.show(ModalQuieroVenderComponent, {
      initialState,
      backdrop: 'static',
      keyboard: false,
      class: 'modal-lg modal-dialog-centered',
    });
    setTimeout(() => {
      this.loadingService.hide();
    }, 2000);
  }
}

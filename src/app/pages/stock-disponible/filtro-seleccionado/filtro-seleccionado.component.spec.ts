import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroSeleccionadoComponent } from './filtro-seleccionado.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FiltrosService } from '@app/services/filtros/filtros.service';
import { Observable } from 'rxjs';

describe('FiltroSeleccionadoComponent', () => {
  let component: FiltroSeleccionadoComponent;
  let fixture: ComponentFixture<FiltroSeleccionadoComponent>;

  const filtrosServiceMock = {
    get filtroSeleccionado$(): any {
      return new Observable((observer) => {
        observer.next('abc');
        observer.complete();
      });
    },
    limpiarFiltro() {},
    limpiarArrayFiltros() {},
    enviarArrayFiltros(param: any) {},
    enviarFiltro(param: any) {}
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FiltroSeleccionadoComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: FiltrosService, useValue: filtrosServiceMock},
      ]
    })
      .overrideTemplate(FiltroSeleccionadoComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroSeleccionadoComponent);
    component = fixture.componentInstance;
    component.filtrosSeleccionados = [component.listOptions[1]];
    component.filtrosSeleccionados[0].nombre = 'abc';
    component.filtrosSeleccionados[0].origen = 'abc';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnDestroy', () => {
    component.ngOnDestroy();
    expect(component).toBeTruthy();
  });

  it('agregarFiltroRange', () => {
    component.agregarFiltroRange(component.filtrosSeleccionados[0]);
    expect(component).toBeTruthy();
  });

  it('agregarFiltroRange false', () => {
    component.agregarFiltroRange({});
    expect(component).toBeTruthy();
  });

  it('agregarFiltroString', () => {
    component.agregarFiltroString(component.filtrosSeleccionados[0]);
    expect(component).toBeTruthy();
  });

  it('eliminarFiltro', () => {
    component.eliminarFiltro(0);
    expect(component).toBeTruthy();
  });

  it('ordenarResultados', () => {
    component.selectedOption = {value: 'abc'};
    component.ordenarResultados();
    expect(component).toBeTruthy();
  });
});

import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { FiltrosService } from '../../../services/filtros/filtros.service';
import { filtrosStrapiUtil } from '@app/util/filtros-strapi.util';

@Component({
  selector: 'app-filtro-seleccionado',
  templateUrl: './filtro-seleccionado.component.html',
  styleUrls: ['./filtro-seleccionado.component.scss']
})
export class FiltroSeleccionadoComponent implements OnInit, OnDestroy {
  /**
   * Filtro seleccionado en la barra de filtros
   */
  filtroSeleccionado: Subscription;

  /**
   * Arreglo de filtros seleccionados
   */
  filtrosSeleccionados: any[];

  /**
   * Opciones de ordenamiento de stock
   */
  listOptions = [
    {
      id: 0,
      value: '',
      text: 'Seleccione'
    },
    {
      id: 1,
      value: filtrosStrapiUtil.orden.precio.descendiente,
      text: 'De mayor a menor precio'
    },
    {
      id: 2,
      value: filtrosStrapiUtil.orden.precio.ascendiente,
      text: 'De menor a mayor precio'
    },
    {
      id: 3,
      value: filtrosStrapiUtil.orden.kilometraje.descendiente,
      text: 'De mayor a menor KM'
    },
    {
      id: 4,
      value: filtrosStrapiUtil.orden.kilometraje.ascendiente,
      text: 'De menor a mayor KM'
    },
    {
      id: 5,
      value: filtrosStrapiUtil.orden.fecha.ascendiente,
      text: 'Más reciente'
    },
    {
      id: 6,
      value: filtrosStrapiUtil.orden.fecha.descendiente,
      text: 'Más antiguo'
    }
  ];

  /**
   * Opcion de ordenamiento seleccionada
   */
  selectedOption: any;

  /**
   * Contador de resultados a desplegar
   */
  @Input() contadorResultados = 0;

  constructor(private filtrosService: FiltrosService) {
    this.selectedOption = this.listOptions[0];
    this.filtrosSeleccionados = [];
    this.filtroSeleccionado = this.filtrosService.filtroSeleccionado$.subscribe(
      res => {
        if (res !== '') {
          if (this.validaFiltroRange(res)) {
            this.agregarFiltroRange(res);
          } else {
            this.agregarFiltroString(res);
          }
          this.filtrosService.enviarArrayFiltros(this.filtrosSeleccionados);
        }
      }
    );
  }

  ngOnInit(): void {
    registerLocaleData(es);
  }

  ngOnDestroy(): void {
    if (this.filtroSeleccionado !== undefined) {
      this.filtrosService.limpiarFiltro();
      this.filtrosService.limpiarArrayFiltros();
      this.filtroSeleccionado.unsubscribe();
    }
  }

  validaFiltroRange(filtro: any): boolean {
    return (
      filtro.origen === 'anio' ||
      filtro.origen === 'kilometros' ||
      filtro.origen === 'precio' ||
      filtro.origen === 'marca' ||
      filtro.origen === 'tipo' ||
      filtro.origen === 'transmision' ||
      filtro.origen === 'combustible' ||
      filtro.origen === filtrosStrapiUtil.orden.origen
    );
  }

  agregarFiltroString(filtro: any): void {
    if (
      !this.filtrosSeleccionados.some(elem => elem.nombre === filtro.nombre)
    ) {
      this.filtrosSeleccionados.push(filtro);
    }
  }

  agregarFiltroRange(filtro: any): void {
    if (this.filtrosSeleccionados.some(elem => elem.origen === filtro.origen)) {
      this.reemplazarFiltroRange(filtro);
    } else {
      this.filtrosSeleccionados.push(filtro);
    }
  }

  eliminarFiltro(indice: number): void {
    this.filtrosSeleccionados.splice(indice, 1);
    this.filtrosService.enviarArrayFiltros(this.filtrosSeleccionados);
  }

  modificarFiltro(indice: number): void {
    this.filtrosSeleccionados.splice(indice, 1);
  }

  reemplazarFiltroRange(filtro: any): void {
    this.filtrosSeleccionados.forEach((elem, index) => {
      if (elem.origen === filtro.origen) {
        this.modificarFiltro(index);
        this.filtrosSeleccionados.push(filtro);
      }
    });
  }

  ordenarResultados(): void {
    if (this.selectedOption.value !== '') {
      this.filtrosService.enviarFiltro({
        origen: filtrosStrapiUtil.orden.origen,
        nombre: this.selectedOption.value
      });
    }
  }
}

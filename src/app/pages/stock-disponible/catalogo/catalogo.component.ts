import { Component, Input, OnInit } from '@angular/core';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.scss'],
})
export class CatalogoComponent implements OnInit {
  /**
   * Arreglo de datos de vehiculo
   */
  @Input() vehiculos: FichaVehiculoModel[] = [];

  constructor() {}

  ngOnInit(): void {}
}

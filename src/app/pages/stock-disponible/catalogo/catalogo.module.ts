import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogoComponent } from '@app/pages/stock-disponible/catalogo/catalogo.component';
import { CardVehiculoModule } from '@app/pages/stock-disponible/components/card-vehiculo/card-vehiculo.module';

@NgModule({
  declarations: [CatalogoComponent],
  imports: [CommonModule, CardVehiculoModule],
  exports: [CatalogoComponent]
})
export class CatalogoModule {}

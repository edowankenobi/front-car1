import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactoComponent } from '@app/pages/contacto/contacto.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { RecaptchaFormsModule, RecaptchaModule } from 'ng-recaptcha';
import { TasadorModule } from '@app/pages/tasador/tasador.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BsModalRef, BsModalService, ModalModule, ModalOptions } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { TasadorService } from '@app/services/tasador/tasador.service';
import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';
import { Observable } from 'rxjs';
import { TemplateRef } from '@angular/core';
import { NgRecaptcha3Service } from 'ng-recaptcha3';
import { ContactoService } from '@app/services/contacto/contacto.service';
import { PeticionEnvioCorreo } from '@app/models/contacto/peticion-envio-correo.model';
import { RespuestaEnvioCorreo } from '@app/models/contacto/respuesta-envio-correo.model';
import { PartePagoModalComponent } from '@app/ui/modals/parte-pago-modal/parte-pago-modal.component';

describe('ContactoComponent', () => {
  let component: ContactoComponent;
  let fixture: ComponentFixture<ContactoComponent>;
  let formBuilder: FormBuilder;

  const recaptchaMock = {
    init(val: any) {},
    getToken() {
      return new Promise((resolve, reject) => {
        resolve('token2');
      });
    },
    destroy() {},
  };

  const routerMock = {
    getCurrentNavigation(): any {
      return {
        extras: {
          state: {
            flag: true
          }
        }
      }
    }
  }

  const tasadorServiceMock = {
    get ofertaAceptada$(): any {
      return new Observable((observer) => {
        observer.next({ } as ValorTasacionModel);
        observer.complete();
      });
    },
    limpiarOferta() {
    },

  };

  const contactoServiceMock = {
    enviarFormularioContacto(
      formulario: PeticionEnvioCorreo
    ): Observable<RespuestaEnvioCorreo> {
      return new Observable((observer) => {
        observer.next({ codigo: '200' } as RespuestaEnvioCorreo);
        observer.complete();
      });
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ContactoComponent],
      imports: [
        ReactiveFormsModule,
        RecaptchaModule,
        RecaptchaFormsModule,
        TasadorModule,
        HttpClientTestingModule,
        ModalModule.forRoot()
      ],
      providers: [
        FormBuilder,
        BsModalService,
        BsModalRef,
        { provide: Router, useValue: routerMock},
        { provide: TasadorService, useValue: tasadorServiceMock},
        { provide: NgRecaptcha3Service, useValue: recaptchaMock },
        { provide: ContactoService, useValue: contactoServiceMock },
      ]
    })
        .overrideTemplate(ContactoComponent, '')
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactoComponent);
    formBuilder = TestBed.inject(FormBuilder);
    component = fixture.componentInstance;
    component.contactoForm = formBuilder.group({
      captcha: [],
      autoPartePago: [],
      rut: [],
      mensaje: [],
      nombre: [],
      apellidos: [],
      telefono: [],
      email: [],
    });
    component.partePago = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnDestroy', () => {
    component.ngOnDestroy();
    expect(component).toBeTruthy();
  });

  it('obtainDataFromBanner', () => {
    component.obtainDataFromBanner();
    expect(component).toBeTruthy();
  });

  it('onFinanciaCompra', () => {
    component.onFinanciaCompra();
    expect(component.financiaCompra).toEqual(true)
  });

  it('onPartePago', () => {
    component.onPartePago();
    expect(component).toBeTruthy();
  });

  it('onSubmit', () => {
    component.onSubmit();
    expect(component).toBeTruthy();
  });

  it('validaRut', () => {
    component.contactoForm.controls.rut.setValue('17808613-8')
    component.validaRut(component.contactoForm.controls.rut);
    expect(component).toBeTruthy();
  });

  it('formDisabled', () => {
    component.formDisabled();
    expect(component).toBeTruthy();
  });

  it('onFocusOutRut value', () => {
    component.onFocusOutRut({target: {
      value: '1'
    }});
    expect(component).toBeTruthy();
  });

  it('onFocusOutRut empty', () => {
    component.onFocusOutRut({target: {
      value: ''
    }});
    expect(component).toBeTruthy();
  });

  it('onFocusOutRut null', () => {
    component.onFocusOutRut({target: {
      value: null
    }});
    expect(component).toBeTruthy();
  });
});

import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { PeticionEnvioCorreo } from '@app/models/contacto/peticion-envio-correo.model';
import { RespuestaEnvioCorreo } from '@app/models/contacto/respuesta-envio-correo.model';
import { FichaVehiculoModel } from '@app/models/ficha-vehiculo/ficha-vehiculo.model';
import { ContactoService } from '@app/services/contacto/contacto.service';
import { LoadingService } from '@app/services/loading/loading.service';
import { ModalsService } from '@app/services/modals/modals.service';
import { TasadorService } from '@app/services/tasador-interno/tasador-interno.service';
import { LoggerService } from '@app/shared/services/logger.service';
import { PartePagoModalComponent } from '@app/ui/modals/parte-pago-modal/parte-pago-modal.component';
import { environment } from '@env/environment';
// import { validateRut } from '@fdograph/rut-utilities';
import { NgRecaptcha3Service } from 'ng-recaptcha3';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable, Subscription } from 'rxjs';
import { RespuestaEnvioCorreoComponent } from '../../ui/modals/respuesta-envio-correo/respuesta-envio-correo.component';
import { HubspotService } from '@app/services/hubspot/hubspot.services';
import { PeticionContacto } from '@app/models/hubspot/peticion-contacto.model';
import { PeticionNegocio } from '@app/models/hubspot/peticion-negocio.model';
import { TasadorCotizacionModel, TasadorCotizacionMethods } from '@app/models/tasador-interno/tasador.cotizacion.model';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss'],
})
export class ContactoComponent implements OnInit, OnDestroy {
  contactoForm: FormGroup;

  public financiaCompra: boolean;
  public partePago: boolean;
  public textoBotonEnviar: string;
  public TEXTO_ENVIAR = 'ENVIAR';
  public TEXTO_ENVIANDO = 'ENVIANDO..';

  /**
   * Constante con la llave para el captcha de inicio de sesión.
   */
  SITE_KEY = environment.siteKeyCaptcha;

  /**
   * Oferta aceptada en formulario de tasación
   */
  ofertaAceptada: Subscription;

  /**
   * Acción recibida desde modal
   */
  modalAction!: Subscription;

  /**
   * referencia al modal
   */
  bsModalRef!: BsModalRef;

  /**
   * Objeto oferta recibido del modal de tasación
   */
  ofertaPartePago: TasadorCotizacionModel = TasadorCotizacionMethods.emptyTasadorCotizacionModel();

  /**
   * Bandera para mostrar/ocultar resumen de la oferta
   */
  mostrarOferta = false;

  /*
   * Indica si rut ha sido ingresado
   */
  rutNoIngresado: boolean;

  /**
   * variable que contiene data emitida de redireccionamiento del banner.
   */
  dataBanner!: any;

  constructor(
    private formBuilder: FormBuilder,
    private servicio: ContactoService,
    private logger: LoggerService,
    private loadingService: LoadingService,
    private tasadorService: TasadorService,
    private modalService: BsModalService,
    private modalsService: ModalsService,
    private recaptcha3: NgRecaptcha3Service,
    private router: Router,
    private hubspotService : HubspotService
  ) {
    this.obtainDataFromBanner();
    this.financiaCompra = false;
    this.partePago = false;
    this.textoBotonEnviar = this.TEXTO_ENVIAR;

    this.contactoForm = this.formBuilder.group({
      nombre: [
        '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$'),
          Validators.minLength(2),
        ],
      ],
      apellidos: [
        '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$'),
          Validators.minLength(2),
        ],
      ],
      telefono: [
        '',
        [
          Validators.required,
          Validators.pattern('^([+])([0-9]+$)'),
          Validators.minLength(12),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern(
            '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'
          ),
          Validators.minLength(2),
        ],
      ],
      mensaje: ['', Validators.maxLength(350)],
      financiarCompra: false,
      autoPartePago: false,
      // rut: [
      //   '',
      //   [
      //     Validators.pattern('^[0-9]{7,8}[-][0-9kK]{1}$'),
      //     Validators.minLength(9),
      //     this.validaRut,
      //   ],
      // ],
      patente: ['', Validators.pattern('^[A-Za-z]{4}[0-9]{2}$')],
      terminosCondiciones: false,
      captcha: '',
    });
    this.modalAction = this.modalsService.modalAction$.subscribe((res) => {
      this.modalService.hide();
      this.partePago = res;
      this.contactoForm.controls.autoPartePago.setValue(this.partePago);
      if (this.partePago) {
        this.mostrarOferta = true;
      }
    });
    this.ofertaAceptada = this.tasadorService.ofertaAceptada$.subscribe(
      (res: any) => {
        if (
          res === null ||
          res.fechaInicioServicio === '' ||
          Object.entries(res).length === 0
        ) {
          this.partePago = false;
        } else {
          this.ofertaPartePago = Object.assign({}, res);
          this.modalService.hide();
        }
      }
    );
    this.rutNoIngresado = false;
  }

  @Input() tipoContacto = '';
  @Input() fichaVehiculo: FichaVehiculoModel = {};
  @Input() urlPdfTerminosCondiciones!: string;

  ngOnInit(): void {
    registerLocaleData(es);
    this.recaptcha3.init(environment.siteKeyCaptcha);
  }

  ngOnDestroy(): void {
    if (this.ofertaAceptada !== undefined) {
      this.tasadorService.limpiarOferta();
      this.ofertaAceptada.unsubscribe();
    }
    if (this.modalAction !== undefined) {
      this.modalAction.unsubscribe();
    }
    this.recaptcha3.destroy();
  }

  /**
   * Metodo que obtiene data emitida desde el routing del banner
   */
  obtainDataFromBanner(): void {
    if (!!this.router.getCurrentNavigation()?.extras.state) {
      this.dataBanner = this.router.getCurrentNavigation()?.extras.state;
      this.dataBanner.flag = true;
    }
  }

  onFinanciaCompra(): void {
    this.financiaCompra = !this.financiaCompra;
  }

  onPartePago(): void {
    this.partePago = !this.partePago;
    this.contactoForm.controls.autoPartePago.setValue(this.partePago);
    if (this.partePago && TasadorCotizacionMethods.isEmpty(this.ofertaPartePago)) {
      const initialState = {
        oferta: this.ofertaPartePago,
      };
      this.bsModalRef = this.modalService.show(PartePagoModalComponent, {
        initialState,
        backdrop: 'static',
        keyboard: false,
        class: 'modal-xl',
        ignoreBackdropClick: true,
      });
    }
    if (this.partePago && !TasadorCotizacionMethods.isEmpty(this.ofertaPartePago)) {
      this.mostrarOferta = true;
    } else {
      this.mostrarOferta = false;
    }
  }

  onSubmit(): void {
    if (!this.contactoForm.valid) {
      return;
    }

    this.recaptcha3.getToken().then(
      (token: string) => {
        this.contactoForm.controls.captcha.setValue(token);

        this.loadingService.show(true);
        this.textoBotonEnviar = this.TEXTO_ENVIANDO;

        let patente: string = this.ofertaPartePago?.patente
          ? this.ofertaPartePago?.patente
          : '';
        patente = patente ? patente.toUpperCase() : '';

        // let rut: string = this.contactoForm.controls.rut.value;
        // rut = rut ? rut.toUpperCase() : '';

        if (!!this.contactoForm.controls.mensaje.value) {
          this.contactoForm.controls.mensaje.setValue(
            this.contactoForm.controls.mensaje.value
              .replace(/\s/g, ' ')
              .replace(/"/g, "'")
          );
        }

        // creamos objeto de envio correo.
        const requestFormulario = new PeticionEnvioCorreo(
          this.contactoForm.controls.nombre.value,
          this.contactoForm.controls.apellidos.value,
          this.contactoForm.controls.telefono.value,
          this.contactoForm.controls.email.value,
          this.contactoForm.controls.mensaje.value,
          this.tipoContacto,
          this.contactoForm.controls.captcha.value,
          this.financiaCompra + '',
          this.partePago + '',
          '9000000-4',
          patente,
          this.fichaVehiculo.Patente,
          this.fichaVehiculo.marca?.Nombre,
          this.fichaVehiculo.Modelo,
          this.fichaVehiculo.AnioFabricacion,
          this.ofertaPartePago?.marca?.toString(),
          this.ofertaPartePago?.modelo?.toString(),
          this.ofertaPartePago?.version?.toString(),
          this.ofertaPartePago?.ano?.toString(),
          this.ofertaPartePago?.kilometraje?.toString(),
          this.ofertaPartePago?.valor?.toString(),
          this.dataBanner?.flag || false
        );

        this.servicio.enviarFormularioContacto(requestFormulario).subscribe(
          (respuesta: RespuestaEnvioCorreo) => {
            this.registraContactoHubspot().subscribe(contacto => { 
              this.registraNegocioHubspot(contacto).subscribe(negocio => { 
                const email = this.contactoForm.controls.email.value;
                this.loadingService.hide();
                this.contactoForm.reset();
                this.financiaCompra = false;
                this.partePago = false;
                this.textoBotonEnviar = this.TEXTO_ENVIAR;
    
                if (respuesta.codigo === '200') {
                  const initialState = {
                    isSuccess: true,
                    emailCliente: email,
                  };
                  this.bsModalRef = this.modalService.show(
                    RespuestaEnvioCorreoComponent,
                    {
                      initialState,
                      backdrop: 'static',
                      keyboard: false,
                      class: 'modal-lg',
                    }
                  );
                } else {
                  this.ofertaPartePago = TasadorCotizacionMethods.emptyTasadorCotizacionModel();
                  this.mostrarOferta = false;
                  const initialState = {
                    isSuccess: false,
                    emailCliente: '',
                  };
                  this.bsModalRef = this.modalService.show(
                    RespuestaEnvioCorreoComponent,
                    {
                      initialState,
                      backdrop: 'static',
                      keyboard: false,
                      class: 'modal-lg',
                    }
                  );
                  throw new Error(
                    '[ContactoComponent][enviarFormularioContacto] Error al enviar Formulario Contacto'
                  );
                }
              }); 
            });
            
          },
          async (error) => {
            this.loadingService.hide();
            this.logger.error('Error:', JSON.stringify(error));
            this.textoBotonEnviar = this.TEXTO_ENVIAR;
            this.ofertaPartePago = TasadorCotizacionMethods.emptyTasadorCotizacionModel();
            this.mostrarOferta = false;
            const initialState = {
              isSuccess: false,
              emailCliente: '',
            };
            this.bsModalRef = this.modalService.show(
              RespuestaEnvioCorreoComponent,
              { initialState, class: 'modal-lg' }
            );
            throw new Error(
              '[ContactoComponent][enviarFormularioContacto] Error al enviar Formulario Contacto'
            );
          }
        );



      },
      (error: any) => {
        // handle error here
        console.log(error);
      }
    );
  }

registraContactoHubspot() : Observable<string> {
  const requestContacto = new PeticionContacto(
    this.contactoForm.controls.nombre.value,
    this.contactoForm.controls.apellidos.value,
    '9000000-4',
    // this.contactoForm.controls.rut.value ? this.contactoForm.controls.rut.value.toUpperCase() : '' ,
    this.contactoForm.controls.email.value,
    this.contactoForm.controls.telefono.value
  );

  return this.hubspotService.enviarContactoHubspotSellOut(requestContacto);
}

registraNegocioHubspot(idContacto:string) : Observable<any> {
  const requestNegocio = new PeticionNegocio(
    '',
    this.contactoForm.controls.financiarCompra.value.toString(),
    this.contactoForm.controls.mensaje.value.toString(),
    this.fichaVehiculo.Patente,
    this.fichaVehiculo.AnioFabricacion,
    this.fichaVehiculo.marca?.Nombre,
    this.fichaVehiculo.Modelo,
    '',
    this.fichaVehiculo.tipo_de_vehiculo?.Nombre,
    this.fichaVehiculo.Kilometraje,
    this.fichaVehiculo.Precio,
    this.fichaVehiculo.Precio,
    this.ofertaPartePago.patente ? this.ofertaPartePago.patente.toUpperCase() : '',
    this.ofertaPartePago?.ano?.toString(),
    this.ofertaPartePago?.marca?.toString(),
    this.ofertaPartePago?.modelo?.toString(),
    this.ofertaPartePago?.version?.toString(),
    this.ofertaPartePago?.kilometraje?.toString(),
    this.ofertaPartePago?.valor?.toString(),
    '','','','','',
    this.contactoForm.controls.nombre.value + ' ' + 
    this.contactoForm.controls.apellidos.value
  );

  return this.hubspotService.enviarNegocioHubspotSellOut(requestNegocio, idContacto);
}

  // validaRut(c: AbstractControl): ValidationErrors | null {
  //   if (c.value) {
  //     return validateRut(c.value) ? null : { invalidRut: true };
  //   } else {
  //     return null;
  //   }
  // }

  formDisabled(): boolean {
    return (
      this.contactoForm.invalid ||
      // (this.tipoContacto === 'ficha' &&
      //   (this.contactoForm.get('rut')?.value === '' ||
      //     this.contactoForm.get('rut')?.value === null)) ||
      this.textoBotonEnviar === this.TEXTO_ENVIANDO ||
      (!this.contactoForm.get('terminosCondiciones')?.value &&
        this.tipoContacto === 'ficha')
    );
  }

  /**
   * Método que levanta el modal de tasación cuando existe una oferta anterior
   */
  volverTasar(): void {
    const initialState = {
      oferta: this.ofertaPartePago,
    };
    this.bsModalRef = this.modalService.show(PartePagoModalComponent, {
      initialState,
      backdrop: 'static',
      keyboard: false,
      class: 'modal-xl',
    });
  }

  /**
   * Método que evalua si el rut no ha sido ingresado
   */
  onFocusOutRut(e: any): void {
    this.rutNoIngresado = false;
    if (e.target.value === '' || e.target.value === null) {
      this.rutNoIngresado = true;
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactoComponent } from '@app/pages/contacto/contacto.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TasadorModule } from '@app/pages/tasador/tasador.module';
import { ModalsModule } from '@app/ui/modals/modals.module';

@NgModule({
  declarations: [ContactoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TasadorModule,
    ModalsModule,
    
  ],
  exports: [ContactoComponent]
})
export class ContactoModule {}

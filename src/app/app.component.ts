import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoadingService } from './services/loading/loading.service';
declare const ga: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  /**
   * Variable que indica si se debe mostrar el elemento Loading
   */
  isLoading = 0;

  /**
   * Variable que indica si el spinner es para formulario.
   */
  isForm = false;

  /**
   * Subscripciones
   */
  private subscription: Subscription;
  private subscriptionForm: Subscription;

  constructor(
    private loadingService: LoadingService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router
  ) {
    this.subscription = new Subscription();
    this.subscriptionForm = new Subscription();
  }

  ngOnInit(): void {
    this.subscription = this.loadingService.loadingState.subscribe((state) => {
      this.isLoading = state;
      this.changeDetectorRef.detectChanges();
    });

    this.subscriptionForm = this.loadingService.formularioState.subscribe(
      (state) => {
        this.isForm = state;
        this.changeDetectorRef.detectChanges();
      }
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.subscriptionForm.unsubscribe();
  }

  /**
   * Metodo que retorna booleano si es que la url contiene las palabras especificas
   */
  shouldHideFooterAndHeader(): boolean {
    let showFooterAndHeader = true;
    if (
      this.router.url.includes('compra-autos-usados') ||
      this.router.url.includes('autos-usados') ||
      this.router.url.includes('photo')
    ) {
      showFooterAndHeader = false;
    }
    return showFooterAndHeader;
  }
}

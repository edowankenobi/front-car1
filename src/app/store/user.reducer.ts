
import { Action, createReducer, on } from '@ngrx/store';
import { changeTelefono, changePatente, changeEmail, userData, fecha } from './user.actions';

export interface State {
    email: string;
    telefono: string;
    patente: string;
    userData: string;
    fecha: string;
    
}

export const initialState: State= {
    email: '',
    telefono: '',
    patente: '', 
    userData:'',
    fecha: '',
    
}

const _serviceReducer = createReducer(
  initialState,
  on(changeEmail, (state, content) => ({...state, email: content.content })),
  on(changeTelefono, (state, content) => ({...state, telefono: content.content })),
  on(changePatente, (state, content) => ({...state, patente: content.content })),
  on(userData, (state, content) => ({...state, userData: content.content })),
  on(fecha, (state, content) => ({...state, fecha: content.content })),

);

export function serviceReducer(state: State | undefined, action: Action) {
  return _serviceReducer(state, action);
}
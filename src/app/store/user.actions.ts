import { createAction, props } from '@ngrx/store';


export const changeEmail = createAction('[USER Component] Change Email', props<{content: string}>());
export const changeTelefono = createAction('[USER Component] Change Phone', props<{content: string}>());
export const changePatente = createAction('[USER Component] Change Patente', props<{content: string}>());
export const userData = createAction('[USER Component] User Data', props<{content: string}>());
export const fecha = createAction('[USER Component] fecha', props<{content: string}>());




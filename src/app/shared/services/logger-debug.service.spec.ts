import { TestBed } from '@angular/core/testing';
import { environment } from '@env/environment';
import { LoggerService } from './logger.service';

describe('LoggerServiceDebug', () => {
  let service: LoggerService;

  beforeEach(() => {
    const env = environment;
    env.log.level = 'DEBUG';
    TestBed.configureTestingModule({
      providers: [{
        provide: 'env',
        useValue: env,
      }]
    });
    service = TestBed.inject(LoggerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('log info', () => {
    service.info("Prueba INFO");
  });

  it('log debug', () => {
    service.debug("Prueba DEBUG");
  });

  it('log error', () => {
    service.error("Prueba ERROR");
  });

  it('log force', () => {
    service.force("Prueba FORCE");
  });

  it('log trace', () => {
    service.trace("Prueba TRACE");
  });

  it('log warning', () => {
    service.warning("Prueba WARNING");
  });
});

import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
export class ActivatedRouteMock implements ActivatedRoute {
  /**
   * Mock
   */
  public snapshot: any;
  /**
   * Mock
   */
  public url: any;
  /**
   * Mock
   */
  public params: any;
  /**
   * Mock
   */
  public queryParams!: Observable<Params>;
  /**
   * Mock
   */
  public fragment!: Observable<string>;
  /**
   * Mock
   */
  public data: any;
  /**
   * Mock
   */
  public outlet: any;
  /**
   * Mock
   */
  public component: any;
  /**
   * Mock
   */
  public paramMap: any;
  /**
   * Mock
   */
  public queryParamMap: any;
  /**
   * Mock
   */
  public routeConfig: any;
  /**
   * Mock
   */
  public root: any;
  /**
   * Mock
   */
  public parent: any;
  /**
   * Mock
   */
  public firstChild: any;
  /**
   * Mock
   */
  public children: any;
  /**
   * Mock
   */
  public pathFromRoot: any;
  public toString(): string {
    return '';
  }
}

import { ActionReducerMap } from "@ngrx/store";
import * as user from './store/user.reducer'
export interface AppState {
    
    user: user.State,
}

export const appReducers: ActionReducerMap<AppState> = {
    user: user.serviceReducer,
} 
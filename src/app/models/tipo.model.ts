export interface TipoModel {
  id: number;
  Tipo: string;
  published_at: Date;
  created_at: Date;
  updated_at: Date;
}

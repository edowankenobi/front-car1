export class LocationResponse {
  codigo!: number;
  tipo!: string;
  nombre!: string;
  lat!: number;
  lng!: number;
  url!: string;
  codigo_padre!: number;
}

export interface AceptaFinanModel {
    montoAFinanciar: number;
    nacionalidad: number;
    tipoCliente: number;
    tipoContrato: number;
    tipoRenta: number;
    renta: number;
    antiguedad: number;
    pie: number;
    plazo: number;
    vehiculo: AceptaVehiculoModel;
    seguros: AceptaSegurosModel;
    persona: AceptaPersonaModel;
    IdApp?: number;
    captcha: string;
}
export interface AceptaVehiculoModel {
    marca: string;
    modelo: string;
    anio: string;
    precio: string;
}
export interface AceptaSegurosModel {
    desgravamen: boolean;
    cesantia: boolean;
}
export interface AceptaPersonaModel {
    rut: string;
    nombre: string;
    primerApellido: string;
    segundoApellido: string;
    direccion: string;
    numero: string;
    correo: string;
    telefono: string;
}
export interface FinanciamientoModel {
    marca: string;
    modelo: string;
    anno: number;
    precio: number;
    montoPie: number;
    plazo: number;
    seguros: SegurosFinanciamientoModel;
}
export interface SegurosFinanciamientoModel {
    desgravamen: boolean;
    cesantia: boolean;
}
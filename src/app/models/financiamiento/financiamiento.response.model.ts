export interface FinanciamientoResponseModel {
    success: boolean;
    body: FinanBodyResponseModel;
    return: FinanRetornoModel;
}
export interface FinanBodyResponseModel {
    cuotaTrinidad: FinanCuotaTrinidadModel;
}
export interface FinanCuotaTrinidadModel {
    error: boolean;
    valorCuota: string;
    valorCae: number;
    totalCredito: number;
    mensaje: string;
}
export interface FinanRetornoModel {
    code: string;
    message: string;
}
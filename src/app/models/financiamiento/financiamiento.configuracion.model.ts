import { IdValueModel } from '../idvalue.model';

export interface FinanConfiguracionModel {
    cuotasPago: number[];
    pie: IdValueModel<number, number>;
    tipoCliente: IdValueModel<number, string>[];
    tipoContrato: IdValueModel<number, string>[];
    tipoNacionalidad: IdValueModel<number, string>[];
    tipoRenta: IdValueModel<number, string>[];
}
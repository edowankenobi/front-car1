export interface PeticionAgendaModel {
    dealsId : string;
    inspector: string;
    propiedades : PropiedadAgendaModel
  }

  export interface PropiedadAgendaModel {
    agenda_inspeccion: string;
    hora_de_visita: string;
  }
export class PeticionContacto {
    firstname: string;
    lastname: string;
    rut: string;
    email: string;
    phone: string;

    constructor(
        firstname: string= '',
        lastname: string= '',
        rut: string= '',
        email: string= '',
        phone: string= '',
    ) {
      this.firstname = firstname;
      this.lastname = lastname;
      this.rut = rut;
      this.email = email;
      this.phone = phone;
    }
  }
  
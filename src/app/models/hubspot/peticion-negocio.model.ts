export class PeticionNegocio {
  id:string;
  financiamiento: string;
  mensajecontacto: string;
  patente: string;
  ano: string;
  marca: string;
  modelo: string;
  version: string;
  tipo_de_auto: string;
  kilometraje: string;
  precio_publicado: string;
  amount: string;
  patente_auto_en_parte_pago: string;
  ano_auto_en_pago: string;
  marca_auto_en_pago: string;
  modelo_auto_en_pago: string;
  version_auto_en_pago: string;
  kilometraje_auto_en_pago: string;
  valor_retoma: string;
  folio_cotizador: string;
  oferta_cotizador: string;
  version_por_agenda: string;
  agenda_inspeccion: string;
  hora_de_visita:string;
  contacto: string;
    constructor(
      id:string='',
      financiamiento: string ='',
      mensajecontacto: string='',
        patente: string='',
        ano: string='',
        marca: string='',
        modelo: string='',
        version: string='',
        tipo_de_auto: string='',
        kilometraje: string='',
        precio_publicado: string='',
        amount='',
        patente_auto_en_parte_pago: string='',
        ano_auto_en_pago: string='',
        marca_auto_en_pago: string='',
        modelo_auto_en_pago: string='',
        version_auto_en_pago: string='',
        kilometraje_auto_en_pago: string='',
        valor_retoma: string='',
        folio_cotizador: string='',
        oferta_cotizador: string='',
        version_por_agenda: string='',
        agenda_inspeccion: string='',
        hora_de_visita:string='',
        contacto:string =''
    ) {
      this.id = id;
      this.financiamiento = financiamiento;
      this.mensajecontacto = mensajecontacto;
      this.patente = patente;
      this.ano = ano;
      this.marca = marca;
      this.modelo = modelo;
      this.version = version;
      this.version = version;
      this.tipo_de_auto = tipo_de_auto;
      this.kilometraje = kilometraje;
      this.precio_publicado = precio_publicado;
      this.amount = amount;
      this.patente_auto_en_parte_pago = patente_auto_en_parte_pago;
      this.ano_auto_en_pago = ano_auto_en_pago;
      this.marca_auto_en_pago = marca_auto_en_pago;
      this.modelo_auto_en_pago = modelo_auto_en_pago;
      this.version_auto_en_pago = version_auto_en_pago;
      this.kilometraje_auto_en_pago = kilometraje_auto_en_pago;
      this.valor_retoma = valor_retoma;
      this.folio_cotizador = folio_cotizador;
      this.oferta_cotizador = oferta_cotizador;
      this.version_por_agenda = version_por_agenda;
      this.agenda_inspeccion = agenda_inspeccion;
      this.hora_de_visita = hora_de_visita;
      this.contacto = contacto
    }
  }
  
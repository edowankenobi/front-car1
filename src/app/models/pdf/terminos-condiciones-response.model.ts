export interface TerminosCondicionesResponseModel {
    id: number;
    name: string;
    alternativeText: string;
    caption: string;
    width: null;
    height: null;
    formats: object;
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: null;
    provider: string;
    provider_metadata: null;
    created_at: string;
    updated_at: string;
    related: string[];
}
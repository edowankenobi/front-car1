import { Publication } from '@app/models/tasador/patente/valor-tasacion/publication.model';

export class ValorTasacionModel {
  brandId?: number;
  modelId?: number;
  year?: number;
  versionId?: number;
  requestedKm?: number;
  requestedTime?: string;
  brandName?: string;
  modelName?: string;
  showName?: string;
  retake?: Publication;
  publication?: Publication;
  patente?: string;
  valorTasacion?: number;
  factor?: string;
  categoria?: string;
  fechaInicioServicio?: string;
  fechaFinServicio?: string;

  constructor() {
    this.brandId = 0;
    this.modelId = 0;
    this.year = 0;
    this.versionId = 0;
    this.requestedKm = 0;
    this.retake = new Publication();
    this.publication = new Publication();
    this.factor = '';
    this.categoria = '';
    this.fechaInicioServicio = '';
    this.fechaFinServicio = '';
  }
}

import { ValorTasacionModel } from '@app/models/tasador/patente/valor-tasacion/valor-tasacion.model';

export interface ValorTasacionResponseModel {
  tasacionVehiculo: ValorTasacionModel;
  codigo: string;
  descripcion: string;
}

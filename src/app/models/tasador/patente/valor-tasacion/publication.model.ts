export class Publication {
  suggestedPrice?: number;
  calculationType?: string;
  precision?: string;
  suggestedBusinessPrice?: number | null;

  constructor() {}
}

import { PatenteTasadorModel } from '@app/models/tasador/patente/patente-tasador.model';

export interface PatenteTasadorResponseModel {
  datosPorPlaca: PatenteTasadorModel;
  codigo: string;
  descripcion: string;
}

export interface VersionTasadorModel {
  anio: number;
  cilindrada: number;
  id: number;
  idModelo: number;
  nombre: string;
  popularidad: number;
  tipoCombustible: number;
  traccion: number;
  transmision: number;
}

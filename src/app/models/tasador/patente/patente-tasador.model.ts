import { VersionTasadorModel } from '@app/models/tasador/patente/version-tasador.model';

export interface PatenteTasadorModel {
  requestID: number;
  modelID: number;
  year: number;
  modelName: string;
  brandName: string;
  color: string;
  cylinderCapacity: number;
  brandID: number;
  versions: VersionTasadorModel[];
  anioValido: boolean;
  anioReglaExcepcion: string;
}

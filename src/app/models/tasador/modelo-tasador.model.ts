export interface ModeloTasadorModel {
    id: number;
    idMarca?: number;
    nombre: string;
    idTipoVehiculo?: number;
}

import { ModeloTasadorModel } from '@app/models/tasador/modelo-tasador.model';

export interface ModeloTasadorResponseModel {
    codigo: string;
    descripcion: string;
    modelos: ModeloTasadorModel[];
}


export interface AnioTasadorResponseModel {
    agno: number[];
    codigo: string;
    descripcion: string;
}

export interface KilometrajeTasadorModel {
  id: number;
  Descripcion: string;
  Valor: string;
  published_at: Date;
  created_at: Date;
  updated_at: Date;
}

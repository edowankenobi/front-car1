import { MarcaTasadorModel } from '@app/models/tasador/marca-tasador.model';

export interface MarcaTasadorResponseModel {
    codigo: string;
    descripcion: string;
    marcas: MarcaTasadorModel[];
}

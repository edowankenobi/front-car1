export interface GuardarDetalleResponse {
  codigo: string;
  descripcion: string;
  folio: string;
}

export class GuardarDetalleRequestModel {
  folio!: string;
  fechaCotizacion!: string; // fecha del dia
  patenteTasacion!: string; // patente inicio tasacion
  anioReglaExcepcion!: string;
  marca!: string; // tasacion
  modelo!: string; // tasacion
  anio!: number; // tasacion
  kilometraje!: number; // tasacion
  version!: string; // tasacion
  emailTasacion!: string; //tasacion
  telefonoTasacion!: string; //tasacion
  aceptaTerminosYCondiciones!: boolean; // al seleccionar check
  fechaHoraTerminosYCondiciones!: string; // fecha y hora al seleccionar al seleccionar check terminos
  precioRetoma!: number; // tasacion retake
  precioMercado!: number; // tasacion publicacion
  precioOferta!: number; // tasacion precio listo
  factor!: string; // tasacion
  categoria!: string; // tasacion
  fechaFinRequestTasacion!: string; //tasacion aun no estan listas
  fechaInicioRequestTasacion!: string; //tasacion aun no estan listas
  clickBotonTasarAuto!: boolean; // boolean al clikear btn tasar
  clickBotonVolverATasar!: boolean; // boolean al clickear al volver a tasar
  clickBotonAgendar!: boolean; // boolean al btn agendar
  patenteDatosCliente!: string; // patente de contacto
  nombre!: string; // nombre de contacto
  apellido!: string; // apellido de contacto
  emailDatosCliente!: string; // email de contacto
  telefono!: string; // telf de contacto
  inspector!: string; // ejecutivo de peticion envio cita
  clickBotonSiguiente!: boolean; // booleano al clickear en siguiente en contacto
  clickBotonVolverATasarDatosCliente!: boolean; // booleano al clickear volver a tasar en contacto
  fechaAgenda!: string; //fecha agenda al confirmar agendamiento
  horaAgenda!: string; // hora agenda al confirmar agendamiento
  clickBotonConfirmar!: boolean; // booleano al clickear en confirmar agenda
  clickBotonVolverAgenda!: boolean; // booleano al clickear en volver agenda
  exitoBotonConfirmar!: boolean; // exito al llegar a pantalla de fin
  clickBotonVolverATasarFinal!: boolean; // boolean al hacer click en volver a tasar final
  exitoEnvioCorreo!: boolean; // se valida en true automaticamente al llegar al final
  generaFolio!: boolean; // valor nuevo, si quiero folio nuevo es 'true', si quiero grabar es 'false'
  pantallaCierre!: string; //cerrar sesion, mandar nombre de url de que pantalla era
  captcha!: string; // captcha
  rutAsociado!: string; // rut de usuario logeado
  nombreAsociado!: string; // nombre usuario logeado
  estado!: string; // activo o no activo, siempre deberia ser activo

  constructor() {
    this.folio = '';
    this.fechaCotizacion = '';
    this.patenteTasacion = '';
    this.anioReglaExcepcion = '';
    this.marca = '';
    this.modelo = '';
    this.anio = 0;
    this.kilometraje = 0;
    this.version = '';
    this.emailTasacion = '';
    this.telefonoTasacion = '';
    this.aceptaTerminosYCondiciones = false;
    this.fechaHoraTerminosYCondiciones = '';
    this.precioRetoma = 0;
    this.precioMercado = 0;
    this.precioOferta = 0;
    this.factor = '';
    this.categoria = '';
    this.fechaFinRequestTasacion = '';
    this.fechaInicioRequestTasacion = '';
    this.clickBotonTasarAuto = false;
    this.clickBotonVolverATasar = false;
    this.clickBotonAgendar = false;
    this.patenteDatosCliente = '';
    this.nombre = '';
    this.apellido = '';
    this.emailDatosCliente = '';
    this.telefono = '';
    this.inspector = '';
    this.clickBotonSiguiente = false;
    this.clickBotonVolverATasarDatosCliente = false;
    this.fechaAgenda = '';
    this.horaAgenda = '';
    this.clickBotonConfirmar = false;
    this.clickBotonVolverAgenda = false;
    this.exitoBotonConfirmar = false;
    this.clickBotonVolverATasarFinal = false;
    this.exitoEnvioCorreo = false;
    this.generaFolio = false;
    this.pantallaCierre = '';
    this.captcha = '';
    this.rutAsociado = '';
    this.nombreAsociado = '';
    this.estado = '';
  }
}

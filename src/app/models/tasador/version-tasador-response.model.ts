import { VersionTasadorModel } from './patente/version-tasador.model';

export interface VersionTasadorResponseModel {
    codigo: string;
    descripcion: string;
    versiones: VersionTasadorModel[];
}

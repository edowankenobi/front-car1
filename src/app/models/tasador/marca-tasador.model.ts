export interface MarcaTasadorModel {
  id: number;
  nombre: string;
  popularidad?: number;
  incluyeVehiculosPasajeros?: boolean;
}

export interface TasadorResponseModel<T> {
    codigo: string;
    descripcion: string;
    elemento: T;
}
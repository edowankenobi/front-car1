export interface TasadorIdValorModel<T1, T2> {
    id: T1;
    valor: T2;
}
import { TasadorIdNombreModel } from "./tasador.idnombre.model";

export interface TasadorPPUModel {
    marcaId: number;
    marca: string;
    modeloId: number;
    modelo: string;
    anioId: number;
    anio: number;
    versiones: TasadorIdNombreModel[];
}
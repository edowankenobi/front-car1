export interface TasadorIdNombreModel {
    id: number;
    nombre: string;
}
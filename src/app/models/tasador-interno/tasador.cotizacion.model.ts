export interface TasadorCotizacionModel {
    versionid: number;
    valorsugerido: number;
    valor: number;
    marca?: string;
    modelo?: string;
    version?: string;
    ano?: number;
    kilometraje?: number;
    fecha: string;
    patente?: string;
}
export class TasadorCotizacionMethods {
   public static emptyTasadorCotizacionModel(): TasadorCotizacionModel {
        return {
            versionid: 0,
            valorsugerido: 0,
            valor: 0,
            fecha: '',
            marca: '',
            modelo: '',
            version: '',
            ano: 0,
            kilometraje: 0,
            patente: ''
        }
    }
    public static isEmpty(tasadorCotizacionModel: TasadorCotizacionModel): boolean {
        let isempty = true;
        const dataObj = Object.entries(tasadorCotizacionModel);
        if (dataObj.length == 0) {
            return isempty;
        }
        dataObj.forEach(item => {
            switch (typeof item[1]) {
                case 'number':
                    if(item[1] != 0) {
                        isempty = false;
                        return;
                    }
                    break;
                case 'string':
                    if(item[1] != '') {
                        isempty = false;
                        return;
                    }
                    break;
            }
        });
        return isempty;
    }
}
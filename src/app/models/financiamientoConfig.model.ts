export interface FinanciamientoConfigModel {
    pie: FinanPieModel;
    seguros: FinanSegurosModel;
    cuotas: FinanCuotasModel;
}
export interface FinanPieModel {
    auto: number;
    minimo: number;
    maximo: number;
}
export interface FinanSegurosModel {
    desgravamen: FinanSegurosDataModel;
    cesantia: FinanSegurosDataModel;
}
export interface FinanSegurosDataModel {
    checked: boolean;
    disabled: boolean;
}
export interface FinanCuotasModel {
    secuencia:  number[];
    defecto: number;
}
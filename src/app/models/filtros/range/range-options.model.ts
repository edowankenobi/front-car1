export interface RangeOptions {
    origen: string;
    titulo: string;
    minValue: number;
    maxValue: number;
    floor: number;
    ceil: number;
    step: number;
  }

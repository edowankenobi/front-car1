export interface FiltroModel {
  id: number;
  Nombre: string;
  NombreLogico: string;
  Orden: number;
  Activo: boolean;
  published_at: Date;
  created_at: Date;
  updated_at: Date;
}

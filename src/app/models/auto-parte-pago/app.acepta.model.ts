import { AppResumenModel } from "./app.resumen.model";

export interface AceptaApp {
    tasacionAppId: number;
    resumen: AppResumenModel;
}
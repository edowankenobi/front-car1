export interface PreguntaAppModel {
    id: number;
    pregunta: string;
    alternativas: AlternativaAppModel[]
}
export interface AlternativaAppModel {
    id: number;
    alternativa: string;
    descripcion: string;
    imagen: string;
}
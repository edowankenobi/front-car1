import { IdValueModel } from '@app/models/idvalue.model';

export interface AppFormModel {
    patente?: string;
    anio: number;
    marcaId: number;
    marca: string;
    modeloId: number;
    modelo: string;
    versionId: number;
    version: string;
    kilometraje: number;
    origenId: number;
    valorTasacion: number;
    encuesta: IdValueModel<number, number>[];
}
export interface AppResumenModel {
    marca?: string;
    modelo?: string;
    version?: string;
    anio?: number;
    kilometraje?: number;
    valorFinal?: number;
}
import { Image } from '@app/models/image.model';

export class BannerHome {
  id?: number;
  Titulo?: string;
  Subtitulo?: string;
  Boton1Texto?: string;
  Boton1URL?: null;
  Boton2Texto?: string;
  Boton2URL?: null;
  published_at?: Date;
  created_at?: Date;
  updated_at?: Date;
  Imagen?: Image[];
}

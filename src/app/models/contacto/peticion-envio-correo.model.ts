export class PeticionEnvioCorreo {
  nombre: string;
  apellidos: string;
  telefono: string;
  email: string;
  mensaje: string;
  financiarCompra: string;
  autoPartePago: string;
  rut: string;
  patentePartePago: string;
  patenteSolicitud: string;
  captcha: string;
  marca: string;
  modelo: string;
  anio: string;
  tipoForm: string;
  marcaPartePago: string;
  modeloPartePago: string;
  versionPartePago: string;
  anioPartePago: string;
  kilometrajePartePago: string;
  valorPartePago: string;
  identificadorBanner: boolean;

  constructor(
    nombre: string,
    apellidos: string,
    telefono: string,
    email: string,
    mensaje: string,
    tipoForm: string,
    captcha: string,
    financiarCompra: string = '',
    autoPartePago: string = '',
    rut: string = '',
    patentePartePago: string = '',
    patenteSolicitud: string = '',
    marca: string = '',
    modelo: string = '',
    anio: string = '',
    marcaPartePago: string = '',
    modeloPartePago: string = '',
    versionPartePago: string = '',
    anioPartePago: string = '',
    kilometrajePartePago: string = '',
    valorPartePago: string = '',
    identificadorBanner: boolean = false
  ) {
    this.nombre = nombre;
    this.apellidos = apellidos;
    this.telefono = telefono;
    this.email = email;
    this.mensaje = mensaje;
    this.financiarCompra = financiarCompra;
    this.autoPartePago = autoPartePago;
    this.rut = rut;
    this.patentePartePago = patentePartePago;
    this.patenteSolicitud = patenteSolicitud;
    this.captcha = captcha;
    this.marca = marca;
    this.modelo = modelo;
    this.anio = anio;
    this.tipoForm = tipoForm;
    this.marcaPartePago = marcaPartePago;
    this.modeloPartePago = modeloPartePago;
    this.versionPartePago = versionPartePago;
    this.anioPartePago = anioPartePago;
    this.kilometrajePartePago = kilometrajePartePago;
    this.valorPartePago = valorPartePago;
    this.identificadorBanner = identificadorBanner;
  }
}

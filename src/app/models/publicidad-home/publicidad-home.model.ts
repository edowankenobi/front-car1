import { Image } from '@app/models/image.model';

export interface PublicidadHome {
  id: number;
  URL: null;
  published_at: Date;
  created_at: Date;
  updated_at: Date;
  Tag: string;
  Orden: number;
  Imagen: Image;
}

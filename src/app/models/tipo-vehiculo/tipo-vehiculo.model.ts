import { Image } from '@app/models/image.model';

export interface TipoVehiculo {
  id: number;
  Nombre: string;
  URLImagen: null;
  published_at: Date;
  created_at: Date;
  updated_at: Date;
  Thumbnail: Image;
}

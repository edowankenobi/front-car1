export interface ClienteCitaModel {
    apellidos: string;
    email: string;
    id: number;
    identificacion: string;
    nombre: string;
}

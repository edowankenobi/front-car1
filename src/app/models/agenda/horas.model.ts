import { HoraModel } from './hora.model';

export interface HorasModel {
    horasDisponibles: HoraModel[];
    dia: string;
    nombreDia: string;
    numeroDia: number;
    fechaFormateada: string;
}

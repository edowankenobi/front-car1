import { ClienteCitaModel } from './cliente-cita-model';

export interface CitaModel {
    cliente: ClienteCitaModel;
    estado: string;
    fechaFin: string;
    fechaInicio: string;
    id: number;
    idEstado: number;
    idProveedor: number;
    idServicio: number;
    idUbicacion: number;
    precio: number;
    proveedorServicio: string;
    servicio: string;
}

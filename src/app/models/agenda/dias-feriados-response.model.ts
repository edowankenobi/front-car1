export interface DiasFeriadosResponseModel {
    codigo: string;
    descripcion: string;
    feriados: string[];
}

export interface HoraModel {
    inicio: string;
    fin: string;
    nombreServicio: string;
    nombreProveedor: string;
    idProveedor: number;
    inicioBloque: string;
    bloqueSeleccionado?: boolean;
}

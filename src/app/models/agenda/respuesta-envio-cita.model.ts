import { CitaModel } from './cita.model';

export interface RespuestaEnvioCita {
    cita: CitaModel;
    codigo: string;
    descripcion: string;
}

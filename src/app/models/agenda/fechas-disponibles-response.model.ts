import { HorasModel } from './horas.model';

export interface FechasDisponiblesResponseModel {
    codigo: string;
    descripcion: string;
    horas: HorasModel;
}

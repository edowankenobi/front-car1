export class PeticionEnvioCita {
  fechaHoraInicio: string;
  fechaHoraFin: string;
  idServicio: string;
  idProveedor: string;
  nombre: string;
  apellidos: string;
  email: string;
  telefono: string;
  anio: string;
  kilometraje: string;
  marca: string;
  modelo: string;
  patente: string;
  ejecutivo: string;
  valor: string;
  version: string;
  captcha: string;
  folio: string;
  fechaCotizacion: string;

  constructor(
    fechaHoraInicio: string,
    fechaHoraFin: string,
    idServicio: string,
    idProveedor: string,
    nombre: string,
    apellidos: string,
    email: string,
    telefono: string,
    anio: string,
    kilometraje: string,
    marca: string,
    modelo: string,
    patente: string,
    ejecutivo: string,
    valor: string,
    version: string,
    captcha: string,
    folio: string,
    fechaCotizacion: string
  ) {
    this.fechaHoraInicio = fechaHoraInicio;
    this.fechaHoraFin = fechaHoraFin;
    this.idServicio = idServicio;
    this.idProveedor = idProveedor;
    this.nombre = nombre;
    this.apellidos = apellidos;
    this.email = email;
    this.telefono = telefono;
    this.anio = anio;
    this.kilometraje = kilometraje;
    this.marca = marca;
    this.modelo = modelo;
    this.patente = patente;
    this.ejecutivo = ejecutivo;
    this.valor = valor;
    this.version = version;
    this.captcha = captcha;
    this.folio = folio;
    this.fechaCotizacion = fechaCotizacion;
  }
}

import { Thumbnail } from '@app/models/thumbnail.model';

export interface Formats {
  thumbnail?: Thumbnail;
}

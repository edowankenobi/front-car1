export interface TipoTModel<T1, T2> {
    val1: T1;
    val2: T2;
}
export class InformacionEmpresa {
  codigo: string;
  descripcion: string;
  razonSocial: string;
  giro: string;

  constructor(
    codigo: string,
    descripcion: string,
    razonSocial: string,
    giro: string
  ) {
    this.codigo = codigo;
    this.descripcion = descripcion;
    this.razonSocial = razonSocial;
    this.giro = giro;
  }
}

export class BotonComprasRequest {
  patente: string;
  tipo: number;
  constructor(patente: string, tipo: number) {
    this.patente = patente;
    this.tipo = tipo;
  }
}

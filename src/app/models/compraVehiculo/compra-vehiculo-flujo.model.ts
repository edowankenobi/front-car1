export class CompraVehiculoFlujo {
  nombreFlujo: string;
  numeroPaso: number;
  timeStamp: string;
  captcha: string;

  constructor(
    nombreFlujo: string,
    numeroPaso: number,
    timeStamp: string,
    captcha: string
  ) {
    this.nombreFlujo = nombreFlujo;
    this.numeroPaso = numeroPaso;
    this.timeStamp = timeStamp;
    this.captcha = captcha;
  }
}

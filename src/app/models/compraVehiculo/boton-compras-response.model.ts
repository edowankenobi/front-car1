export class BotonCompras {
  codigo: string;
  descripcion: string;
  estado: number;
  fechaInicioCompra: string;
  patente: string;

  constructor(
    codigo: string,
    descripcion: string,
    estado: number,
    fechaInicioCompra: string,
    patente: string
  ) {
    this.codigo = codigo;
    this.descripcion = descripcion;
    this.estado = estado;
    this.fechaInicioCompra = fechaInicioCompra;
    this.patente = patente;
  }
}

export class EnumApp {
    formulario: number = 1;
    oferta: number = 2;
    rechaza: number = 3;
}
export class EnumSolicitudFinanForm {
    formulario: number = 1;
    exito: number = 2;
}
export class EnumAceptacionFinan {
    correcta: number = 1;
    rechazada: number = 2;
    errorCaptcha: number = 3;
    errorBD: number = 4;
    errorServicio: number = 5;
}
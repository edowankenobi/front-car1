export interface IdValueModel<T1, T2> {
    id: T1;
    value: T2;
}
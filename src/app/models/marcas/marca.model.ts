import { Image } from '@app/models/image.model';

export interface MarcaModel {
  id: number;
  Nombre: string;
  published_at: Date;
  created_at: Date;
  updated_at: Date;
  Logo: Image[];
}

const gulp = require('gulp');
const awspublish = require('gulp-awspublish');

var localConfig = {
    buildSrc: './dist/front-car1/**/*',

    getAwsConf: function (environment) {
        var conf = require('./config/aws');

        if (!conf[environment]) {
            throw 'No aws configuration for environment: ' + environment;
        } else if (!conf[environment + 'Headers']) {
            throw 'No aws headers for environment: ' + environment;
        }
        return { keys: conf[environment], headers: conf[environment + 'Headers'] };
    }
}

gulp.task('deploy-develop', function () {
    var awsConf = localConfig.getAwsConf('deploy-develop');
    var publisher = awspublish.create(awsConf.keys);

    return gulp.src(localConfig.buildSrc)
        .pipe(awspublish.gzip({ ext: '' }))
        .pipe(publisher.publish(awsConf.headers))
        .pipe(publisher.cache())
        .pipe(publisher.sync())
        .pipe(awspublish.reporter());
});

gulp.task('deploy-qa', function () {
    var awsConf = localConfig.getAwsConf('deploy-qa');
    var publisher = awspublish.create(awsConf.keys);

    return gulp.src(localConfig.buildSrc)
        .pipe(awspublish.gzip({ ext: '' }))
        .pipe(publisher.publish(awsConf.headers))
        .pipe(publisher.cache())
        .pipe(publisher.sync())
        .pipe(awspublish.reporter());
});

gulp.task('deploy-production', function () {
  var awsConf = localConfig.getAwsConf('deploy-production');
  var publisher = awspublish.create(awsConf.keys);

  return gulp.src(localConfig.buildSrc)
      .pipe(awspublish.gzip({ ext: '' }))
      .pipe(publisher.publish(awsConf.headers))
      .pipe(publisher.cache())
      .pipe(publisher.sync())
      .pipe(awspublish.reporter());
});

pipeline {
    agent {
        node {
            label 'agente-spot-large'
        }
    }
    tools {
        nodejs 'node-12.14.1'
    }

    environment {
        ENTORNO_ACTUAL = obtenerEntorno(env.BRANCH_NAME)
        SONAR_HOST_URL = "http://3.23.230.39:9000"
        SONAR_KEY = "front-car1"
    }

    stages {
        stage('Check configuraciones') {
            steps {
                echo "Iniciando..."
                sh 'node --version'
                sh 'npm --version'
                sh 'ng --version'
                echo "ENTORNO_ACTUAL: ${ENTORNO_ACTUAL}"
                echo "SONAR_HOST_URL: ${SONAR_HOST_URL}"
                echo "SONAR_KEY     : ${SONAR_KEY}"
                echo "BRANCH        : ${env.BRANCH_NAME}"

                script {
                    SONAR_TAG = "${SONAR_KEY}" + "-" + "${env.BRANCH_NAME}".replaceAll('/','-');
                }
                echo "SONAR_TAG     : ${SONAR_TAG}"
            }
        }

        stage('Instalar dependencias') {
            steps {
                echo "Instalar dependencias."
                sh "npm install"
            }
        }

        stage('Construir artefactos') {
            steps {
                echo "Elimino directorio build anterior"
                sh "rm -rf dist/*"
                echo "Construyendo... ${env.BUILD_ID} ambiente ${ENTORNO_ACTUAL} on ${env.JENKINS_URL}${env.JOB_NAME}"
                sh "ng build -c ${ENTORNO_ACTUAL}"
            }
        }

        stage('Pruebas unitarias') {
            steps {
                echo "Ejecutar pruebas unitarias del codigo"
                sh "ng test --all --watch=false --coverage --ci"
            }
        }

        stage('Analisis de código estatico') {
            steps {
                echo "Ejecutando Sonar..."
                withCredentials([string(credentialsId: 'sonar-car1', variable: 'SONAR_LOGIN_TOKEN')]) {
                    sh "npm run sonar -- -Dsonar.host.url=${SONAR_HOST_URL} -Dsonar.login=${SONAR_LOGIN_TOKEN} -Dsonar.projectKey=${SONAR_TAG} -Dsonar.projectName=${SONAR_TAG}"
                }
            }
        }

        stage('Instalar en QA') {
            when {
                anyOf {
                    branch 'develop';
                    branch 'hotfix/mejoraFormularioContacto';
                    branch 'feature/prueba-fix-hotfix';                }
            }
            steps {
                echo "instalando rama ${env.BRANCH_NAME}"
                sh "npm run deploy-qa"
            }
        }

        stage('Instalar en Produccion') {
            when {
                branch "master"
            }
            steps {
                echo "instalando rama ${env.BRANCH_NAME}"
                sh "npm run deploy-produccion"
            }
        }
    }
}

def obtenerEntorno(branch) {
    def res = 'dev'
    if (branch == 'develop') {
        res = 'qa'
    } else if (branch == 'feature/configuracion-jenkins') {
        res = 'qa'
    } else if (branch == 'feature/prueba-fix-hotfix') {
        res = 'qa'
    } else if (branch == 'release') {
        res = 'qa'
    } else if (branch == 'master') {
        res = 'produccion'
    }
    return res;
}
